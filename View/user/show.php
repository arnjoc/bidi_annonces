<?php require dirname(__DIR__) . '/inc/admin_header.php' ?>

<!-- main content start-->
<div id="page-wrapper">
    <div class="main-page">
        <div class="forms">
            <h2 class="title1">Utilisateurs / <small> détails</small></h2>
            <?php require dirname(__DIR__) . '/inc/msg.php' ?>
      	<div class=" col-md-offset-3 col-md-6">
            <div class=" form-grids row form-grids-right">
                <div class="widget-shadow " data-example-id="basic-forms">
                    <div class="form-title">
                        <h4>Détails de <b> <?= $this->oUser->username ?></b>:</h4>
                    </div>
                    <div class="form-body">
                        <div class="user-marorm">
                            <div class="malorum-top">
                            </div>
                            <div class="malorm-bottom">
                                <span class="malorum-pro"> </span>
                                <h2><?= $this->oUser->name ?></h2>
                                <p><i class="fa fa-envelope"></i> <?= $this->oUser->email ?> &nbsp;<i class="fa fa-user"></i> <?= $this->oUser->username ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
</div>
<!-- main content end-->

<?php require dirname(__DIR__) . '/inc/admin_footer.php' ?>