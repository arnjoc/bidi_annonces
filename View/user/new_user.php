<?php require dirname(__DIR__) . '/inc/admin_header.php' ?>

<!-- main content start-->
<div id="page-wrapper">
    <div class="main-page">
      <div class="col-md-6">
        <div class="forms">
            <h2 class="title1">Utilisateurs / <small> Nouveau</small></h2>
            <?php require dirname(__DIR__) . '/inc/msg.php' ?>
            <div class=" form-grids row form-grids-right">
                <div class="widget-shadow " data-example-id="basic-forms">
                    <div class="form-title">
                        <h4>Ajout d'un utilisateur:</h4>
                    </div>
                    <div class="form-body">
                        <form class="form-horizontal"  action="" method="post" data-toggle="validator">
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control" id="inputName" placeholder="Nom complet" required value="<?=isset($_POST['name']) ? $_POST['name'] : ''; ?>">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="username" class="form-control" id="inputName" placeholder="Pseudo" required value="<?=isset($_POST['username']) ? $_POST['username'] : ''; ?>">
                                </div>
                                <div class="form-group has-feedback">
                                    <input type="email" name="email" class="form-control" id="inputEmail" placeholder="Email" data-error="Entrez un email valide" required value="<?=isset($_POST['email']) ? $_POST['email'] : ''; ?>">
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                </div>
                                <div class="form-group">
                                  <input type="password" name="password" data-toggle="validator" data-minlength="6" class="form-control" id="inputPassword" placeholder="Mot de passe" required>
                                  <span class="help-block"> 6 caractères minimum</span>
                                </div>
                                <div class="form-group">
                                  <input type="password" name="password_confirm" class="form-control" id="inputPasswordConfirm" data-match="#inputPassword" data-match-error="oh! oh!, ne correspondent pas" placeholder="confirmez mot de passe" required>
                                  <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <select name="role" id="selector1" class="form-control1">
                                        <option>Choisir le role de l'utilisateur</option>
                                        <option 
            
                                            value="ROLE_USER">Role User

                                        </option>
                                        <option value="ROLE_ADMIN">Role Admin</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-8">
                                        <div class="radio block"><label><input type="radio" value="1" name="enabled" checked=""> Activé</label></div>
                                        <div class="radio block"><label><input type="radio" name="enabled"  value="0"> Desactivé</label></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="submit" name="add_submit" value="Ajouter" class="btn btn-success"/>
                                </div>
                        </form> 
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
</div>
<!-- main content end-->

<?php require dirname(__DIR__) . '/inc/admin_footer.php' ?>
