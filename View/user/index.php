<?php require dirname(__DIR__) . '/inc/admin_header.php' ?>

<!-- main content start-->
<div id="page-wrapper">
	<div class="main-page">
		<h2 class="title1">Utilisateurs</h2>
		<?php require dirname(__DIR__) . '/inc/msg.php' ?>
		<?php if (empty($this->oUsers)): ?>
		    <div class="text-center">
			    <h3 class="well">Aucun utilisateur trouvée.</h3>
			    <p>
			    	<button type="button" onclick="window.location='<?=ROOT_URL?>admin/user/add'" class="btn btn-primary btn-pri">
			    		<i class="fa fa-plus" aria-hidden="true"></i>
			    		Ajouter un utilisateur !
			    	</button>
			    </p>
			</div>
		<?php else: ?>
			<div class="tables">
				<div class="table-responsive bs-example widget-shadow">
					<h4>Liste des utilisateurs:</h4>
					<table class="table table-bordered"> 
						<thead> 
							<tr> 
								<th>#</th> 
								<th>Nom Complet</th> 
								<th>Email</th>
								<th>Role</th>
								<th>Date d'inscription</th> 
								<th>Action</th>
							</tr> 
						</thead> 
						<tbody> 
							<?php foreach ($this->oUsers as $k => $User): ?>
								<tr> 
									<th scope="row"><?=$k + 1?></th> 
									<td><a href="<?=ROOT_URL?>admin/user/show/<?=$User->id?>"><?=ucfirst(htmlspecialchars($User->name))?></a></td> 
									<td><?=$User->email?></td> 
									<td><?=$User->role?></td> 
									<td><?=$User->created_at?></td> 
									<td>
										<?php if(!empty($_SESSION['is_logged'])): ?>

										    <a onclick="window.location='<?=ROOT_URL?>admin/user/edit/<?=$User->id?>'" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a> 
										    <form action="<?=ROOT_URL?>admin/user/delete/<?=$User->id?>" method="post" style="display:inline;"><button type="submit" name="delete" value="1" class="btn btn-danger "><i class="fa fa-trash"></i> Delete</button></form>
										<?php endif ?>
									</td>
								</tr> 
							<?php endforeach ?>
						</tbody> 
					</table> 
				</div>
			</div>

		<?php endif ?>
	</div>
</div>
<!-- main content end-->

<?php require dirname(__DIR__) . '/inc/admin_footer.php' ?>
