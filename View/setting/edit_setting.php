<?php require dirname(__DIR__) . '/inc/admin_header.php' ?>

<!-- main content start-->
<div id="page-wrapper">
    <div class="main-page">
        <div class="forms">
            <h2 class="title1">Paramètres du site</h2>
            <?php require dirname(__DIR__) . '/inc/msg.php' ?>
            <div class=" form-grids row form-grids-right">
                <div class="widget-shadow " data-example-id="basic-forms">
                    <?php if (!isset($this->oSetting) && !empty($this->oSetting)): ?>
                        <div class="form-body text-center">
                            <h3 class="well"> Pas de paramètre disponible.</h3>

                            <p>
                                <button type="button" onclick="window.location='<?=ROOT_URL?>admin/setting/add'" class="btn btn-primary btn-pri">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                    Ajouter les paramètres!
                                </button>
                            </p>
                        </div>
                    <?php else: ?>

                    <div class="form-title">
                        <h4>Infos Bidi Annonces:</h4>
                    </div>
                    <div class="form-body">
                        <form class="form-horizontal" action="" method="post" enctype="multipart/form-data"> 
                            <div class="form-group"> 
                                <label for="name" class="col-sm-2 control-label">Nom du site</label> 
                                <div class="col-sm-9"> 
                                    <input type="text" name="name" class="form-control" value="<?=isset($this->oSetting->name) ? ucfirst(htmlspecialchars($this->oSetting->name)) : ''?>" id="name" required="required"> 
                                </div> 
                            </div>
                            <div class="form-group"> 
                                <label for="slogan" class="col-sm-2 control-label">Slogan</label> 
                                <div class="col-sm-9"> 
                                    <input type="text" name="slogan" class="form-control" value="<?=isset($this->oSetting->slogan) ? ucfirst(htmlspecialchars($this->oSetting->slogan)) : ''?>" id="slogan" required="required"> 
                                </div> 
                            </div>
                            <div class="form-group"> 
                                <label for="description" class="col-sm-2 control-label">description</label> 
                                <div class="col-sm-9"> 
                                    <textarea class="form-control" name="description" rows="5" id="description"><?=isset($this->oSetting->description) ? ucfirst(htmlspecialchars($this->oSetting->description)) : ''?></textarea>
                                </div> 
                            </div>
                            
                            <div class="form-group"> 
                                <label for="address" class="col-sm-2 control-label">Adresse</label> 
                                <div class="col-sm-9"> 
                                    <input type="text" name="address" class="form-control" value="<?=isset($this->oSetting->address) ? ucfirst(htmlspecialchars($this->oSetting->address)) : ''?>" id="address" > 
                                </div> 
                            </div>

                            <div class="form-group"> 
                                <label for="phone" class="col-sm-2 control-label">Téléphone</label> 
                                <div class="col-sm-9"> 
                                    <input type="text" name="phone" class="form-control" value="<?=isset($this->oSetting->phone) ? ucfirst(htmlspecialchars($this->oSetting->phone)) : ''?>" id="phone" > 
                                </div> 
                            </div>

                            <div class="form-group"> 
                                <label for="email" class="col-sm-2 control-label">Email</label> 
                                <div class="col-sm-9"> 
                                    <input type="text" name="email" class="form-control" value="<?=isset($this->oSetting->email) ? ucfirst(htmlspecialchars($this->oSetting->email)) : ''?>" id="email" > 
                                </div> 
                            </div>

                            <div class="form-group"> 
                                <label for="facebook" class="col-sm-2 control-label">Facebook</label> 
                                <div class="col-sm-9"> 
                                    <input type="text" name="facebook" class="form-control" value="<?=isset($this->oSetting->facebook) ? ucfirst(htmlspecialchars($this->oSetting->facebook)) : ''?>" id="facebook" > 
                                </div> 
                            </div>

                            <div class="form-group"> 
                                <label for="twitter" class="col-sm-2 control-label">Twitter</label> 
                                <div class="col-sm-9"> 
                                    <input type="text" name="twitter" class="form-control" value="<?=isset($this->oSetting->twitter) ? ucfirst(htmlspecialchars($this->oSetting->twitter)) : ''?>" id="twitter" > 
                                </div> 
                            </div>

                            <div class="form-group"> 
                                <label for="instagram" class="col-sm-2 control-label">Instagram</label> 
                                <div class="col-sm-9"> 
                                    <input type="text" name="instagram" class="form-control" value="<?=isset($this->oSetting->instagram) ? ucfirst(htmlspecialchars($this->oSetting->instagram)) : ''?>" id="instagram" > 
                                </div> 
                            </div>

                            <div class="form-group"> 
                                <label for="linkedin" class="col-sm-2 control-label">Linkedin</label> 
                                <div class="col-sm-9"> 
                                    <input type="text" name="linkedin" class="form-control" value="<?=isset($this->oSetting->linkedin) ? ucfirst(htmlspecialchars($this->oSetting->linkedin)) : ''?>" id="linkedin" > 
                                </div> 
                            </div>

                            <div class="form-group"> 
                                <label for="google_plus" class="col-sm-2 control-label">Google plus</label> 
                                <div class="col-sm-9"> 
                                    <input type="text" name="google_plus" class="form-control" value="<?=isset($this->oSetting->google_plus) ? ucfirst(htmlspecialchars($this->oSetting->google_plus)) : ''?>" id="google_plus" required="required"> 
                                </div> 
                            </div>
                            <input type="hidden" name="setting_id" value="<?=isset($this->oSetting->id) ? $this->oSetting->id : ''?>">
                            <div class="col-sm-offset-2"> 
                                <input type="submit" name="setting_submit" value="Enregistrer" class="btn btn-info"/>
                            </div> 
                        </form> 
                    </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- main content end-->

<?php require dirname(__DIR__) . '/inc/admin_footer.php' ?>
