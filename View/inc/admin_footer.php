		<!--footer-->
		<div class="footer">
		   <p>&copy; <?= date("Y"); ?> Bidi Annonces. Accedez à <a href="<?=ROOT_URL?>" target="_blank">Acceuil</a></p>
	   </div>
        <!--//footer-->
	</div>
	
	<!-- side nav js -->
	<script src='<?=ROOT_URL?>static/js/SidebarNav.min.js' type='text/javascript'></script>
	<script>
      $('.sidebar-menu').SidebarNav();
    </script>
	<!-- //side nav js -->
	
	<!-- Classie --><!-- for toggle left push menu script -->
		<script src="<?=ROOT_URL?>static/js/classie.js"></script>
		<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script>
	<!-- //Classie --><!-- //for toggle left push menu script -->
	
	<!--scrolling js-->
	<script src="<?=ROOT_URL?>static/js/jquery.nicescroll.js"></script>
	<script src="<?=ROOT_URL?>static/js/scripts.js"></script>
	<!--//scrolling js-->
	
	<!-- Bootstrap Core JavaScript -->
   <script src="<?=ROOT_URL?>static/js/jQuery-2.1.4.min.js"> </script>
   <script src="<?=ROOT_URL?>static/js/bootstrap.js"> </script>
   <script src="<?=ROOT_URL?>static/js/bootstrap3-wysihtml5.all.min.js"> </script>
   <script type="text/javascript">

   	$('.js-datepicker').datepicker({
   	            format: 'dd-mm-yyyy'
   	});


   </script>
   <script >
   (function($){
      
     $('#duplicatebtn').click(function(e){
        e.preventDefault();
        var $clone = $('#duplicate').clone().attr('id', '').removeClass('hidden');
        $('#duplicate').before($clone);
        $('#duplicate').parent().removeClass('hidden');
     });
   })(jQuery);
   </script>
</body>
</html>