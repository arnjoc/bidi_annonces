<?php if (!empty($this->sErrMsg)): ?>
    <div class="alert alert-danger alert-dismissable" style="background-color:#d9534f;color:#fff;">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong><?=$this->sErrMsg?></strong>
    </div>
<?php endif ?>

<?php if (!empty($this->sSuccMsg)): ?>
    <div class="alert alert-success alert-dismissable" style="background-color:#5cb85c;color:#fff;">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong><?=$this->sSuccMsg?></strong>
    </div>
<?php endif ?>
