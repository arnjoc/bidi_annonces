<?php $image_url = !empty($_SESSION['auth']->image_url) ? $_SESSION['auth']->image_url : 'img.png';?>
<!DOCTYPE HTML>
<html>
<head>
<title>Bidi Annonce</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="<?=ROOT_URL?>static/css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="<?=ROOT_URL?>static/css/style.css" rel='stylesheet' type='text/css' />
<!-- font-awesome icons CSS -->
<link href="<?=ROOT_URL?>static/css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons CSS -->
 <!-- side nav css file -->
 <link href='<?=ROOT_URL?>static/css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
 <!-- side nav css file -->
 <!-- js-->
<script src="<?=ROOT_URL?>static/js/jquery-1.11.1.min.js"></script>
<script src="<?=ROOT_URL?>static/js/modernizr.custom.js"></script>
<!--webfonts
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">-->
<!--//webfonts--> 
<!-- Metis Menu -->
<script src="<?=ROOT_URL?>static/js/metisMenu.min.js"></script>
<script src="<?=ROOT_URL?>static/js/custom.js"></script>
<link href="<?=ROOT_URL?>static/css/custom.css" rel="stylesheet">
<link href="<?=ROOT_URL?>static/css/bootstrap3-wysihtml5.min.css" rel="stylesheet">
<link href="<?=ROOT_URL?>static/css/datepicker3.css" rel="stylesheet">
<!--//Metis Menu -->
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
	<div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
		<!--left-fixed -navigation-->
		<aside class="sidebar-left">
      <nav class="navbar navbar-inverse">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".collapse" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <h1><a class="navbar-brand" href="<?=ROOT_URL?>admin"><span class="fa fa-area-chart"></span> Bidi<span class="dashboard_text">Annonces</span></a></h1>
          </div>
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="sidebar-menu">
              <li class="header">Menu Principal</li>
              <li class="treeview">
                <a href="<?=ROOT_URL?>admin">
                <i class="fa fa-dashboard"></i> <span>Tableau de Bord</span>
                </a>
              </li>

              <li class="treeview">
                <a href="#">
                <i class="fa fa-list"></i> <span>Catégories D'annonces</span>
                <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                  <li><a href="<?=ROOT_URL?>admin/category/add"><i class="fa fa-angle-right"></i> nouvelle categorie</a></li>
                  <li><a href="<?=ROOT_URL?>admin/category/all"><i class="fa fa-angle-right"></i> Liste des categories </a></li>
                </ul>
              </li>

              <li class="treeview">
                <a href="#">
                <i class="fa fa-list-alt"></i> <span>Catégories de Publicités</span>
                <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                  <li><a href="<?=ROOT_URL?>admin/pubcategory/add"><i class="fa fa-angle-right"></i> nouvelle categorie</a></li>
                  <li><a href="<?=ROOT_URL?>admin/pubcategory/all"><i class="fa fa-angle-right"></i> Liste des categories </a></li>
                </ul>
              </li>
              
              <li class="treeview">
                <a href="#">
                <i class="fa fa-th"></i> <span>Annonces</span>
                <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                  <li><a href="<?=ROOT_URL?>admin/advert/add"><i class="fa fa-angle-right"></i> Nouvelle annonce</a></li>
                  <li><a href="<?=ROOT_URL?>admin/advert/all"><i class="fa fa-angle-right"></i> Liste des annonces </a></li>
                </ul>
              </li>

              <li class="treeview">
                <a href="#">
                <i class="fa fa-bullhorn"></i> <span>Publicités</span>
                <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                  <li><a href="<?=ROOT_URL?>admin/pub/add"><i class="fa fa-angle-right"></i> Nouvelle Publicité</a></li>
                  <li><a href="<?=ROOT_URL?>admin/pub/all"><i class="fa fa-angle-right"></i> Liste des Publicités </a></li>
                </ul>
              </li>

              <li class="treeview">
                <a href="#">
                <i class="fa fa-users"></i> <span>Utilisateurs</span>
                <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                  <li><a href="<?=ROOT_URL?>admin/user/add"><i class="fa fa-angle-right"></i> Ajouter un Utilisateur</a></li>
                  <li><a href="<?=ROOT_URL?>admin/user/all"><i class="fa fa-angle-right"></i> Liste des Utilisateurs </a></li>
                </ul>
              </li>
              <li class="treeview">
                <a href="#">
                <i class="fa fa-envelope"></i> <span>Messages</span>
                <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                  <li><a href="<?=ROOT_URL?>admin/contact/index"><i class="fa fa-angle-right"></i> Boite de réception</a></li>
                </ul>
              </li>
              <li class="header">Paramètres</li>
              <li class="treeview">
                <a href="<?=ROOT_URL?>admin/setting/add">
                  <i class="fa fa-cogs"></i> <span>Info du site</span>
                </a>
              </li>
            </ul>
          </div>
          <!-- /.navbar-collapse -->
      </nav>
    </aside>
	</div>
		<!--left-fixed -navigation-->
		
		<!-- header-starts -->
		<div class="sticky-header header-section ">
			<div class="header-left">
				<!--toggle button start-->
				<button id="showLeftPush"><i class="fa fa-bars"></i></button>
				<!--notification menu end -->
        <div class="profile_details_left"><!--notifications of menu start -->
            <ul class="nofitications-dropdown">
              <li class="dropdown head-dpdn">
                <a href="<?=ROOT_URL?>" target="blank" class="dropdown-toggle" style="color:#fff;"><i class="fa fa-home"></i><span class="badge" style="background: transparent;"></span> </a>
              </li>
            </ul>
            <div class="clearfix"> </div>
        </div>
				<div class="clearfix"> </div>
			</div>
			<div class="header-right">
				<!--search-box-->
				<div class="search-box">
					
				</div><!--//end-search-box-->
				
				<div class="profile_details">   
            <ul>
              <li class="dropdown profile_details_drop">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                  <div class="profile_img"> 
                    <span class="prfil-img"><img src="<?=ROOT_URL?>static/images/user/<?=$image_url?>" alt="" height="40"> </span> 
                    <div class="user-name">
                      <p><?= $_SESSION['auth']->username ?></p>
                      <span>Administrateur</span>
                    </div>
                    <i class="fa fa-angle-down lnr"></i>
                    <i class="fa fa-angle-up lnr"></i>
                    <div class="clearfix"></div>  
                  </div>  
                </a>
                <ul class="dropdown-menu drp-mnu">
                  <li> <a href="<?=ROOT_URL?>admin/user/show/<?=$_SESSION['auth']->id?>"><i class="fa fa-suitcase"></i> Profile</a> </li> 
                  <li> <a href="<?=ROOT_URL?>admin/security/logout"><i class="fa fa-sign-out"></i> Deconnexion</a> </li>
                </ul>
              </li>
            </ul>
        </div>
				<div class="clearfix"> </div>				
			</div>
			<div class="clearfix"> </div>	
		</div>
		<!-- //header-ends -->