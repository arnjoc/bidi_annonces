<?php require SITE_ROOT.'/view/public/inc/header.php';?>
<?php require SITE_ROOT.'/view/public/inc/banner.php';?>

<!--breadcrumb start-->
<?php require SITE_ROOT.'/view/public/inc/breadcrumb.php';?>
<!--breadcrumb end-->

<!-- Category list start-->
<div class="car-loan-mid w3l" style="padding-bottom: 48px;">
	<div class="container">
		<div class="row Introduction" style="padding-bottom:40px; ">
			<div class="col-md-offset-1 col-md-10 text-center">
				<h1> Foie aux questions</h1>
			</div>
		</div>
	    <div class="row">
		    <div class="col-md-offset-1 col-md-10">
			    <div class="panel-group collapse-style-1" id="accordion">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
									<i class="fa fa-info-circle pr-10"></i> Comment poster des annonces?
								</a>
							</h4>
						</div>
						<div id="collapseOne" class="panel-collapse collapse in">
							<div class="panel-body">
								Avant de poster une annonce, il vous faut d'abord vous inscrire et valider votre inscription. Ensuite a l'acceuil, accédez à votre espace utilisateur(<b>Profile</b>) en cliquant sur la liste déroulante apres un click sur votre pseudo en haut a droite de l'écran. Ensuite dans le menu à droite, cliquez sur <b>Ajouter une annonce</b>. Un formulaire s'affiche, vous entrez les informations de votre annonce puis vous validez.
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed">
									<i class="fa fa-info-circle pr-10"></i> Comment changer mon mot de passe?
								</a>
							</h4>
						</div>
						<div id="collapseTwo" class="panel-collapse collapse">
							<div class="panel-body">
								Allez dans votre espace utilisateur(<b>Profile</b>) puis dans le menu à droite, cliquez sur <b>changer mot de passe</b>. Un formulaire s'affiche, vous entrez dans les deux premiers champs le nouveau mot de passe puis l'ancien dans le dernier champs.
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="collapsed">
									<i class="fa fa-info-circle pr-10"></i> Mon annonce ne s'affiche pas
								</a>
							</h4>
						</div>
						<div id="collapseThree" class="panel-collapse collapse">
							<div class="panel-body">
								Allez dans votre espace utilisateur(<b>Profile</b>)  puis dans <b>mes annonces</b>. dans la liste, cliquez sur le bouton d'édition de l'annonce, ce qui vous affiche le formulaire. Une fois sur le formulaire, assurez vous que la case </b>Publié</b> est cochée.
							</div>
						</div>
					</div>
				</div>
		    </div>
	    </div>
	</div>
</div>
<!-- Category list end-->
<?php require SITE_ROOT.'/view/public/inc/footer.php';?>