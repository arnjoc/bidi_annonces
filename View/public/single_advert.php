<?php require SITE_ROOT.'/view/public/inc/header.php';?>
<?php require SITE_ROOT.'/view/public/inc/banner.php';?>

<!--breadcrumb start-->
<?php require SITE_ROOT.'/view/public/inc/breadcrumb.php';?>
<!--breadcrumb end-->

<!--adverts listing start-->
<div class="single w3ls">
    <div class="container">
        <div class="col-md-9 single-left">
            <div class="sample1">
                <div class="carousel" style="height: 283px;">
                    <ul>
                        <li class=""> <img src="<?=ROOT_URL?>static/images/advert/<?=$this->oSingleAdvert->image_url?>" alt="quibusdam et aut offi" height="283" style="width:50%;margin:0 auto;">
                            <div class="caption1    "><span><?=$this->oSingleAdvert->title?></span></div>
                        </li>
                    </ul>
                    <div class="controls">
                        <div class="prev"></div>
                        <div class="next"></div>
                    </div>
                </div>
            </div>
            <div class="single-left2">
                <h3><?=$this->oSingleAdvert->title?></h3>
                <ul class="com">
                    <li><span class="glyphicon glyphicon-user" aria-hidden="true"></span><a href="javascript:;"><?=$this->oSingleAdvert->author_name?></a></li>
                    <li><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span><a href="javascript:;"><?= date("d M Y", strtotime($v->created_at))?></b></a></li>
                    <li><span class="glyphicon glyphicon-tag" aria-hidden="true"></span><a href="<?=ROOT_URL?>category/<?=$this->oSingleAdvert->category_slug?>"><?=$this->oSingleAdvert->category_name?></b></a></li>
                    <li><a data-toggle="modal" href="#author-details" class="btn btn-default author-details">Contacts de l'annonceur</a></li>
                </ul>
            </div>
            <div class="single-left3">
                <p><?=$this->oSingleAdvert->content ?></p>
            </div>
            <div class="single-left4">
                <h4>Partager cette annonce</h4>
                <ul class="social-icons social-icons1">
                    <li><a href="#" class="icon icon-border icon-border1 facebook"></a></li>
                    <li><a href="#" class="icon icon-border icon-border1 twitter"></a></li>
                    <li><a href="#" class="icon icon-border icon-border1 instagram"></a></li>
                    <li><a href="#" class="icon icon-border icon-border1 pinterest"></a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-3 single-right">
            <div class="blo-top">
                <div class="tech-btm one">
                    <a href="//<?=$this->oSinglePub->url?>"><img src="<?=ROOT_URL?>static/images/pub/<?=$this->oSinglePub->image_url?>" class="img-responsive" alt=""></a>
                    <span class="price">PUB</span>
                </div>
            </div>
            <div class="blo-top1">
                <div class="agileits_twitter_posts">
                    <h4> Catégories</h4>
                    <ul>
                        <?php foreach ($this->oCategories as $k => $v): ?>
                            <li><i class="fa fa-angle-double-right" aria-hidden="true"></i><a href="<?=ROOT_URL?>category/<?=$v->slug?>"><?=$v->name?></a></li>
                        <?php endforeach ?>
                        
                    </ul>
                </div>
            </div>
            <div class="blo-top1 w3l">
                <div class="tech-btm">
                    <h4>Annonces Similaires</h4>
                    <?php foreach ($this->oRelativeAdverts as $k => $v): ?>
                    <?php if ($k < 6 and $v->id != $this->oSingleAdvert->id): ?>
                        <div class="blog-grids">
                            <div class="blog-grid-left">
                                <a href="<?=ROOT_URL?>advert/<?= $v->slug?>"><img src="<?=ROOT_URL?>static/images/advert/<?= $v->image_url?>" class="img-responsive" alt=""></a>
                            </div>
                            <div class="blog-grid-right">
                                <h5><a href="<?=ROOT_URL?>advert/<?= $v->slug?>"><?= $v->title?></a> </h5>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    <?php endif ?>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
        <div class="clearfix"> </div>
    </div>

    <div id="author-details" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Contacts de l'annonceur</h4>
              </div>
              <div class="modal-body">
                <p style="font-size: 1em;"><i class="fa fa-envelope"></i> <?=!empty($this->oSingleAdvert->author_email) ? $this->oSingleAdvert->author_email: 'Non renseigné' ?></p>
                <p style="font-size: 1em;"><i class="fa fa-phone"></i> <?=!empty($this->oSingleAdvert->author_phone) ? $this->oSingleAdvert->author_phone: 'Non renseigné' ?></p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
        </div>
    </div>
</div>
<!--adverts listing end-->
<?php require SITE_ROOT.'/view/public/inc/footer.php';?>