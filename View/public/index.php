<?php require SITE_ROOT.'/view/public/inc/header.php';?>
<?php require SITE_ROOT.'/view/public/inc/index_header.php';?>

<!-- Category list start-->
<div class="car-loan-mid w3l" style="padding-bottom: 48px;">
	<div class="container">
	    <h3 class="tittle">Catégories d'annonces</h3>
	    <div class="separator"></div>
	    <div class="categories">
	        <?php foreach ($this->oCategories as $k => $v): ?>
	        <?php if ($k < 4): ?>
	        <div class="col-md-3 focus-grid">
	            <a href="<?= ROOT_URL?>category/<?=$v->slug?>">
	                <div class="focus-border">
	                    <div class="focus-layout">
	                        <div class="focus-image"><i class="fa fa-<?=!empty($v->icon) ? $v->icon : 'tag' ?>"></i></div>
	                        <h4 class="clrchg"><?= $v->name?></h4>
	                    </div>
	                </div>
	            </a>
	        </div>
	        <?php endif ?>
	        <?php endforeach ?>
	        <div class="clearfix"></div>
	        <div class="serch-button">
	            <a href="<?=ROOT_URL?>categories" class="btn btn-link">Plus de catégories <i class="fa fa-angle-double-right"></i></a>
	        </div>
	    </div>
	</div>
</div>
<!-- Category list end-->

<!-- recent adverts slider start -->
<div class="slider1" style="background-color:#f9f9f9; padding-top:3em;padding-bottom:3em;">
	<div class="container">
		<h3 class="tittle">Récentes Annonces</h3>
		<div class="separator"></div>
		<div class="row">
			<?php foreach ($this->oAdverts as $k => $v): ?>
			<?php if ($k < 8): ?>
				<div class="col-md-3">
					<div class="product-item">
					    <div class="pi-img-wrapper">
					        <img src="<?= ROOT_URL?>static/images/advert/<?= $v->image_url?>" class="img-responsive" alt="<?= $v->title?>">
					        <div>
					            <a href="<?= ROOT_URL?>advert/<?= $v->slug?>" class="btn btn-default fancybox-button"><i class="fa fa-link"></i></a>
					        </div>
					    </div>
					    <h3><a href="<?= ROOT_URL?>advert/<?= $v->slug?>"><?= $v->title?></a></h3>
					    <div class="pi-price">
					    	<?= $v->currency=='cfa'?'':$v->currency ?><?= $v->price?>
					    	<?= $v->currency=='cfa'? $v->currency :'' ?>	
					    </div>
					    <a href="<?= ROOT_URL?>advert/<?= $v->slug?>" class="btn btn-default add2cart">Détails</a>
					</div>
				</div>
			<?php endif ?>
	        <?php endforeach ?>
		</div>
	</div>
</div>
<!-- recent adverts slider end -->

<!-- recent adverts slider start -->
<div class="tips w3l" style="">
    <div class="container">
    	<div class="col-md-8 col-md-offset-2">
	        <div class="news-grid">
	            <div class="news-img">
	                <a href="//<?=$this->oSinglePub->url?>"> <img src="<?=ROOT_URL?>static/images/pub/<?=$this->oSinglePub->image_url?>" alt=" " class="img-responsive"></a>
	                <span class="price1">PUBLICITE</span>
	            </div>
	            <div class="news-text">
	                <h3><a href="//<?=$this->oSinglePub->url?>" onclick="confirm('Vous allez être redirigé vers: <?=$this->oSinglePub->url?>')" target="_blank"><?=$this->oSinglePub->title?></a></h3>
	                <ul class="news">
	                    <li><i class="fa fa-tags" aria-hidden="true"></i> <a href="javascript:void(0)"> <?=$this->oSinglePub->category_name?></a></li>
	                </ul>
	                <p style="padding-bottom:1em;"><?=nl2br(htmlspecialchars(mb_strimwidth($this->oSinglePub->content, 0, 100, '...')))?></p>
	                <a href="//<?=$this->oSinglePub->url?>" class="read hvr-shutter-in-horizontal" onclick="confirm('Vous allez être redirigé vers: <?=$this->oSinglePub->url?>')">En savoir plus</a>
	            </div>
	            <div class="clearfix"></div>
	        </div>
        </div>
    </div>
</div>
<!-- recent adverts slider end -->
<?php require SITE_ROOT.'/view/public/inc/footer.php';?>