<?php require SITE_ROOT.'/view/public/inc/header.php';?>
<?php require SITE_ROOT.'/view/public/inc/banner.php';?>

<!--breadcrumb start-->
<?php require SITE_ROOT.'/view/public/inc/breadcrumb.php';?>
<!--breadcrumb end-->

<!-- Category list start-->
<div class="car-loan-mid w3l" style="padding-bottom: 48px;">
	<div class="container">
	    <div class="row">
		    <div class="col-md-offset-4 col-md-4 text-center">
			    <div class="register-form " style="margin-top: -50%;padding-top:30px;">
			    	<h1 style="padding-bottom: 30px;">Connexion</h1>
			    	<!--flash message start-->
			    	<?php require SITE_ROOT.'/view/inc/msg.php';?>
			    	<!--flash message end-->
			        <form class="form-horizontal" role="form" action="" method="post">
			            <div class="form-group has-feedback">
			                <div class="">
			                    <input type="email" name="email" class="form-control" id="email" placeholder="Email" required="" value="<?=isset($_POST['email']) ? $_POST['email'] : ''; ?>">
			                    <i class="fa fa-envelope form-control-feedback"></i>
			                </div>
			            </div>
			            <div class="form-group has-feedback">
			                <div class="">
			                    <input type="password" class="form-control" id="password" placeholder="Password" name="password" required="" >
			                    <i class="fa fa-lock form-control-feedback"></i>
			                </div>
			            </div>
			            <div class="form-group">
			                <div class="" style="padding-top: 30px;">
			                    <button type="submit" name="sign_up" value="S'inscris" class="btn btn-group btn-lg btn-default btn-animated">Connexion &nbsp;<i class="fa fa-check"></i></button>
			                </div>
			            </div>
			            <div class="clearfix"></div>
			            <div class="text-primary text-left" style="padding-top: 15px;">
			            	<a href="javascript:void">Oublié mot de passe</a>
			            </div>
			        </form>
			    </div>
		    </div>
	    </div>
	</div>
</div>
<!-- Category list end-->
<?php require SITE_ROOT.'/view/public/inc/footer.php';?>