<?php require SITE_ROOT.'/view/public/inc/header.php';?>
<?php require SITE_ROOT.'/view/public/inc/banner.php';?>
<!--breadcrumb start-->
<?php require SITE_ROOT.'/view/public/inc/breadcrumb.php';?>
<!--breadcrumb end-->
<?php $image_url = !empty($this->oUser->image_url) ? $this->oUser->image_url : 'img.png';?>
<!-- Category list start-->
<div class="car-loan-mid w3l" style="padding-bottom: 48px;">
    <div class="container">
        <div class="row">
            <div class="col-md-8 profile-content">
            	<?php if($this->pToggle == 'show'): ?>
                <div class="my-profile">
                	<h2 class="text-center profile-head">Mon Profile</h2>
                	<div class="text-center">
	                	<div class="">
							<img src="<?=ROOT_URL?>static/images/user/<?=$image_url?>" alt=" " class="img-responsive">
							<div class="row">
								<div class="avatar  col-md-4 col-md-offset-4" style="padding-top:1em;">
									<div class="avatar-form hidden">
										<form action="<?=ROOT_URL?>avatarchange/<?= $this->oUser->id?>" method="post" enctype="multipart/form-data">
											<label class="avatarInputField">
												<span><i class="fa fa-cloud-upload"></i> cliquez ici</span>
												<input type="file" name="avatar" >
											</label>
											<div style="padding-top: 1em;">
											<button type="submit" class="btn btn-success btn-sm">Valider</button>
											</div>
										</form>
									</div>
									<button class="btn btn-default btn-sm" id="change-avatar">changer mon avatar</button>
								</div>
							</div>
							<h5><?= $this->oUser->name?></h5>
							<div class="profile-details">
								<div class="row">
									<div class="col-md-6 col-md-offset-3">
										<p><b>Email: </b> <?= $this->oUser->email?></p>
										<p><b>Pays: </b> <?= $this->oUser->country?></p>
										<p><b>Ville: </b> <?= $this->oUser->town?></p>
									</div>
								</div>
							</div>
						</div>
					</div>
                </div>
            	<?php elseif($this->pToggle == 'edit'): ?>
                <div class="profile-edit">
                	<h2 class="text-center profile-head">Edition de profile</h2>
                	<?php require SITE_ROOT . '/View/inc/msg.php' ?>
                	<div class="row">
	                	<div class="col-md-10 col-md-offset-1">
		                    <form class="form-horizontal"  action="" method="post">
		                            <div class="form-group">
		                            	<label for="name">Nom complet:</label>
		                                <input type="text" name="name" class="form-control" id="name" value="<?=isset($this->oUser->name) ? ucfirst(htmlspecialchars($this->oUser->name)) : ''?>" placeholder="Nom complet" required>
		                            </div>
		                            <div class="form-group has-feedback">
		                            	<label for="country">Pays:</label>
		                                <input type="text" name="country" class="form-control" id="country" value="<?=isset($this->oUser->country) ? ucfirst(htmlspecialchars($this->oUser->country)) : ''?>" placeholder="country" data-error="Entrez votre pays" required>
		                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
		                            </div>
		                            <div class="form-group has-feedback">
		                            	<label for="town">Ville:</label>
		                                <input type="text" name="town" class="form-control" id="town" value="<?=isset($this->oUser->town) ? ucfirst(htmlspecialchars($this->oUser->town)) : ''?>" placeholder="votre ville" data-error="Entrez votre ville" required>
		                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
		                            </div>
		                            <div class="form-group has-feedback">
		                            	<label for="tel">Téléphone:</label>
		                                <input type="text" name="phone" class="form-control" id="tel" value="<?=isset($this->oUser->phone) ? ucfirst(htmlspecialchars($this->oUser->phone)) : ''?>" placeholder="numéro de téléphone" required>
		                            </div>
		                            <div class="form-group">
		                            	<label for="password">Mot de passe:</label>
		                              <input type="password" name="password" data-toggle="validator" data-minlength="6" class="form-control" id="password" placeholder="Mot de passe" required>
		                            </div>
		                            <div class="form-group">
		                                <input type="submit" name="edit_profile" value="Sauver" class="btn btn-info btn-lg"/>
		                            </div>
		                    </form> 
	                	</div>
                	</div>
                </div>
            	<?php elseif($this->pToggle == 'pass'): ?>
                <div class="profile-pass">
                	<h2 class="text-center profile-head">Chagement de mot de passe</h2>
                	<?php require SITE_ROOT . '/View/inc/msg.php' ?>
                	<div class="row">
	                	<div class="col-md-10 col-md-offset-1">
		                    <form class="form-horizontal"  action="" method="post">
		                           
		                            <div class="form-group">
		                            	<label for="password_new">Nouveau Mot de passe:</label>
		                              <input type="password" name="password_new" data-toggle="validator" data-minlength="6" class="form-control" id="password_new" placeholder="Nouveau" required>
		                            </div>
		                            <div class="form-group">
		                            	<label for="password_new_confirm">Confirmez nouveau mot de passe:</label>
		                              <input type="password" name="password_new_confirm" data-toggle="validator" data-minlength="6" class="form-control" id="password_new_confirm" placeholder="Nouveau" required>
		                            </div>
		                            <div class="form-group">
		                            	<label for="password_old">Ancien mot de passe:</label>
		                              <input type="password" name="password_old" data-toggle="validator" data-minlength="6" class="form-control" id="password_old" placeholder="ancien" required>
		                            </div>
		                            <div class="form-group" style="padding-top: 2em;">
		                                <input type="submit" name="edit_pass_profile" value="Enregistrez" class="btn btn-info btn-lg"/>
		                            </div>
		                    </form> 
	                	</div>
                	</div>
                </div>
            	<?php elseif($this->pToggle == 'advert_add'): ?>
                <div class="profile-add-advert">
                	<h2 class="text-center profile-head">Ajouter une annonce</h2>
                	<?php require SITE_ROOT . '/View/inc/msg.php' ?>
                	<div class="row">
	                	<div class="">
		                    <form class="form-horizontal"  action="" method="post" enctype="multipart/form-data"> 
		                        <div class="form-group"> 
		                            <label for="title" class="col-sm-2 control-label">Titre de l'annonce</label> 
		                            <div class="col-sm-8"> 
		                                <input type="text" name="title" class="form-control" id="title" required="required" value="<?=isset($_POST['title']) ? $_POST['title'] : ''; ?>"> 
		                            </div> 
		                        </div> 
		                        <div class="form-group">
		                            <label for="title" class="col-sm-2 control-label">Prix</label>
		                            <div class="col-sm-6"> 
		                                <input type="text" name="price" class="form-control" id="title" required="required" value="<?=isset($_POST['price']) ? $_POST['price'] : ''; ?>"> 
		                            </div> 
		                            <div class="col-sm-2">
			                              <select class="form-control" name="currency" required="required">
			                                <option>-Devise-</option>
			                                <option value="cfa">CFA</option>
			                                <option value="€">€</option>
			                                <option value="$">USD</option>
			                              </select> 
		                            </div>
		                        </div>
		                        <div class="form-group"> 
		                            <label for="summary" class="col-sm-2 control-label">Contenu</label> 
		                            <div class="col-sm-8"> 
		                                <textarea class="form-control" name="content" rows="5" id="content"><?=isset($_POST['content']) ? $_POST['content'] : ''; ?></textarea>
		                            </div> 
		                        </div>
		                        <div class="form-group"> 
		                            <label for="location" class="col-sm-2 control-label">Lieu de l'annonce</label>
		                            <div class="col-sm-8"> 
		                                <input type="text" name="location" class="form-control" id="location" required="required" value="<?=isset($_POST['location']) ? $_POST['location'] : ''; ?>"> 
		                            </div> 
		                        </div>  
		                        <div class="form-group">
		                            <label for="category" class="col-sm-2 control-label">Categorie:</label>
		                            <div class="col-sm-8">
		                                <select class="form-control" name="category_id" id="category">
		                                	<option>Choisir une categorie</option>
		                                    <?php if (!empty($this->categoryList)): ?>

		                                        <?php foreach($this->categoryList as $category): ?>
		                                            <option value="<?= $category->id ?>">
		                                                <?= $category->name ?> 
		                                            </option>
		                                        
		                                        <?php endforeach ?>
		                                    <?php endif ?>
		                                </select>
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <label for="type" class="col-sm-2 control-label">Type d'annonce:</label>
		                            <div class="col-sm-8">
		                                <select class="form-control" name="type" id="type">
		                                  <option>Choisir un type</option>
		                                  <option value="standard">standard</option>
		                                  <option value="sponsored">sponsorisée</option>
		                                </select>
		                            </div>
		                        </div>

		                        <div class="form-group"> 
		                            <div class="col-md-offset-2"> 
		                                <label for="image"></label> 
		                                <input type="file" name="image" id="image">
		                            </div>
		                        </div>
		                        <div class="form-group">
	                                <label for="images" class="col-md-2" >Plus d'images</label>
	                                <div class="col-md-4 hidden">
	                                    <input type="file" name="images[]" class="hidden" id="duplicate">
	                                </div>
                                </div>

	                            <div class="col-sm-offset-2"> 
	                                <a href="#" class="btn btn-success btn-xs" id="duplicatebtn">Ajouter image</a>
	                            </div><br><br>

		                        <div class="form-group"> 
		                        	<label for="status" class="col-sm-2 control-label">Mise en ligne</label>
		                            <div class="col-sm-8"> 
		                            	<div class="radio block">
		                                    <label class="radio-inline"> 
		                                        <input type="radio" name="status" value="1" checked> Publier 
		                                    </label>
		                                </div>
		                                <div class="radio block">
		                                    <label class="radio-inline">
		                                        <input type="radio" name="status" value="0"> Ne pas Publier 
		                                    </label>
		                            	</div> 
		                            </div> 
		                        </div>
		                        
		                        <div class="col-sm-offset-2"> 
		                            <input type="submit" name="add_submit" value="Ajouter" class="btn btn-success"/>
		                        </div> 
		                    </form>  
	                	</div>
                	</div>
                </div>
            	<?php elseif($this->pToggle == 'advert_edit'): ?>
                <div class="profile-edit-advert">
                	<h2 class="text-center profile-head">Editer une annonce</h2>
                	<?php require SITE_ROOT . '/View/inc/msg.php' ?>
                	<div class="row">
	                	<div class="">
		                    <form class="form-horizontal" action="" method="post" enctype="multipart/form-data"> 
                            <div class="form-group"> 
                                <label for="title" class="col-sm-2 control-label">Titre</label> 
                                <div class="col-sm-8"> 
                                    <input type="text" name="title" class="form-control" value="<?=htmlspecialchars($this->oAdvert->title)?>" id="title" required="required"> 
                                </div> 
                            </div> 
                            <div class="form-group">
		                            <label for="title" class="col-sm-2 control-label">Prix</label>
		                            <div class="col-sm-6"> 
		                                <input type="text" name="price" class="form-control" id="title" required="required" value="<?=htmlspecialchars($this->oAdvert->price)?>"> 
		                            </div> 
		                            <div class="col-sm-2">
			                              <select class="form-control" name="currency" required="required">
			                                <option>-Devise-</option>

			                                <option 
			                                	<?=$this->oAdvert->currency == 'cfa' ? 'selected' : ''?>
			                                	value="cfa">CFA</option>
			                                <option 
			                                	<?=$this->oAdvert->currency == '€' ? 'selected' : ''?>
			                                	value="€">€</option>
			                                <option 
												<?=$this->oAdvert->currency == '$' ? 'selected' : ''?>
			                                	value="$">USD</option>
			                              </select> 
		                            </div>
		                        </div>
                            <div class="form-group"> 
                                <label for="content" class="col-sm-2 control-label">Contenu</label> 
                                <div class="col-sm-8"> 
                                    <textarea class="form-control" name="content" rows="5" id="content"><?=htmlspecialchars($this->oAdvert->content)?></textarea>
                                </div> 
                            </div>
							<div class="form-group"> 
                                <label for="location" class="col-sm-2 control-label">Lieu de l'annonce</label>
                                <div class="col-sm-8"> 
                                    <input type="text" name="location" class="form-control" id="location" required="required" value="<?=htmlspecialchars($this->oAdvert->location)?>"> 
                                </div> 
                            </div>
                            <div class="form-group">
                                <label for="category" class="col-sm-2 control-label">Categorie:</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="category_id" id="category">
                                        <option>Choisir une categorie</option>
                                        <?php if (!empty($this->categoryList)): ?>

                                            <?php foreach($this->categoryList as $category): ?>
                                                <option 
                                                    <?php if ($category->id == $this->oAdvert->category_id): ?> 
                                                         selected="selected"
                                                    <?php endif ?>
                                                    value="<?= $category->id ?>">
                                                    <?= $category->name ?> 
                                                </option>
                                            
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="type" class="col-sm-2 control-label">Type d'annonce:</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="type" id="type">
                                      <option>Choisir un type</option>
                                      <option 
                                            <?php if( $this->oAdvert->type == 'standard'): ?>
                                                selected
                                            <?php endif ?>
                                            value="standard">standard
                                      </option>
                                      <option 
                                            <?php if( $this->oAdvert->type == 'sponsored'): ?>
                                                selected
                                            <?php endif ?>
                                            value="sponsored">sponsorisée
                                      </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group"> 
                                <div class="col-md-offset-2 col-md-6"> 
                                    <label for="image"></label> 
                                    <input type="file" name="image" id="image">
                                </div>
                                <div class="col-md-4" style="padding-top:1.2em;"> 
                                    <?php if( $this->oAdvert->image_url): ?>
                                        <img src="<?= ROOT_URL . 'static/images/advert/'.$this->oAdvert->image_url?>" alt="" height="120" width="120" class="img-thumbnail">
                                    <?php endif ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="images" class="col-md-2" >Plus d'images</label>
                                <div class="col-md-4 hidden">
                                    <input type="file" name="images[]" class="hidden" id="duplicate">
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <?php foreach($this->oAdvertImages as $image): ?>
                                         <div class="col-md-3" style="position: relative;">
                                            <img src="<?= ROOT_URL . 'static/images/advert/'.$image->url?>" alt="" class="img-thumbnail">
                                            	<a onclick="window.location='<?=ROOT_URL?>deleteAdvertImage/<?=$image->id?>/<?=$this->oAdvert->slug?>'" class="btn btn-danger btn-xs" style="position:absolute;top: 0;right: 0;"><i class="fa fa-trash"></i></a>
                                         </div>
                                        <?php endforeach ?>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-offset-2"> 
                                <a href="#" class="btn btn-success btn-xs" id="duplicatebtn">Ajouter image</a>
                            </div><br><br>
                            <div class="form-group"> 
                                <label for="status" class="col-sm-2 control-label">Mise en ligne</label>
                                <div class="col-sm-8"> 
                                    <div class="radio block">
                                        <label class="radio-inline"> 
                                            <input type="radio" name="status" value="1" 
                                            <?php if( $this->oAdvert->status == '1'): ?>
                                                checked
                                            <?php endif ?>> Publier  
                                        </label>
                                    </div>
                                    <div class="radio block">
                                        <label class="radio-inline">
                                            <input type="radio" name="status" value="0"
                                            <?php if( $this->oAdvert->status == '0'): ?>
                                                checked
                                            <?php endif ?>> Ne pas Publier  
                                        </label>
                                    </div> 
                                </div> 
                            </div> 
                            <div class="col-sm-offset-2"> 
                                <input type="submit" name="edit_submit" value="Editer" class="btn btn-info"/>
                            </div> 
                        </form> 
	                	</div>
                	</div>
                </div>
                <?php elseif($this->pToggle == 'advert_list'): ?>
                <div class="profile-my-adverts">
                	<h2 class="text-center profile-head">Mes annonces</h2>
                	<?php require SITE_ROOT . '/View/inc/msg.php' ?>
                	<div class="row">
	                	<div class="">
	                    	<div class="table-responsive">
	                    	  <table class="table table-hover table-condensed">
	                    	      <thead>
	                    	        <tr> 
	                    	        	<th>#</th> 
	                    	        	<th>Titre</th> 
	                    	        	<th>Categorie</th> 
	                    	        	<th>Status</th> 
	                    	        	<th>Date d'ajout</th> 
	                    	        	<th>Action</th>
	                    	        </tr> 
	                    	      </thead>
	                    	      <tbody>
	                    	        <?php foreach ($this->oAdverts as $k => $oAdvert): ?>
	                    	        	<tr> 
	                    	        		<th scope="row"><?=$k + 1?></th> 
	                    	        		<td><a href="<?=ROOT_URL?>useradvertshow/<?=$oAdvert->slug?>"><?=nl2br(htmlspecialchars(mb_strimwidth($oAdvert->title, 0, 25, '...')))?></a></td>
	                    	        		<td><?=$oAdvert->category_name?></td> 
	                    	        		<td>
	                    	        			<?= !empty($oAdvert->status) ? "<span class='label label-success'>Publié <i class='fa fa-check'></i> </span>" : "<span class='label label-primary'>Non-publié <i class='fa fa-close'></i> </span>"; ?>
	                    	        		</td> 
	                    	        		<td><?=date("d M Y", strtotime($oAdvert->created_at))?></td> 
	                    	        		<td>
	                    	        			<?php if(!empty($_SESSION['is_logged'])): ?>

	                    	        			    <a onclick="window.location='<?=ROOT_URL?>advertedit/<?=$oAdvert->slug?>'" class="btn btn-default btn-xs" title="Editer"><i class="fa fa-edit"></i></a> 
	                    	        			    <form action="<?=ROOT_URL?>userAdvertDelete" method="post" style="display:inline;"><button type="submit" name="delete" value="<?=$oAdvert->id?>" class="btn btn-danger btn-xs" style="display:inline;" onclick="confirm('Etes vous sure de supprimer')"><i class="fa fa-trash" title="Supprimer"></i></button></form>
	                    	        			<?php endif ?>
	                    	        		</td>
	                    	        	</tr> 
	                    	        <?php endforeach ?>
	                    	      </tbody>
	                    	    </table>
	                    	</div>
	                	</div>
                	</div>
                </div>
            	<?php elseif($this->pToggle == 'advert_show'): ?>
                <div class="profile-show-advert">
                	<h2 class="text-center profile-head"><?=$this->oAdvert->title?></h2>
                	<div class="row">
	                	<div class="col-md-8 col-md-offset-2 text-center">
		                    <img src="<?= ROOT_URL.'static/images/advert/'.$this->oAdvert->image_url?>">
		                    <div><br> 
		                    	<span><i class="fa fa-tag"></i> <?=$this->oAdvert->category_name?></span>&nbsp;
		                    	<span> <i class="fa fa-calendar"></i> <?=date("d M Y",strtotime($this->oAdvert->created_at))?></span>
		                    </div><br>
		                    
		                    <p class="text-muted"> <?=$this->oAdvert->summary?></p><br>
		                    <p> <?=$this->oAdvert->content?></p>
	                	</div>
                	</div>
                </div>
            	<?php endif ?>
            </div>
            <div class="col-md-4">
                <?php require SITE_ROOT.'/view/public/inc/profile_menu.php';?>
            </div>
        </div>
    </div>
</div>
<!-- Category list end-->
<?php require SITE_ROOT.'/view/public/inc/footer.php';?>