<?php require SITE_ROOT.'/view/public/inc/header.php';?>
<?php require SITE_ROOT.'/view/public/inc/banner.php';?>

<!--breadcrumb start-->
<?php require SITE_ROOT.'/view/public/inc/breadcrumb.php';?>
<!--breadcrumb end-->

		   <!-- Privacy-policy-section -->
		<div class="privacy w3l">
	<div class="container">
		<h3>L'accès au Site</h3>
		<p>L’accès à l'url www.Bibi.com est gratuit. Les frais d’accès et d’utilisation du réseau de télécommunication sont à la charge de tout utilisateur du site, selon les modalités fixées par ses fournisseurs d’accès et opérateurs de télécommunication.
		Il est rappelé que le fait d’accéder ou de se maintenir frauduleusement dans un système informatique, d’entraver ou de fausser le fonctionnement d’un tel système, d’introduire ou de modifier frauduleusement des données dans un système informatique constituent des délits passibles de sanctions pénales.</p> 
		<h4>Informations Techniques </h4>
		<p>Il est rappelé que le secret des correspondances n'est pas garanti sur le réseau Internet et qu'il appartient à chaque utilisateur d'Internet de prendre toutes les mesures appropriées de façon à protéger ses propres données et/ou logiciels de la contamination d'éventuels virus circulant sur Internet.</p>
		<h3>Contenu du Site - Responsabilité</h3>
		<p>Bibi s'efforce d'assurer l'exactitude et la mise à jour des informations diffusées sur ce site et se réserve le droit de modifier, à tout moment et sans préavis, le contenu ou la présentation de ce site.
		Bibi décline toute responsabilité en cas de retard, d’erreur ou d’omission quant au contenu des présentes pages de même qu’en cas d’interruption ou d’indisponibilité du service.
		A ce titre la responsabilité de Bibi ne saurait en aucun cas être retenue lors de dommages indirects quels qu’ils soient.Bibine peut être tenu responsable de toute décision prise sur la base d’une information contenue sur ce site, ni de l’utilisation qui pourrait en être faite par des tiers et se dégage en particulier de toute responsabilité découlant de la transmission d’informations confidentielles sur le réseau Internet.
		Toute personne désireuse de se procurer un des produits ou services présentés sur le site devra contacter Bibi afin de s'informer de la disponibilité du produit ou service en question ainsi que des conditions contractuelles et des tarifs qui lui sont applicable.</p>
		<h4>Données Personnelles</h4>
		<p>Les informations que vous aurez saisies à l’adresse www.Bibi.com seront enregistrées pour leur traitement par Bibi, pour l’envoi de la Newsletter ou des documentations demandées .Vous pouvez vous désinscrire, à chaque réception de newsletter via un lien permettant de se désabonner.
		A aucun moment, les adresses e-mail ou autres informations transmises à Bibi ne seront cédées à des tiers. Ces informations sont exclusivement destinées à Bibi pour le traitement de vos demandes.
		Vous disposez d'un droit d'accès, de modification, de rectification et de suppression concernant les données collectées sur ce site, dans les conditions prévues par la loi n°78-17 du 6 janvier 1978 (modifiée) relative à l'informatique, aux fichiers et aux libertés. Pour l'exercer, adressez-vous au gestionnaire du site :</p>
		<h4> Liens Hypertextes</h4>
		<p>Bibi n'est pas responsable des liens hypertextes pointant vers le présent site. Les liens hypertextes établis en direction d'autres sites à partir de l’adresse www.Bibi.com ne sauraient, en aucun cas, engager la responsabilité de Bibi.</p>
		<h3>Cookies</h3> 
		<p>L’utilisateur est informé que lors de ses visites sur le site, un cookie peut s’installer automatiquement et être conservé temporairement en mémoire ou sur son disque dur. Un cookie est un élément qui ne permet pas d’identifier l’utilisateur mais sert à l’enregistrement des informations relatives à la navigation de celui-ci sur le site Internet.
		Les utilisateurs du site reconnaissent avoir été informés de cette pratique et autorisent Bibi à l’employer. Ils pourront désactiver ce cookie par l’intermédiaire des paramètres figurant au sein de leur navigateur.</p>
		<h3>Droits de Propriété Intellectuelle</h3>
		<p>L'ensemble de ce site relève de la législation internationale sur le droit d'auteur, le droit des marques et, de façon générale, sur la propriété intellectuelle, en ce qui concerne chacun des éléments de son contenu (textes, images, données, dessins, graphiques, photos et bandes sonores, balises métas, codes sources...) qu’en ce qui concerne sa forme (choix, plan, disposition des matières, moyens d'accès aux données, organisation des données...). Ces contenus, figurant sur les pages de ce site, sont la propriété exclusive de Bibi.</p>
		<p>La reproduction ou représentation, intégrale ou partielle, des pages, des données, des balises HTML et de toute autre élément constitutif au site, par quelque procédé ou support que ce soit, est interdite et constitue, sans autorisation expresse et préalable de l'éditeur, une contrefaçon sanctionnée par les articles L335-2 et suivants du Code de Propriété Intellectuelle.
		Toute reproduction totale ou partielle de cette marque ou de ces logos, effectuée à partir des éléments du site sans l’autorisation expresse et préalable de Bibi, est donc prohibée, au sens de l’article L713-2 du Code de la Propriété Intellectuelle. De même, toute utilisation du contenu et du site à des fins illégales fera l’objet de poursuites judiciaires à l’égard des contrevenants.
		Toute reproduction, représentation, diffusion ou rediffusion, en tout ou partie, du contenu de ce site sur quelque support ou par tout procédé que ce soit, de même que toute vente, revente, retransmission ou mise à disposition de tiers de quelque manière que ce soit sont interdites. Le non-respect de cette interdiction constitue une contrefaçon susceptible d’engager la responsabilité civile et pénale du contrefacteur.</p>
		
	</div>
</div>
<?php require SITE_ROOT.'/view/public/inc/footer.php';?>