<?php require SITE_ROOT.'/view/public/inc/header.php';?>
<?php require SITE_ROOT.'/view/public/inc/banner.php';?>
<!--breadcrumb start-->
<?php require SITE_ROOT.'/view/public/inc/breadcrumb.php';?>
<!--breadcrumb end-->

<!--adverts listing start-->
<div class="tips w3l">
    <div class="container">
        <div class="col-md-9 tips-info">
        	<?php foreach ($this->oAdvertsList as $k => $v): ?>
        	    <div class="news-grid">
        	        <div class="news-img up">
        	            <a href="<?=ROOT_URL?>advert/<?= $v->slug?>"> <img src="<?=ROOT_URL?>static/images/advert/<?= $v->image_url?>" alt=" " class="img-responsive"></a>
        	        </div>
        	        <div class="news-text coming">
        	            <h3><a href="<?=ROOT_URL?>advert/<?= $v->slug?>"><?= $v->title?></a></h3>
        	            <p class="lead">Ajouté le: <b><?= date("d M Y", strtotime($v->created_at))?></b></p>
        	            <p><?=nl2br(htmlspecialchars(mb_strimwidth($v->content, 0, 75, '...')))?></p>
        	            <a href="<?=ROOT_URL?>advert/<?= $v->slug?>" class="read hvr-shutter-in-horizontal"> Détails</a>
        	        </div>
        	        <div class="clearfix"></div>
        	    </div>
        	<?php endforeach ?>
            <?php if (count($this->oAdvertsList) < 1): ?>
                <div class="well text-center">
                    <h3>Aucune annonce trouvée.</h3>
                </div>
            <?php endif ?>
            <div class="blog-pagenat">
                <ul>
                    <li><a class="frist" href="#">Prev</a></li>
                    <li><a href="#">1</a></li>
                    <li><a class="last" href="#">Next</a></li>
                    <div class="clearfix"> </div>
                </ul>
            </div>
        </div>
        <div class="col-md-3 advice-right w3-agile">
            <div class="blo-top">
                <div class="tech-btm one">
                    <a href="//<?=$this->oSinglePub->url?>"><img src="<?=ROOT_URL?>static/images/pub/<?=$this->oSinglePub->image_url?>" class="img-responsive" alt=""></a>
                    <span class="price">PUB</span>
                </div>
            </div>
            <div class="blo-top1">
                <div class="agileits_twitter_posts">
                    <h4> Catégories</h4>
                    <div class="separator"></div>
                    <ul>
                        <?php foreach ($this->oCategories as $k => $v): ?>
                            <li><i class="fa fa-angle-double-right" aria-hidden="true"></i><a href="<?=ROOT_URL?>category/<?=$v->slug?>"><?=$v->name?></a></li>
                        <?php endforeach ?>
                        
                    </ul>
                </div>
            </div>
            <div class="blo-top1 w3l">
                <div class="tech-btm">
                    <h4>Récentes Annonces</h4>
                    <div class="separator"></div>
                    <?php foreach ($this->oRecentAdverts as $k => $v): ?>
                    <?php if ($k < 6): ?>
                        <div class="blog-grids">
                            <div class="blog-grid-left">
                                <a href="<?=ROOT_URL?>advert/<?= $v->id?>"><img src="<?=ROOT_URL?>static/images/advert/<?= $v->image_url?>" class="img-responsive" alt=""></a>
                            </div>
                            <div class="blog-grid-right">
                                <h5><a href="<?=ROOT_URL?>advert/<?= $v->slug?>"><?= $v->title?></a> </h5>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    <?php endif ?>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>
<!--adverts listing end-->
<?php require SITE_ROOT.'/view/public/inc/footer.php';?>