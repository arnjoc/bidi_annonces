<?php require SITE_ROOT.'/view/public/inc/header.php';?>
<?php require SITE_ROOT.'/view/public/inc/banner.php';?>

<!--breadcrumb start-->
<?php require SITE_ROOT.'/view/public/inc/breadcrumb.php';?>
<!--breadcrumb end-->

<!-- Category list start-->
<div class="car-loan-mid w3l" style="padding-bottom: 48px;color: #777;">
	<div class="container">
	    <div class="row">
	        <!-- aside end -->
	        <aside role="sidebar" class=" col-md-3 address">
				<h3 class="">Adresses <span style="color: #efc509;"> BIDI</span></h3>
				<div class="separator"></div>
				<ul class="list-unstyled">
					<li><i class="fa fa-home"></i><span class="text-center"> <?=$this->gSetting->address?></span></li>
					<li style="padding: 10px 0;"><i class="fa fa-phone"></i> <a href="tel: address.telephone " title="Phone">  <?=$this->gSetting->phone?> </a></li>
					<li><i class="fa fa-envelope-o"></i> <a href="mailto: address.email "> <?=$this->gSetting->email?></a></li>
				</ul><br>

				<ul class="social-icons social-icons1">
					<?php if(!empty($this->gSetting->facebook)): ?>
						<li><a href="//<?=$this->gSetting->facebook?>" class="icon icon-border icon-border1 facebook"></a></li>
					<?php endif ?>
					<?php if(!empty($this->gSetting->twitter)): ?>
						<li><a href="//<?=$this->gSetting->twitter?>" class="icon icon-border icon-border1 twitter"></a></li>
					<?php endif ?>
					<?php if(!empty($this->gSetting->instagram)): ?>
						<li><a href="//<?=$this->gSetting->instagram?>" class="icon icon-border icon-border1 instagram"></a></li>
					<?php endif ?>
				    <?php if(!empty($this->gSetting->linkedin)): ?>
						<li><a href="//<?=$this->gSetting->linkedin?>" class="icon icon-border icon-border1 pinterest"></a></li>
					<?php endif ?>
				</ul>
	        </aside>
	        <!-- aside end -->

	        <!-- main start -->
	        <main role="main" class="col-md-8 contact-form">
				<p class="lead"> Des questions ou autres préocupations! Contactez nous en remplissant ce formulaire.</p>
				<div class="">
					<form action="" method="post">
						<?php require SITE_ROOT.'/view/inc/msg.php';?>
						<div class="form-group has-feedback">
							<label for="name">Name</label>
							<input type="text" class="form-control" id="name" name="name" placeholder="" required="required">
							<i class="fa fa-user form-control-feedback"></i>
						</div>
						<div class="form-group has-feedback">
							<label for="email">Email</label>
							<input type="email" class="form-control" id="name" name="email" placeholder="" required="required">
							<i class="fa fa-envelope form-control-feedback"></i>
						</div>
						<div class="form-group has-feedback">
							<label for="subject">Sujet</label>
							<input type="text" class="form-control" id="name" name="subject" placeholder="">
							<i class="fa fa-navicon form-control-feedback"></i>
						</div>
						<div class="form-group has-feedback">
							<label for="subject">Message</label>
							<textarea class="form-control" rows="6" id="message" name="message" placeholder="" required="required"></textarea> 
							<i class="fa fa-pencil form-control-feedback"></i>
						</div>
						<input type="submit" name="contact_form" value="Envoyer" class="btn btn-default">
					 
					</form>
				</div>
	        </main>
	        <!-- main end -->
		</div>
	</div>
</div>
<!-- Category list end-->
<?php require SITE_ROOT.'/view/public/inc/footer.php';?>