<!--/banner-section-->
<div id="demo-1" data-zs-src='["<?=ROOT_URL?>static/public/images/2.jpg", "<?=ROOT_URL?>static/public/images/1.jpg", "<?=ROOT_URL?>static/public/images/3.jpg"]' data-zs-overlay="dots">
    <div class="demo-inner-content">
        <!-- top heaader -->
        <?php require SITE_ROOT.'/view/public/inc/top_header.php';?>
        <div class="clearfix"></div>
        <!--banner-info-->
        <div class="banner-info">
            <h1><a href="<?=ROOT_URL?>">Bidi <span class="logo-sub">Annonces</span> </a></h1>
            
            <p><?=$this->gSetting->slogan?></p>
            <form action="<?=ROOT_URL?>search" method="post">
                <div class="search-two">
                    <input type="text" name="advert" class="search-advert" value="" placeholder="recherche ...">
                </div>
                <div class="section_room">
                    <select id="country" name="category" onchange="change_country(this.value)" class="frm-field required">
                        <option value="null"> Catégories</option>
                        <?php foreach ($this->gCategories as $k => $v): ?>
                            <option value="<?= $v->id?>"><?= $v->name?></option>
                        <?php endforeach ?>
                    </select>
                </div>
                <input type="submit" name="home_search" value="Chercher">
                <div class="clearfix"></div>
            </form>
        </div>
        <!--//banner-info-->
    </div>
</div>