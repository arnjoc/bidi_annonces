<!-- footer -->
<div class="footer w3l">
    <div class="container">
        <div class="footer-grids">
            <div class="col-md-4 footer-grid">
                <h3><?=$this->gSetting->name?></h3>
                <p><?=$this->gSetting->description?></p>
            </div>
            <div class="col-md-4 footer-grid">
                <h3>Nos contact</h3>
                <ul>
                    <li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i><?=$this->gSetting->address?> </li>
                    <li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i><a href="mailto:<?=$this->gSetting->email?>"><?=$this->gSetting->email?></a></li>
                    <li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i><?=$this->gSetting->phone?></li>
                </ul>
            </div>
            <div class="col-md-4 footer-grid">
                <h3>Catégories</h3>
                <ul>
                    <?php foreach($this->gCategories as $k => $v): ?>
                        <?php if($k < 4): ?>
                            <li><a href="<?=ROOT_URL?>category/<?= $v->slug?>"><i class="fa fa-chevron-circle-right" aria-hidden="true" style="border:0;padding: 0;"></i><?=$v->name ?></a></li>
                        <?php endif ?>
                    <?php endforeach ?>
                    <a href="<?=ROOT_URL?>categories"><small>Toutes les catégories </small> <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                </ul>
            </div>
            <div class="clearfix"> </div>
        </div>
        <div class="footer-logo animated wow slideInUp" data-wow-delay=".5s">
            <h2><a href="<?=ROOT_URL?>"><?=$this->gSetting->name?> <span><?=$this->gSetting->slogan?></span></a></h2>
        </div>
        <div class="copy-right animated wow slideInUp" data-wow-delay=".5s">
            <p>&copy <?=date("Y")?> <?=$this->gSetting->name?>. <a href="<?=ROOT_URL?>termes"> Mentions Légales</a></p>
        </div>
    </div>
</div>
<script src="<?=ROOT_URL?>static/public/js/jquery-1.11.1.min.js"></script>
<script src="<?=ROOT_URL?>static/public/js/bootstrap.js"></script>
<script src="<?=ROOT_URL?>static/public/js/jquery.maskedinput.min.js"></script>
<script src="<?=ROOT_URL?>static/public/js/jquery.validate.min.js"></script>
<script src="<?=ROOT_URL?>static/public/js/jquery.form.min.js"></script>
<script src="<?=ROOT_URL?>static/public/js/j-forms.min.js"></script>
<script src="<?=ROOT_URL?>static/public/js/jquery.light-carousel.js"></script>
<script>
$('.sample1').lightCarousel();
</script>
<script type="text/javascript" src="<?=ROOT_URL?>static/public/js/jquery.zoomslider.min.js"></script>
<script type="text/javascript">
$(window).load(function() {
    $("#flexiselDemo").flexisel({
        visibleItems: 1,
        animationSpeed: 1000,
        autoPlay: true,
        autoPlaySpeed: 1000,
        pauseOnHover: true,
        enableResponsiveBreakpoints: true,
        responsiveBreakpoints: {
            portrait: {
                changePoint: 480,
                visibleItems: 1
            },
            landscape: {
                changePoint: 640,
                visibleItems: 1
            },
            tablet: {
                changePoint: 768,
                visibleItems: 1
            }
        }
    });
});
</script>
<script type="text/javascript">
$(window).load(function() {
    $("#flexiselDemo1").flexisel({
        visibleItems: 4,
        animationSpeed: 1000,
        autoPlay: true,
        autoPlaySpeed: 3000,
        pauseOnHover: true,
        enableResponsiveBreakpoints: true,
        responsiveBreakpoints: {
            portrait: {
                changePoint: 480,
                visibleItems: 1
            },
            landscape: {
                changePoint: 640,
                visibleItems: 2
            },
            tablet: {
                changePoint: 768,
                visibleItems: 3
            }
        }
    });
});
</script>
<script type="text/javascript" src="<?=ROOT_URL?>static/public/js/jquery.flexisel.js"></script>
<script>
var sidebarbtn = document.getElementById('sidebar-btn');
var sidebar = document.getElementById('sidebar');
sidebarbtn.addEventListener('click', function() {
    if (sidebar.classList.contains('visible')) {
        sidebar.classList.remove('visible');
    } else {
        sidebar.className = 'visible';
    }
});
</script>
<script src="<?=ROOT_URL?>static/public/js/classie2.js"></script>
<!-- <script>
var menuRight = document.getElementById('cbp-spmenu-s2'),
    showRight = document.getElementById('showRight'),
    body = document.body;

showRight.onclick = function() {
    classie.toggle(this, 'active');
    classie.toggle(menuRight, 'cbp-spmenu-open');
    disableOther('showRight');
};

function disableOther(button) {
    if (button !== 'showRight') {
        classie.toggle(showRight, 'disabled');
    }
}
</script> -->
<script >
(function($){
   
  $('#duplicatebtn').click(function(e){
     e.preventDefault();
     var $clone = $('#duplicate').clone().attr('id', '').removeClass('hidden');
     $('#duplicate').before($clone);
     $('#duplicate').parent().removeClass('hidden');
  });
})(jQuery);

  $('#change-avatar').on('click', function(e){
    e.preventDefault();
    $('.avatar-form').removeClass('hidden');
    $(this).hide();
  });
</script>
</body>

</html>