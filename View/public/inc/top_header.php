<?php $image_url = !empty($_SESSION['auth']->image_url) ? $_SESSION['auth']->image_url : 'img.png';?>
<div class="header-top">
    <!-- /header-left -->
    <div class="header-left">
        <!-- /sidebar -->
        <div id="sidebar">
            <h4 class="menu">Menu</h4>
            <ul>
                <li><a href="<?=ROOT_URL?>">Acceuil</a></li>
                <li><a href="<?=ROOT_URL?>categories">Catégories <i class="glyphicon glyphicon-triangle-bottom"> </i></a>
                    <ul>
                        <?php foreach ($this->gCategories as $k => $v): ?>
                        <?php if ($k < 6): ?>
                            <li><a href="<?=ROOT_URL?>category/<?= $v->slug?>"><span><?= $v->name?></span></a></li>
                        <?php endif ?>
                        <?php endforeach ?>
                    </ul>
                </li>
                <li><a href="<?=ROOT_URL?>faq">Comment ça marche</a></li>
                <li><a href="<?=ROOT_URL?>contactus">Contact</a></li>
            </ul>
            <div id="sidebar-btn">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <!-- //sidebar -->
    </div>
    <!-- //header-left -->
    <div class="search-box">
        <ul>
            <li>
                <a href="<?= ROOT_URL?>advertadd" class="navig link-top btn btn-warning"><i class="fa fa-plus"></i> &nbsp;Poster une annonce</a>
            </li>
            <?php if(empty($_SESSION['auth'])): ?>
                
                <li>
                    <a href="<?= ROOT_URL?>register" class="navig link-top"><i class="fa fa-user"></i> &nbsp;Sign Up </a>
                </li>
                <li>
                    <a href="<?= ROOT_URL?>login" class="navig link-top"><i class="fa fa-lock"></i> &nbsp;Login </a>
                    <div class="cbp-spmenu-push">
                        <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right" id="cbp-spmenu-s2">
                            <h3>Login</h3>
                            <div class="login-inner">
                                <div class="login-top">
                                    <form action="#" method="post">
                                        <input type="text" name="email" class="email" placeholder="Email" required="" />
                                        <input type="password" name="password" class="password" placeholder="Password" required="" />
                                        <input type="checkbox" id="brand" value="">
                                        <label for="brand"><span></span> Remember me</label>
                                    </form>
                                    <div class="login-bottom">
                                        <ul>
                                            <li>
                                                <a href="#">Forgot password?</a>
                                            </li>
                                            <li>
                                                <form action="#" method="post">
                                                    <input type="submit" value="LOGIN" />
                                                </form>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </nav>
                    </div>
                </li>
            <?php else:?>
                <li class="dropdown profile-drop">
                    <a href="<?= ROOT_URL?>profile" class="navig link-top dropdown-toggle" data-toggle="dropdown">
                    <img src="<?= ROOT_URL?>static/images/user/<?=$image_url?>" alt="" height="40"> &nbsp; Salut! <span> &nbsp;<?=ucfirst($_SESSION['auth']->username)?></span> <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="<?=ROOT_URL?>profile"><i class="fa fa-user"></i> Profile</a></li>
                        <?php if($_SESSION['auth']->role == 'ROLE_ADMIN'): ?>
                            <li><a href="<?=ROOT_URL?>admin"><i class="fa fa-dashboard"></i> Admin Dashboard</a></li>
                        <?php endif?>
                        <li class="divider"></li>
                        <li><a href="<?=ROOT_URL?>logout"><i class="fa fa-sign-out"></i> Deconnexion</a></li>
                      </ul>
                </li>
            <?php endif?>
        </ul>
    </div>
    <div class="clearfix"></div>
</div>