<!--/breadcrumb-->
<div class="service-breadcrumb">
    <div class="container">
        <div class="wthree_service_breadcrumb_left">
            <ul>
                <li><a href="<?=ROOT_URL?>">Acceuil</a> <i>|</i></li>
                <?php if(!empty($this->oAdvertsList[0]->category_name)):?>
                    <li><a href="<?=ROOT_URL?>categories"><?=$this->gCurrentPage?></a> <i>|</i></li>
                    <li><?=$this->oAdvertsList[0]->category_name ?></li>
                <?php elseif(!empty($this->oRelativeAdverts)): ?>
                    <li><a href="<?=ROOT_URL?>categories"><?=$this->gCurrentPage?></a> <i>|</i></li>
                    <li><?=$this->oRelativeAdverts[0]->category_name ?></li>
                <?php else: ?>
                    <li><?=$this->gCurrentPage?></li>
                <?php endif ?>
            </ul>
        </div>
        
        <div class="wthree_service_breadcrumb_right">
            <?php if(!empty($this->oAdvertsList[0]->category_name)):?>
                <h3><?=$this->oAdvertsList[0]->category_name ?></h3>
            <?php elseif(!empty($this->oRelativeAdverts)): ?>
                <h3><?=$this->oRelativeAdverts[0]->category_name ?></h3>
            <?php else: ?>
                <h3><?=$this->gCurrentPage?></h3>
            <?php endif ?>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>
<!--//breadcrumb-->