<!--/banner-section-->
<?php $bg = ['banner-inner.jpg', 'banner-inner1.jpg', 'banner-inner2.jpg', 'banner-inner3.jpg'] ?>
<div id="demo-1" class="banner-inner" 
style="background-image: url('<?=ROOT_URL?>static/public/images/<?=$bg[mt_rand(0, 3)]?>');">
    <div class="banner-inner-dott">
        <!-- top heaader -->
        <?php require SITE_ROOT.'/view/public/inc/top_header.php';?>
        <div class="clearfix"></div>
        <!--banner-info-->
        <div class="banner-info">
            <h1><a href="<?=ROOT_URL?>">Bidi <span class="logo-sub">Annonces</span> </a></h1>
            <br>
            <p><?=$this->gSetting->slogan?></p>
        </div>
        <!--//banner-info-->
    </div>
</div>