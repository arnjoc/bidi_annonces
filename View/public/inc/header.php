<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Bidi Annonces | site de petites annonces</title>
    <!-- for-mobile-apps -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="site de petites annonces gratuites" />
    <script type="application/x-javascript">
    addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);

    function hideURLbar() { window.scrollTo(0, 1); }
    </script>
    <!-- //for-mobile-apps -->
    <link href="<?=ROOT_URL?>static/public/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <link rel="stylesheet" type="text/css" href="<?=ROOT_URL?>static/public/css/zoomslider.css" />
    <link rel="stylesheet" type="text/css" href="<?=ROOT_URL?>static/public/css/style.css" />
    <link rel="stylesheet" type="text/css" href="<?=ROOT_URL?>static/public/css/component.css" />
    <link rel="stylesheet" type="text/css" href="<?=ROOT_URL?>static/public/css/recommend.css" />
    <link href="<?=ROOT_URL?>static/public/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" />
    <script type="text/javascript" src="<?=ROOT_URL?>static/public/js/modernizr-2.6.2.min.js"></script>
    <link href="<?=ROOT_URL?>static/public/css/light-carousel.css" rel="stylesheet" type="text/css">
    <!--/web-fonts-->
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,600italic,300,300italic,700,400italic' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Wallpoet' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Ubuntu:400,500,700,300' rel='stylesheet' type='text/css'>
    <!--//web-fonts-->
</head>

<body>
    