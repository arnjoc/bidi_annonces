<div class="profile-menu" style="box-shadow: 0 2px 4px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12)!important;
        ">
	<div class="list-group">
	  <a href="<?=ROOT_URL?>profile" class="list-group-item active"><i class="fa fa-user"></i> <b> Mon profile</b></a>
	  <a href="<?=ROOT_URL?>profileedit" class="list-group-item"><i class="fa fa-pencil"></i> <b> Editer mon profile</b></a>
	  <a href="<?=ROOT_URL?>profileeditpass" class="list-group-item"><i class="fa fa-lock"></i> <b> Changer mot de passe</b></a>
	  <a href="<?=ROOT_URL?>advertadd" class="list-group-item"><i class="fa fa-plus"></i> <b> Ajouter une annonce</b></a>
	  <a href="<?=ROOT_URL?>useradvertsindex" class="list-group-item"><i class="fa fa-th"></i> <b> Mes annonces</b></a>
	  <a href="<?=ROOT_URL?>logout" class="list-group-item"><i class="fa fa-sign-out"></i> <b> Deconnexion</b></a>
	</div>
</div>