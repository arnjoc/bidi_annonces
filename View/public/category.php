<?php require SITE_ROOT.'/view/public/inc/header.php';?>
<?php require SITE_ROOT.'/view/public/inc/banner.php';?>

<!--breadcrumb start-->
<?php require SITE_ROOT.'/view/public/inc/breadcrumb.php';?>
<!--breadcrumb end-->

<!-- Category list start-->
<div class="car-loan-mid w3l" style="padding-bottom: 48px;">
	<div class="container">
	    <h3 class="tittle">Catégories d'annonces</h3>
	    <div class="separator"></div>
	    <div class="categories">
	        <?php foreach ($this->oCategories as $k => $v): ?>
	        <?php// if ($k < 4): ?>
	        <div class="col-md-3 focus-grid">
	            <a href="<?=ROOT_URL?>category/<?=$v->slug?>">
	                <div class="focus-border">
	                    <div class="focus-layout">
	                        <div class="focus-image"><i class="fa fa-<?=!empty($v->icon) ? $v->icon : 'tag' ?>"></i></div>
	                        <h4 class="clrchg"><?= $v->name?></h4>
	                    </div>
	                </div>
	            </a>
	        </div>
	        <?php //endif ?>
	        <?php endforeach ?>
	        <div class="clearfix"></div>
	    </div>
	</div>
</div>
<!-- Category list end-->
<?php require SITE_ROOT.'/view/public/inc/footer.php';?>