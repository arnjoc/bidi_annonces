<?php require dirname(__DIR__) . '/inc/admin_header.php' ?>

<!-- main content start-->
<div id="page-wrapper">
    <div class="main-page">
        <div class="forms">
            <h2 class="title1">Publicité</h2>
            <?php require dirname(__DIR__) . '/inc/msg.php' ?>
            <div class=" form-grids row form-grids-right">
                <div class="widget-shadow " data-example-id="basic-forms">
                    <div class="form-title">
                        <h4>Nouvelle publicité:</h4>
                    </div>
                    <div class="form-body">
                        <form class="form-horizontal"  action="" method="post" enctype="multipart/form-data"> 
                            <div class="form-group"> 
                                <label for="title" class="col-sm-2 control-label">Titre</label> 
                                <div class="col-sm-8"> 
                                    <input type="text" name="title" class="form-control" id="title" required="required" value="<?=isset($_POST['title']) ? $_POST['title'] : ''; ?>"> 
                                </div> 
                            </div>
                            <div class="form-group"> 
                                <label for="content" class="col-sm-2 control-label">Contenu</label> 
                                <div class="col-sm-8"> 
                                    <textarea class="form-control" name="content" rows="5" id="content" required="required"><?=isset($_POST['content']) ? $_POST['content'] : ''; ?></textarea>
                                </div> 
                            </div> 
                            <div class="form-group">
                                <label for="category" class="col-sm-2 control-label">Categorie:</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="pub_category_id" id="category" required="required">
                                    	<option>Choisir une categorie</option>
                                        <?php if (!empty($this->categoryList)): ?>

                                            <?php foreach($this->categoryList as $category): ?>
                                                <option value="<?= $category->id ?>">
                                                    <?= $category->name ?> 
                                                </option>
                                            
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group"> 
                                <label for="url" class="col-sm-2 control-label">Lien de l'annonceur</label> 
                                <div class="col-sm-8"> 
                                    <input type="text" name="url" class="form-control" id="url" required="required" placeholder="http://exemple.com" value="<?=isset($_POST['url']) ? $_POST['url'] : ''; ?>"> 
                                </div> 
                            </div>

                            <div class="form-group"> 
                                <label for="end_date" class="col-sm-2 control-label">Date Fin de Pub</label> 
                                <div class="col-sm-8"> 
                                    <input type="text" name="end_date" class="form-control js-datepicker" id="end_date" required="required" value="<?=isset($_POST['end_date']) ? $_POST['end_date'] : ''; ?>"> 
                                </div> 
                            </div>

                            <div class="form-group"> 
                                <div class="col-md-offset-2"> 
                                    <label for="image"></label> 
                                    <input type="file" name="image" id="image">
                                </div>
                            </div>

                            <div class="form-group"> 
                            	<label for="status" class="col-sm-2 control-label">Etat</label>
                                <div class="col-sm-8"> 
                                	<div class="radio block">
                                        <label class="radio-inline"> 
                                            <input type="radio" name="status" value="1" checked> Publier 
                                        </label>
                                    </div>
                                    <div class="radio block">
                                        <label class="radio-inline">
                                            <input type="radio" name="status" value="0"> Ne pas Publier 
                                        </label>
                                	</div> 
                                </div> 
                            </div> 
                            <div class="col-sm-offset-2"> 
                                <input type="submit" name="add_submit" value="Ajouter" class="btn btn-success"/>
                            </div> 
                        </form> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- main content end-->

<?php require dirname(__DIR__) . '/inc/admin_footer.php' ?>
