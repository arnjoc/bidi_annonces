<?php require dirname(__DIR__) . '/inc/admin_header.php' ?>

<!-- main content start-->
<div id="page-wrapper">
	<div class="main-page">
		<h2 class="title1">Publicités</h2>
		<?php require dirname(__DIR__) . '/inc/msg.php' ?>
		<?php if (empty($this->oPubs)): ?>
		    <div class="text-center">
			    <h3 class="well">Aucune publicité trouvée.</h3>
			    <p>
			    	<button type="button" onclick="window.location='<?=ROOT_URL?>admin/pub/add'" class="btn btn-primary btn-pri">
			    		<i class="fa fa-plus" aria-hidden="true"></i>
			    		Ajouter une Publicité !
			    	</button>
			    </p>
			</div>
		<?php else: ?>
			<div class="tables">
				<div class="table-responsive bs-example widget-shadow">
					<h4>Liste des Publicités:</h4>
					<table class="table table-bordered"> 
						<thead> 
							<tr> 
								<th>#</th> 
								<th>Titre</th> 
								<th>Categorie</th>
								<th>Date d'ajout</th> 
								<th>Date de fin</th> 
								<th>Action</th>
							</tr> 
						</thead> 
						<tbody> 
							<?php foreach ($this->oPubs as $k => $oPub): ?>
								<tr> 
									<th scope="row"><?=$k + 1?></th> 
									<td><a href="<?=ROOT_URL?>admin/Pub/show/<?=$oPub->id?>"><?=ucfirst(htmlspecialchars($oPub->title))?></a></td> 
									<td><?=$oPub->category_name?></td> 
									<td><?=$oPub->created_at?></td> 
									<td><?=$oPub->end_date?></td> 
									<td>
										<?php if(!empty($_SESSION['is_logged'])): ?>

										    <a onclick="window.location='<?=ROOT_URL?>admin/pub/edit/<?=$oPub->id?>'" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a> 
										    <form action="<?=ROOT_URL?>admin/pub/delete/<?=$oPub->id?>" method="post" style="display:inline;"><button type="submit" name="delete" value="1" class="btn btn-danger " onclick="confirm('Etes vous sure de supprimer')"><i class="fa fa-trash"></i> Delete</button></form>
										<?php endif ?>
									</td>
								</tr> 
							<?php endforeach ?>
						</tbody> 
					</table> 
				</div>
			</div>

		<?php endif ?>
	</div>
</div>
<!-- main content end-->

<?php require dirname(__DIR__) . '/inc/admin_footer.php' ?>
