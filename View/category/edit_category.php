<?php require dirname(__DIR__) . '/inc/admin_header.php' ?>

<!-- main content start-->
<div id="page-wrapper">
    <div class="main-page">
        <div class="forms">
            <h2 class="title1">Categorie / <small> <?= $this->oHead ?></small></h2>
            <?php require dirname(__DIR__) . '/inc/msg.php' ?>
            <div class=" form-grids row form-grids-right">
                <div class="widget-shadow " data-example-id="basic-forms">
                    <div class="form-title">
                        <h4>Edition de categorie:</h4>
                    </div>
                    <div class="form-body">
                        <form class="form-horizontal"  action="" method="post" enctype="multipart/form-data"> 
                            <div class="form-group"> 
                                <label for="name" class="col-sm-2 control-label">Nom</label> 
                                <div class="col-sm-8"> 
                                    <input type="text" name="name" class="form-control" id="name" value="<?=$this->oCategory->name?>" required="required"> 
                                </div> 
                            </div> 
                            <div class="form-group"> 
                                <label for="description" class="col-sm-2 control-label">description</label> 
                                <div class="col-sm-8"> 
                                    <textarea class="form-control" name="description" rows="2" id="description"><?=htmlspecialchars($this->oCategory->description)?></textarea>
                                </div> 
                            </div>
                            <div class="form-group"> 
                                <div class="col-md-offset-1 col-md-5"> 
                                    <label for="icon" class="col-sm-2 control-label">Icon</label> 
                                    <div class="col-sm-8"> 
                                        <input type="text" name="icon" class="form-control" id="icon" required="required" value="<?=htmlspecialchars($this->oCategory->icon)?>"> 
                                    </div>
                                </div>
                                <div class="col-md-6" style="padding-top:1.2em;">
                                    <div class="fa fa-<?=$this->oCategory->icon?>"></div>
                                </div>   
                            </div> 
                            <div class="form-group"> 
                                <div class="col-md-offset-2 col-md-6"> 
                                    <label for="image"></label> 
                                    <input type="file" name="image" id="image">
                                </div>
                                <div class="col-md-4" style="padding-top:1.2em;"> 
                                    <?php if( $this->oCategory->image_url): ?>
                                        <img src="<?= ROOT_URL . 'static/images/category/'.$this->oCategory->image_url?>" alt="" height="120" width="120" class="img-thumbnail">
                                    <?php endif ?>
                                </div>
                            </div>
                             
                            <div class="col-sm-offset-2"> 
                                <input type="submit" name="edit_submit" value="Editer" class="btn btn-info"/>
                            </div> 
                        </form> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- main content end-->

<?php require dirname(__DIR__) . '/inc/admin_footer.php' ?>
