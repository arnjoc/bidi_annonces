<?php require dirname(__DIR__) . '/inc/admin_header.php' ?>

<!-- main content start-->
<div id="page-wrapper">
	<div class="main-page">
		<h2 class="title1">Categorie / <small> <?= $this->oHead ?></small></h2>
		<?php require dirname(__DIR__) . '/inc/msg.php' ?>
		<?php if (empty($this->oCategories)): ?>
			<div class="text-center">
			    <h3 class="well">Aucune categorie disponible.</h3>
			    <p>
			    	<button type="button" onclick="window.location='<?=ROOT_URL?>admin/category/add'" class="btn btn-default">Ajouter une category!</button>
			    </p>
			</div>
		<?php else: ?>
			<div class="tables">
				<div class="table-responsive bs-example widget-shadow">
					<h4>Liste des Categories:</h4>
					<table class="table table-bordered"> 
						<thead> 
							<tr> 
								<th>#</th> 
								<th>Nom</th> 
								<th>Icon</th>
								<th>Date d'ajout</th> 
								<th>Action</th>
							</tr> 
						</thead> 
						<tbody> 
							<?php foreach ($this->oCategories as $k => $oCategory): ?>
								<tr> 
									<th scope="row"><?=$k + 1 ?></th> 
									<td><a href="<?=ROOT_URL?>admin/<?=$this->oLinkText?>/edit/<?=$oCategory->id?>"><?=ucfirst($oCategory->name)?></a></td>
									<td> <span class="fa fa-<?=!empty($oCategory->icon) ? $oCategory->icon : 'tag' ?>"></span></td>
									<td><?=$oCategory->created_at?></td> 
									<td>
										<?php if(!empty($_SESSION['is_logged'])): ?>

										    <a onclick="window.location='<?=ROOT_URL?>admin/<?=$this->oLinkText?>/edit/<?=$oCategory->id?>'" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a> 
										    <form action="<?=ROOT_URL?>admin/<?=$this->oLinkText?>/delete/<?=$oCategory->id?>" method="post" style="display:inline;"><button type="submit" name="delete" value="1" class="btn btn-danger " onclick="confirm('Etes vous sure de supprimer')"><i class="fa fa-trash"></i> Delete</button></form>
										<?php endif ?>
									</td>
								</tr> 
							<?php endforeach ?>
						</tbody> 
					</table> 
				</div>
			</div>

		<?php endif ?>
	</div>
</div>
<!-- main content end-->

<?php require dirname(__DIR__) . '/inc/admin_footer.php' ?>
