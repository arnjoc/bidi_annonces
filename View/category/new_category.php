<?php require dirname(__DIR__) . '/inc/admin_header.php' ?>

<!-- main content start-->
<div id="page-wrapper">
    <div class="main-page">
        <div class="forms">
            <h2 class="title1">Categorie / <small> <?= $this->oHead ?></small></h2>
            <?php require dirname(__DIR__) . '/inc/msg.php' ?>
            <div class=" form-grids row form-grids-right">
                <div class="widget-shadow " data-example-id="basic-forms">
                    <div class="form-title">
                        <h4>Ajout de categorie:</h4>
                    </div>
                    <div class="form-body">
                        <form class="form-horizontal"  action="" method="post" enctype="multipart/form-data"> 
                            <div class="form-group"> 
                                <label for="name" class="col-sm-2 control-label">Nom</label> 
                                <div class="col-sm-8"> 
                                    <input type="text" name="name" class="form-control" id="name" required="required" value="<?=isset($_POST['name']) ? $_POST['name'] : ''; ?>"> 
                                </div> 
                            </div> 
                            <div class="form-group"> 
                                <label for="description" class="col-sm-2 control-label">description</label> 
                                <div class="col-sm-8"> 
                                    <textarea class="form-control" name="description" rows="2" id="description"><?=isset($_POST['description']) ? $_POST['description'] : ''; ?></textarea>
                                </div> 
                            </div>
                            <div class="form-group"> 
                                <label for="icon" class="col-sm-2 control-label">Icon</label> 
                                <div class="col-sm-8"> 
                                    <input type="text" name="icon" class="form-control" id="icon" required="required" value="<?=isset($_POST['icon']) ? $_POST['icon'] : ''; ?>"> 
                                </div> 
                            </div> 
                            <div class="form-group"> 
                                <label for="image" class="col-md-2" >Image principal</label>
                                <div class="col-md-8">
                                    <input type="file" name="image" id="image">
                                </div>
                            </div>

                            <div class="form-group"> 
                                <label for="images" class="col-md-2" >Plus d'images</label>
                                <div class="col-md-4"> 
                                    <input type="file" name="images[]" id="images">
                                    <input type="file" name="images[]" class="hidden" id="duplicate">
                                </div>
                                <div class="col-md-6">
                                    <?php if(0)://foreach($images as $k => $image): ?>
                                     <p>   
                                         <img src="<?=WEBROOT ?>images/works/<?=$image['name']; ?>" width="100">
                                         <a href="?delete_image=<?=$image['id']; ?>&<?= csrf(); ?>" onclick="return confirm('Sur ?');">Supprimer</a>
                                         <a href="?highlight_image=<?=$image['id']; ?>&id=<?= $_GET['id']; ?>&<?= csrf(); ?>">Mettre à la une</a>
                                     </p>
                                    <?php endif//endforeach ?>
                                </div>
                            </div>
                            <div class="col-sm-offset-2"> 
                                <a href="#" class="btn btn-success btn-xs" id="duplicatebtn">Ajouter image</a>
                            </div><br><br>
                             
                            <div class="col-sm-offset-2"> 
                                <input type="submit" name="add_submit" value="Ajouter" class="btn btn-success"/>
                            </div> 
                        </form> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- main content end-->

<?php require dirname(__DIR__) . '/inc/admin_footer.php' ?>
