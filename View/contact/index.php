<?php require dirname(__DIR__) . '/inc/admin_header.php' ?>

<!-- main content start-->
<div id="page-wrapper">
	<div class="main-page">
		<h2 class="title1">Messages page de contact</h2>
		<?php require dirname(__DIR__) . '/inc/msg.php' ?>
		<?php if (empty($this->oMessages)): ?>
		    <div class="text-center">
			    <h3 class="well">Aucun message reçu.</h3>
			</div>
		<?php else: ?>
			<div class="col-md-4 compose-left">
			    <div class="folder widget-shadow">
			        <ul>
			            <li class="head"><i class="fa fa-folder" aria-hidden="true"></i>Dossiers</li>
			            <li><a href="#"><i class="fa fa-inbox"></i>Boite de reception <span>52</span></a></li>
			            <li><a href="#"><i class="fa fa fa-envelope-o"></i>Envoyés </a></li>
			            <li><a href="#"><i class="fa fa-file-text-o"></i>Brouillons <span>03</span></a> </li>
			            <li><a href="#"><i class="fa fa-flag-o"></i>Spam </a></li>
			            <li><a href="#"><i class="fa fa-trash-o"></i>Poubelle</a></li>
			        </ul>
			    </div>
			</div>
			<div class="col-md-8 compose-right widget-shadow">
			    <div class="panel-default">
			        <div class="panel-heading">
			            Boite de reception Message de contact
			        </div>
			        <div class="inbox-page">
			            <h4>Messages non lus</h4>
			            <?php foreach ($this->oMessages as $msg):?>
				            <div class="inbox-row widget-shadow" id="accordion" role="tablist" aria-multiselectable="true">
				                <div class="mail ">
				                    <input type="checkbox" class="checkbox"> </div>
				                <div class="mail"><img src="<?=ROOT_URL?>static/images/user.png" alt=""/></div>
				                <div class="mail mail-name">
				                    <h6><?=!empty($msg->name) ? ucfirst($msg->name) : $msg->email; ?></h6> </div>
				                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
				                    <div class="mail">
				                        <p><?=!empty($msg->subject) ? ucfirst($msg->subject) : "Pas de sujet"; ?></p>
				                    </div>
				                </a>
				                <div class="mail-right dots_drop">
				                    <div class="dropdown">
				                        <a href="#" data-toggle="dropdown" aria-expanded="false">
				                            <p><i class="fa fa-ellipsis-v mail-icon"></i></p>
				                        </a>
				                        <ul class="dropdown-menu float-right">
				                            <li>
				                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
													<i class="fa fa-reply mail-icon"></i>
													Repondre
												</a>
				                            </li>
				                            <li>
				                            	<form action="<?=ROOT_URL?>admin/contact/delete/<?=$msg->id?>" method="post">
				                                <button type="submit" name="delete" value="<?=$msg->id?>" class="font-red btn btn-link" onclick="confirm('Etes vous sure de supprimer');">
													<i class="fa fa-trash-o mail-icon"></i>
													 Supprimer
												</button>
												</form>
				                            </li>
				                        </ul>
				                    </div>
				                </div>
				                <div class="mail-right">
				                    <p><?=date("d M Y", strtotime($msg->sent_at))?></p>
				                </div>
				                <div class="clearfix"> </div>
				                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
				                    <div class="mail-body">
				                        <p><?=!empty($msg->message) ? $msg->message : "Pas de message"; ?></p>
				                        <form action="<?=ROOT_URL?>admin/contact/reply" method="post">
				                            <input type="text" name="reply" placeholder="Repondre à l'émetteur" required="">
				                            <input type="submit" value="Envoyer">
				                        </form>
				                    </div>
				                </div>
				            </div>
				        <?php endforeach ?>
			        </div>
			    </div>
			</div>
			<div class="clearfix"> </div>
		<?php endif ?>
	</div>
</div>
<!-- main content end-->

<?php require dirname(__DIR__) . '/inc/admin_footer.php' ?>
