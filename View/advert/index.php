<?php require dirname(__DIR__) . '/inc/admin_header.php' ?>

<!-- main content start-->
<div id="page-wrapper">
	<div class="main-page">
		<h2 class="title1">Annonces</h2>
		<?php require dirname(__DIR__) . '/inc/msg.php' ?>
		<?php if (empty($this->oAdverts)): ?>
		    <div class="text-center">
			    <h3 class="well">Aucune annonce trouvée.</h3>
			    <p>
			    	<button type="button" onclick="window.location='<?=ROOT_URL?>admin/advert/add'" class="btn btn-primary btn-pri">
			    		<i class="fa fa-plus" aria-hidden="true"></i>
			    		Ajouter une annonce !
			    	</button>
			    </p>
			</div>
		<?php else: ?>
			<div class="tables">
				<div class="table-responsive bs-example widget-shadow">
					<h4>Liste des Annonces:</h4>
					<table class="table table-bordered"> 
						<thead> 
							<tr> 
								<th>#</th> 
								<th>Titre</th> 
								<th>Prix</th> 
								<th>Categorie</th> 
								<th>Auteur</th> 
								<th>Date d'ajout</th> 
								<th>Action</th>
							</tr> 
						</thead> 
						<tbody> 
							<?php foreach ($this->oAdverts as $k => $oAdvert): ?>
								<tr> 
									<th scope="row"><?=$k + 1?></th> 
									<td><a href="<?=ROOT_URL?>admin/advert/edit/<?=$oAdvert->id?>"><?=ucfirst(htmlspecialchars($oAdvert->title))?></a></td> 
									<td><?= $oAdvert->currency=='cfa'?'':$oAdvert->currency ?><?= $oAdvert->price?>
					    				<?= $oAdvert->currency=='cfa'? $oAdvert->currency :'' ?></td> 
									<td><?=$oAdvert->category_name?></td> 
									<td><?=htmlspecialchars($oAdvert->author)?></td> 
									<td><?=$oAdvert->created_at?></td> 
									<td>
										<?php if(!empty($_SESSION['is_logged'])): ?>

										    <a onclick="window.location='<?=ROOT_URL?>admin/advert/edit/<?=$oAdvert->id?>'" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a> 
										    <form action="<?=ROOT_URL?>admin/advert/delete/<?=$oAdvert->id?>" method="post" style="display:inline;"><button type="submit" name="delete" value="1" class="btn btn-danger " onclick="confirm('Etes vous sure de supprimer')"><i class="fa fa-trash"></i> Delete</button></form>
										<?php endif ?>
									</td>
								</tr> 
							<?php endforeach ?>
						</tbody> 
					</table> 
				</div>
			</div>

		<?php endif ?>
	</div>
</div>
<!-- main content end-->

<?php require dirname(__DIR__) . '/inc/admin_footer.php' ?>
