<?php require dirname(__DIR__) . '/inc/admin_header.php' ?>

<!-- main content start-->
<div id="page-wrapper">
    <div class="main-page">
        <div class="forms">
            <h2 class="title1">Annonce</h2>
            <?php require dirname(__DIR__) . '/inc/msg.php' ?>
            <div class=" form-grids row form-grids-right">
                <div class="widget-shadow " data-example-id="basic-forms">
                    <div class="form-title">
                        <h4>Nouvelle annonce:</h4>
                    </div>
                    <div class="form-body">
                        <form class="form-horizontal"  action="" method="post" enctype="multipart/form-data"> 
                            <div class="form-group"> 
                                <label for="title" class="col-sm-2 control-label">Titre</label> 
                                <div class="col-sm-8"> 
                                    <input type="text" name="title" class="form-control" id="title" required="required" value="<?=isset($_POST['title']) ? $_POST['title'] : ''; ?>"> 
                                </div> 
                            </div>
                            <div class="form-group"> 
                                <label for="content" class="col-sm-2 control-label">Contenu</label> 
                                <div class="col-sm-8"> 
                                    <textarea class="form-control" name="content" rows="5" id="content"><?=isset($_POST['content']) ? $_POST['content'] : ''; ?></textarea>
                                </div> 
                            </div> 
                            <div class="form-group">
                                <label for="category" class="col-sm-2 control-label">Categorie:</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="category_id" id="category">
                                    	<option>Choisir une categorie</option>
                                        <?php if (!empty($this->categoryList)): ?>

                                            <?php foreach($this->categoryList as $category): ?>
                                                <option value="<?= $category->id ?>">
                                                    <?= $category->name ?> 
                                                </option>
                                            
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="type" class="col-sm-2 control-label">Type d'annonce:</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="type" id="type">
                                      <option>Choisir un type</option>
                                      <option value="standard">standard</option>
                                      <option value="sponsored">sponsorisée</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group"> 
                                <div class="col-md-offset-2"> 
                                    <label for="image"></label> 
                                    <input type="file" name="image" id="image">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="images" class="col-md-2" >Plus d'images</label>
                                <div class="col-md-4 hidden">
                                    <input type="file" name="images[]" class="hidden" id="duplicate">
                                </div>
                            </div>

                            <div class="col-sm-offset-2"> 
                                <a href="#" class="btn btn-success btn-xs" id="duplicatebtn">Ajouter image</a>
                            </div><br><br>

                            <div class="form-group"> 
                            	<label for="status" class="col-sm-2 control-label">Mise en ligne</label>
                                <div class="col-sm-8"> 
                                	<div class="radio block">
                                        <label class="radio-inline"> 
                                            <input type="radio" name="status" value="1" checked> Publier 
                                        </label>
                                    </div>
                                    <div class="radio block">
                                        <label class="radio-inline">
                                            <input type="radio" name="status" value="0"> Ne pas Publier 
                                        </label>
                                	</div> 
                                </div> 
                            </div>
                            
                            <div class="col-sm-offset-2"> 
                                <input type="submit" name="add_submit" value="Ajouter" class="btn btn-success"/>
                            </div> 
                        </form> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- main content end-->

<?php require dirname(__DIR__) . '/inc/admin_footer.php' ?>
