<?php require dirname(__DIR__) . '/inc/admin_header.php' ?>

<!-- main content start-->
<div id="page-wrapper">
    <div class="main-page">
        <div class="forms">
            <h2 class="title1">Edition d'annonce</h2>
            <?php require dirname(__DIR__) . '/inc/msg.php' ?>
            <div class=" form-grids row form-grids-right">
                <div class="widget-shadow " data-example-id="basic-forms">
                    <?php if (empty($this->oAdvert)): ?>
                        <div class="form-body">
                            <h3 class="well"> Aucune donnée de l'annonce</h3>
                        </div>
                    <?php else: ?>

                    <div class="form-title">
                        <h4><?=ucfirst(htmlspecialchars($this->oAdvert->title))?> :</h4>
                        <a href="<?=ROOT_URL?>admin/advert/add" class="btn btn-primary btn-lg pull-right form-inline"><i class="fa fa-plus"></i> </a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-body">
                        <form class="form-horizontal" action="" method="post" enctype="multipart/form-data"> 
                            <div class="form-group"> 
                                <label for="title" class="col-sm-2 control-label">Titre</label> 
                                <div class="col-sm-8"> 
                                    <input type="text" name="title" class="form-control" value="<?=htmlspecialchars($this->oAdvert->title)?>" id="title" required="required"> 
                                </div> 
                            </div>
                            <div class="form-group">
                                <label for="title" class="col-sm-2 control-label">Prix</label>
                                <div class="col-sm-6"> 
                                    <input type="text" name="price" class="form-control" id="title" required="required" value="<?=htmlspecialchars($this->oAdvert->price)?>"> 
                                </div> 
                                <div class="col-sm-2">
                                      <select class="form-control" name="currency" required="required">
                                        <option>-Devise-</option>

                                        <option 
                                            <?=$this->oAdvert->currency == 'cfa' ? 'selected' : ''?>
                                            value="cfa">CFA</option>
                                        <option 
                                            <?=$this->oAdvert->currency == '€' ? 'selected' : ''?>
                                            value="€">€</option>
                                        <option 
                                            <?=$this->oAdvert->currency == '$' ? 'selected' : ''?>
                                            value="$">USD</option>
                                      </select> 
                                </div>
                            </div>
                            <div class="form-group"> 
                                <label for="content" class="col-sm-2 control-label">Contenu</label> 
                                <div class="col-sm-8"> 
                                    <textarea class="form-control" name="content" rows="5" id="content"><?=htmlspecialchars($this->oAdvert->content)?></textarea>
                                </div> 
                            </div>
                            <div class="form-group"> 
                                <label for="location" class="col-sm-2 control-label">Lieu de l'annonce</label>
                                <div class="col-sm-8"> 
                                    <input type="text" name="location" class="form-control" id="location" required="required" value="<?=htmlspecialchars($this->oAdvert->location)?>"> 
                                </div> 
                            </div>
                            <div class="form-group">
                                <label for="category" class="col-sm-2 control-label">Categorie:</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="category_id" id="category">
                                        <option>Choisir une categorie</option>
                                        <?php if (!empty($this->categoryList)): ?>

                                            <?php foreach($this->categoryList as $category): ?>
                                                <option 
                                                    <?php if ($category->id == $this->oAdvert->category_id): ?> 
                                                         selected="selected"
                                                    <?php endif ?>
                                                    value="<?= $category->id ?>">
                                                    <?= $category->name ?> 
                                                </option>
                                            
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="type" class="col-sm-2 control-label">Type d'annonce:</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="type" id="type">
                                      <option>Choisir un type</option>
                                      <option 
                                            <?php if( $this->oAdvert->type == 'standard'): ?>
                                                selected
                                            <?php endif ?>
                                            value="standard">standard
                                      </option>
                                      <option 
                                            <?php if( $this->oAdvert->type == 'sponsored'): ?>
                                                selected
                                            <?php endif ?>
                                            value="sponsored">sponsorisée
                                      </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group"> 
                                <div class="col-md-offset-2 col-md-6"> 
                                    <label for="image"></label> 
                                    <input type="file" name="image" id="image">
                                </div>
                                <div class="col-md-4" style="padding-top:1.2em;"> 
                                    <?php if( $this->oAdvert->image_url): ?>
                                        <img src="<?= ROOT_URL . 'static/images/advert/'.$this->oAdvert->image_url?>" alt="" height="120" width="120" class="img-thumbnail">
                                    <?php endif ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="images" class="col-md-2" >Plus d'images</label>
                                <div class="col-md-4 hidden">
                                    <input type="file" name="images[]" class="hidden" id="duplicate">
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <?php foreach($this->oAdvertImages as $image): ?>
                                         <div class="col-md-3" style="position: relative;">
                                            <img src="<?= ROOT_URL . 'static/images/advert/'.$image->url?>" alt="" class="img-thumbnail" height="120" width="120">
                                            <a href="" class="btn btn-danger btn-xs" style="position: absolute;top: 0;right: 0;"><i class="fa fa-trash"></i></a>
                                         </div>
                                        <?php endforeach ?>
                                    </div><div class="clearfix"></div>
                                </div><div class="clearfix"></div>
                            </div><div class="clearfix"></div>

                            <div class="col-sm-offset-2"> 
                                <a href="#" class="btn btn-success btn-xs" id="duplicatebtn">Ajouter image</a>
                            </div><br><br>
                            <div class="form-group"> 
                                <label for="status" class="col-sm-2 control-label">Mise en ligne</label>
                                <div class="col-sm-8"> 
                                    <div class="radio block">
                                        <label class="radio-inline"> 
                                            <input type="radio" name="status" value="1" 
                                            <?php if( $this->oAdvert->status == '1'): ?>
                                                checked
                                            <?php endif ?>> Publier  
                                        </label>
                                    </div>
                                    <div class="radio block">
                                        <label class="radio-inline">
                                            <input type="radio" name="status" value="0"
                                            <?php if( $this->oAdvert->status == '0'): ?>
                                                checked
                                            <?php endif ?>> Ne pas Publier  
                                        </label>
                                    </div> 
                                </div> 
                            </div> 
                            <div class="col-sm-offset-2"> 
                                <input type="submit" name="edit_submit" value="Editer" class="btn btn-info"/>
                            </div> 
                        </form> 
                    </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- main content end-->

<?php require dirname(__DIR__) . '/inc/admin_footer.php' ?>
