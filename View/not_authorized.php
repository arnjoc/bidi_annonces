<?php require SITE_ROOT.'/view/public/inc/header.php';?>
<?php require SITE_ROOT.'/view/public/inc/banner.php';?>

<!--breadcrumb start-->
<?php require SITE_ROOT.'/view/public/inc/breadcrumb.php';?>
<!--breadcrumb end-->
<div class="car-loan-mid w3l" style="padding-bottom: 48px;">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3 text-center notfound-page">
			    <h1 class="">401</h1>
			    <h2 class="">Vous ne pouvez pas accéder à cette page ou effectuez cette action.</h2>
			    <p>Vérifiez le lien</p>
				<div>
					<a href="<?=ROOT_URL?>" class="btn btn-default"><i class="fa fa-home"></i> <i class="fa fa-angle-double-left"></i> &nbsp;Retour à l'Acceuil</a>
				</div>
			    
			</div>
		</div>
	</div>
</div>

<?php require SITE_ROOT.'/view/public/inc/footer.php';?>
