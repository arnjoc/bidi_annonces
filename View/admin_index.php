<?php require 'inc/admin_header.php' ?>

<!-- main content start-->
<div id="page-wrapper">
	<div class="main-page">
		<h2 class="title1">Acceuil Tableau de Bord</h2>
		<div class="col_3">
        	<div class="col-md-3 widget widget1">
        		<div class="r3_counter_box">
                    <i class="pull-left fa fa-users dollar2 icon-rounded"></i>
                    <div class="stats">
                      <h5><strong><?= $this->userCount ?></strong></h5>
                      <span>Utilisateurs</span>
                    </div>
                </div>
        	 </div>
        	 <div class="col-md-3 widget widget1">
        		<div class="r3_counter_box">
                    <i class="pull-left fa fa-th user2 icon-rounded"></i>
                    <div class="stats">
                      <h5><strong><?= $this->advertCount ?></strong></h5>
                      <span>Annonces</span>
                    </div>
                </div>
        	 </div>
        	 <div class="col-md-3 widget widget1">
        		<div class="r3_counter_box">
                    <i class="pull-left fa fa-bullhorn user1 icon-rounded"></i>
                    <div class="stats">
                      <h5><strong><?= $this->pubCount ?></strong></h5>
                      <span>Publicités</span>
                    </div>
                </div>
        	 </div>
        	 <div class="col-md-3 widget widget1">
        		<div class="r3_counter_box">
                    <i class="pull-left fa fa-list dollar1 icon-rounded"></i>
                    <div class="stats">
                      <h5><strong><?= $this->categoriesCount ?></strong></h5>
                      <span>Categories</span>
                    </div>
                </div>
        	 </div>
        	<div class="clearfix"> </div>
		</div>
	</div>
</div>
<!-- main content end-->

<?php require  'inc/admin_footer.php' ?>
