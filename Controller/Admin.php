<?php

namespace Bidi\Controller;

use Bidi\Model\User;
use Bidi\Model\Advert;
use Bidi\Model\Pub;
use Bidi\Model\Category;

class Admin extends Security
{

    // Admin Homepage
    public function index()
    {
        $this->hasRoleAdmin();

        $this->oUtil->userCount = count((new User())->getAll());
        $this->oUtil->advertCount = count((new Advert())->getAll());
        $this->oUtil->pubCount = count((new Pub())->getAll());
        $this->oUtil->categoriesCount = count((new Category())->getAll());
        $this->oUtil->getView('admin_index');
    }
}
