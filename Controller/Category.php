<?php

namespace Bidi\Controller;

class Category extends Base
{
    const MAX_CATEGORY = 5;

    public function __construct()
    {
        parent::__construct(
            'Category', 
            isset($_GET['id']) ? $_GET['id'] : null, 
            ROOT_URL."admin/category/all");
    }

    /***** Front end *****/
    // Homepage
    public function index()
    {
        $this->oUtil->oCategory = $this->oModel->get(0, self::MAX_CATEGORY); // Get only the latest X posts

        $this->oUtil->getView('category/index');
    }

    /***** For Admin (Back end) *****/
    public function all()
    {
        $this->auth->hasRoleAdmin();

        $this->oUtil->oHead = 'Annonces';
        $this->oUtil->oLinkText = 'category';

        $this->oUtil->oCategories = $this->oModel->getAll();

        $this->oUtil->getView('category/index');
    }

    public function add()
    {

        $this->auth->hasRoleAdmin();
        
        if (!empty($_POST['add_submit']))
        {
            $safeData = [];
            foreach ($_POST as $name => $value) {
                $safeData[$name] = $this->test_input($value);
            }

            if(isset($safeData['name']))
            {
                if ($this->isUniq($safeData['name']))
                {
                    $safeData['slug'] = static::sluggify($safeData['name']);
                    $safeData['icon'] = !empty($safeData['icon']) ? $safeData['icon'] : 'tags' ;
                    $safeData['created_at'] = date("Y-m-d H:i:s");

                    $image_id ='';
                    if(!empty($_FILES['image']["tmp_name"])){
                        $image_id = $this->singleUpload($_FILES['image'], 'category_id', $this->_iId);
                    }

                    $aData = array('name' => $safeData['name'], 'slug' => $safeData['slug'], 'description' => $safeData['description'], 'icon' => $safeData['icon'], 'image_id' => $image_id, 'created_at' => $safeData['created_at']);
                    
                    if ($this->oModel->add($aData))
                        $this->oUtil->sSuccMsg = 'La categorie a bien été ajoutée.';
                    else
                        $this->oUtil->sErrMsg = 'oops! Il y a une erreur! Veuillez reéssayer.';

                }
                else {
                        $this->oUtil->sErrMsg = 'Ce nom de catégorie exist déjà.';
                     }                        
            }
            else
            {
                $this->oUtil->sErrMsg = 'Veuillez remplir tous les champs.';
            }
        }

        $this->oUtil->oHead = 'Annonces';
        $this->oUtil->oLinkText = 'category';

        $this->oUtil->getView('category/new_Category');
    }

    public function edit()
    {
        $this->auth->hasRoleAdmin();

        if (!empty($_POST['edit_submit']))
        {
            $safeData = [];
            foreach ($_POST as $name => $value) {
                $safeData[$name] = $this->test_input($value);
            }

            if (isset($safeData['name']) && $this->isUniq('name', $safeData['name']))
            {
                $category = $this->oModel->getById($this->_iId);

                $safeData['slug'] = static::sluggify($safeData['name']);
                $safeData['icon'] = !empty($safeData['icon']) ? $safeData['icon'] : 'tags' ;
                $safeData['created_at'] = date("Y-m-d H:i:s");

                $image_id = isset($category->image_id) ? $category->image_id : '';

                if(!empty($_FILES['image']["tmp_name"])){
                    $image_id = $this->singleUpload($_FILES['image'], 'category_id', $this->_iId, $image_id);
                }

                $aData = array('category_id' => $this->_iId, 'name' => $safeData['name'], 'slug' => $safeData['slug'], 'description' => $safeData['description'], 'icon' => $safeData['icon'], 'image_id' => $image_id);


                if ($this->oModel->update($aData))
                    $this->oUtil->sSuccMsg = 'Bravo, la categorie a bien mise à jour.';
                else
                    $this->oUtil->sErrMsg = 'oops! il y a une erreur! Veuillez reéssayer';
            }
            else
            {
                $this->oUtil->sErrMsg = 'Tous les champs sont requis.';
            }
        }

        $this->oUtil->oHead = 'Annonces';
        $this->oUtil->oLinkText = 'category';
        /* Get the data of the post */
        $this->oUtil->oCategory = $this->oModel->getById($this->_iId);

        $this->oUtil->getView('category/edit_Category');
    }

}
