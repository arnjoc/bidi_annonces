<?php

namespace Bidi\Controller;

use Bidi\Model\PubCategory;

class User extends Base
{
    const MAX_USER = 5;

    public function __construct()
    {
        parent::__construct(
            'User', 
            isset($_GET['id']) ? $_GET['id'] : null, 
            ROOT_URL."admin/user/all");
    }

    /***** Front end *****/
    // Homepage
    public function index()
    {
        $this->oUtil->oUser = $this->oModel->get(0, self::MAX_PUB); // Get only the latest X posts

        $this->oUtil->getView('user/index');
    }

    /***** For Admin (Back end) *****/
    public function all()
    {
        $this->auth->hasRoleAdmin();

        $this->oUtil->oUsers = $this->oModel->getAll();

        $this->oUtil->getView('user/index');
    }

    public function show()
    {
        $this->auth->hasRoleAdmin();

        $this->oUtil->oUser = $this->oModel->getById($this->_iId); // Get the data of the post

        $this->oUtil->getView('user/show');
    }

    public function add()
    {
        $this->auth->hasRoleAdmin();
        
        if (!empty($_POST['add_submit']))
        {

            $safeData = [];
            foreach ($_POST as $name => $value) {
                $safeData[$name] = $this->test_input($value);
            }
            if( !$this->chckMailExist($safeData['email'])){

            if (isset($safeData['name'], $safeData['email'], $safeData['password']) && $safeData['password'] == $safeData['password_confirm'])
            {
                $safeData['enabled'] = isset($safeData['enabled']) ? $safeData['enabled'] : false;
                $safeData['confirm_token'] = isset($safeData['confirm_token']) ? $safeData['confirm_token'] : '';
                $safeData['country'] = isset($safeData['country']) ? $safeData['country'] : '';
                $safeData['town'] = isset($safeData['town']) ? $safeData['town'] : '';
                $safeData['phone'] = isset($safeData['phone']) ? $safeData['phone'] : '';

                $safeData['created_at'] = date("Y-m-d H:i:s");
                if($safeData['enabled'] == false )
                    $safeData['confirm_token'] = uniqid().date("Y-m-d h:i:sa");

                if(empty($safeData['role']))
                    $safeData['role'] = 'ROLE_USER';

                $safeData['password'] = password_hash($safeData['password'] , PASSWORD_BCRYPT, array('cost' => 14));

                $aData = array('name' => $safeData['name'], 'username' => $safeData['username'], 'email' => $safeData['email'], 'phone' => $safeData['phone'], 'password' => $safeData['password'], 'country' => $safeData['country'], 'town' => $safeData['town'], 'enabled' => $safeData['enabled'], 'role' => $safeData['role'], 'confirm_token' => $safeData['confirm_token'], 'created_at' => $safeData['created_at']);
                
                if ($this->oModel->add($aData))
                    $this->oUtil->sSuccMsg = 'L\'utilisateur a bien été ajouté.';
                else
                    $this->oUtil->sErrMsg = 'oops! Il y a une erreur! Veuillez reéssayer.';
            }
            else
            {
                $this->oUtil->sErrMsg = 'Veuillez remplir tous les champs.';
            }
        } else {
            $this->oUtil->sErrMsg = 'Un compte exist déjà avec cet email';
        }
        }

        $this->oUtil->categoryList = (new PubCategory())->getAll();

        $this->oUtil->getView('user/new_user');
    }

    public function edit()
    {
    
        $this->auth->hasRoleAdmin();

        if (!empty($_POST['edit_submit']))
        {
            $pub = $this->oModel->getById($this->_iId);
            
            $safeData = [];
            foreach ($_POST as $name => $value) {
                $safeData[$name] = $this->test_input($value);
            }

            if (isset($safeData['name'], $safeData['email'], $safeData['password']) && $safeData['password'] == $safeData['password_confirm'])
            {
                $safeData['enabled'] = isset($safeData['enabled']) ? $safeData['enabled'] : false;
                $safeData['country'] = isset($safeData['country']) ? $safeData['country'] : '';
                $safeData['phone'] = isset($safeData['phone']) ? $safeData['phone'] : '';
                $safeData['town'] = isset($safeData['town']) ? $safeData['town'] : '';

                if($safeData['enabled'] == false )
                    $safeData['confirm_token'] = uniqid().date("Ymdhis");

                if(empty($safeData['role']))
                    $safeData['role'] = 'ROLE_USER';

                $safeData['password'] = password_hash($safeData['password'] , PASSWORD_BCRYPT, array('cost' => 14));
                

                $aData = array('user_id' => $this->_iId, 'name' => $safeData['name'], 'username' => $safeData['username'], 'email' => $safeData['email'], 'phone' => $safeData['phone'], 'password' => $safeData['password'], 'country' => $safeData['country'], 'town' => $safeData['town'], 'enabled' => $safeData['enabled'], 'role' => $safeData['role'], 'confirm_token' => $safeData['confirm_token']);
                
                if ($this->oModel->update($aData))
                    $this->oUtil->sSuccMsg = 'Bravo, l\'utilisateur a bien été mise à jour.';
                else
                    $this->oUtil->sErrMsg = 'oops! il y a une erreur! Veuillez reéssayer';
            }
            else
            {
                $this->oUtil->sErrMsg = 'Tous les champs sont requis.';
            }
        }

        /* Get the data of the post */
        $this->oUtil->oUser = $this->oModel->getById($this->_iId);

        $this->oUtil->getView('user/edit_user');
    }

    public function chckMailExist ($email) 
    {
        return $checker = $this->oModel->isMailExist($email);
    }

}
