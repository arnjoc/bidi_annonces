<?php

namespace Bidi\Controller;

use Bidi\Model\Category;
use Bidi\Controller\Security;

class Advert extends Base
{
    const MAX_ADVERTS = 5;

    public function __construct()
    {
        parent::__construct(
            'Advert', 
            isset($_GET['id']) ? $_GET['id'] : null, 
            ROOT_URL."admin/advert/all");
    }


    /***** Front end *****/
    // Homepage
    public function index()
    {
        $this->oUtil->oAdverts = $this->oModel->get(0, self::MAX_ADVERTS); // Get only the latest X posts

        $this->oUtil->getView('advert/index');
    }

    public function show()
    {
        $this->oUtil->oPost = $this->oModel->getById($this->_iId); // Get the data of the post

        $this->oUtil->getView('post');
    }

    public function notFound()
    {
        $this->oUtil->getView('not_found');
    }

    /***** For Admin (Back end) *****/
    public function all()
    { 
        $this->auth->hasRoleAdmin();

        $this->oUtil->oAdverts = $this->oModel->getWithCat();

        $this->oUtil->getView('advert/index');
    }

    public function add()
    {
        $this->auth->hasRoleAdmin();

        if (!empty($_POST['add_submit']))
        {
            $safeData = [];
            foreach ($_POST as $name => $value) {
                $safeData[$name] = $this->test_input($value);
            }

            if (isset($safeData['title'], $safeData['content']) && mb_strlen($safeData['title']) <= 50)
            {
                $image_id = '';
                if(!empty($_FILES['image']['tmp_name'])){
                    $image_id = $this->singleUpload($_FILES['image'], 'advert_id', $this->_iId);
                }

                $safeData['slug'] = static::sluggify($safeData['title']);
                $safeData['created_at'] = date('Y-m-d H:i:s');
                $safeData['updated_at'] = date('Y-m-d H:i:s');
                $safeData['author_id'] = $_SESSION['auth']->id;
                $safeData['image_id'] = $image_id;
                $safeData['published_at'] = '';

                if($safeData['status'] == '1')
                    $safeData['published_at'] = date('d-m-Y H:i:s');
                
                $data = array('title' => $safeData['title'], 'slug' => $safeData['slug'], 'price' => $safeData['price'], 'currency' => $safeData['currency'], 'content' => $safeData['content'], 'location' => $safeData['location'], 'category_id' => $safeData['category_id'], 'type' => $safeData['type'], 'status' => $safeData['status'], 'author_id' => $safeData['author_id'], 'image_id' => $safeData['image_id'], 'created_at' => $safeData['created_at'], 'updated_at' => $safeData['updated_at'], 'published_at' => $safeData['published_at'],);

                $returnedValues = $this->oModel->add($data);

                if ($returnedValues['status']){
                    if(!empty($_FILES['images'])){
                        $image_id = $this->multiUpload($_FILES['images'], 'advert_id', $returnedValues['parent_id']);
                    }
                    $this->oUtil->sSuccMsg = 'L\'annonce a bien été ajoutée.';
                    header('Location: ' . ROOT_URL."admin/advert/edit/".$returnedValues['parent_id']);
                    exit;
                }
                else{
                    $this->oUtil->sErrMsg = 'oops! Il y a une erreur! Veuillez reéssayer.';
                }
            }
            else
            {
                $this->oUtil->sErrMsg = 'Veuillez remplir tous les champs.';
            }
        }

        $this->oUtil->categoryList = (new Category())->getAll();

        //echo'<pre>';print_r($category->getAll()); echo'</pre>';die();
        $this->oUtil->getView('advert/new_advert');
    }

    public function edit()
    {
        $this->auth->hasRoleAdmin();

        if (!empty($_POST['edit_submit']))
        {   
            //$this->dump($_FILES);
            $advert = $this->oModel->getBy($this->_iId);
            $safeData = [];
            foreach ($_POST as $name => $value) {
                $safeData[$name] = $this->test_input($value);
            }

            if (isset($safeData['title'], $safeData['content']))
            {
                $image_id = isset($advert->image_id) ? $advert->image_id : null;

                if(!empty($_FILES['image']['tmp_name'])){
                    $image_id = $this->singleUpload($_FILES['image'], 'advert_id', $this->_iId, $image_id);
                }

                $safeData['slug'] = static::sluggify($safeData['title']);
                $safeData['updated_at'] = date("d-m-Y h:i:sa");
                $safeData['author_id'] = $_SESSION['auth']->id;
                $safeData['image_id'] = $image_id;

                $aData = array('advert_id' => $this->_iId, 'title' => $safeData['title'], 'slug' => $safeData['slug'], 'price' => $safeData['price'], 'currency' => $safeData['currency'], 'content' => $safeData['content'], 'location' => $safeData['location'], 'category_id' => $safeData['category_id'], 'image_id' => $safeData['image_id'], 'type' => $safeData['type'], 'status' => $safeData['status'], 'author_id' => $safeData['author_id'], 'updated_at' => $safeData['updated_at']);


                if ($this->oModel->updateById($aData)){
                    if(!empty($_FILES['images'])){
                        $image_id = $this->multiUpload($_FILES['images'], 'advert_id', $this->_iId);
                    }

                    $this->oUtil->sSuccMsg = 'Bravo, l\'annonce a bien mise à jour.';
                }

                else
                    $this->oUtil->sErrMsg = 'oops! il y a une erreur! Veuillez reéssayer';
            }
            else
            {
                $this->oUtil->sErrMsg = 'Tous les champs sont requis.';
            }
        }

        $this->oUtil->categoryList = (new Category())->getAll();
        /* Get the data of the post */
        $this->oUtil->oAdvert = $this->oModel->getBy($this->_iId);
        $this->oUtil->oAdvertImages = $this->oModel->getAdvertImages($this->_iId);

        $this->oUtil->getView('advert/edit_advert');
    }
}
