<?php

namespace Bidi\Controller;

class Security
{
    protected $oUtil, $oModel;
    private $_iId;

    public function __construct()
    {
        // Enable PHP Session
        if (empty($_SESSION))
            @session_start();

        $this->oUtil = new \Bidi\Engine\Util;

        /** Get the Model class in all the controller class **/
        $this->oUtil->getModel('Security');
        $this->oModel = new \Bidi\Model\Security;
    }

    public function login()
    {
        if ($this->isLogged()) {

            header('Location: ' . ROOT_URL);
        }

        if (isset($_POST['email'], $_POST['password']))
        {

            $currentUser =  $this->oModel->login($_POST['email']);

            if (!empty($currentUser) && password_verify($_POST['password'], $currentUser->password))
            {
                $_SESSION['is_logged'] = 1; // Admin is logged now
                $_SESSION['auth'] = $currentUser; // Admin is logged now

                // Redirect to Dashboard if user has Admin role
                if($currentUser->role == 'ROLE_ADMIN')
                    header('Location: ' . ROOT_URL . 'admin');exit;

                 // Admin is logged now
                header('Location: ' . ROOT_URL);
                exit;
            }
            else
            {
                $this->oUtil->sErrMsg = 'Email ou Mot de passe incorrect!';
            }
        }

        header('Location: ' . ROOT_URL . 'login');
        exit;
    }

    public function logout()
    {
        if (!$this->isLogged())
            exit;

        // If there is a session, destroy it to disconnect the admin
        if (!empty($_SESSION))
        {
            $_SESSION = array();
            session_unset();
            session_destroy();
        }

        // Redirect to the homepage
        header('Location: ' . ROOT_URL);
        exit;
    }

    protected function isLogged()
    {
        return !empty($_SESSION['is_logged']);
    }

    public function oAuth()
    {
        if (!$this->isLogged()){
            return header('Location: ' . ROOT_URL . 'login'); exit;
        }

        return;
    }

    public function hasRoleAdmin()
    {
        if (!$this->isLogged()){
            return header('Location: ' . ROOT_URL . 'login'); exit;
        }
        //var_dump($_SESSION); die();
        if($_SESSION['auth']->role == 'ROLE_ADMIN'){
           return;
        }

        return header('Location: ' . ROOT_URL . 'front/notauthorized'); exit;
    }
}
