<?php

namespace Bidi\Controller;

use Bidi\Model\Category;
use Bidi\Model\Advert;
use Bidi\Model\Pub;
use Bidi\Model\Setting;
use Bidi\Model\User;
use Bidi\Model\Contact;
use Bidi\Model\Image;

class Front
{
    const MAX_ADVERTS = 5;
    protected $oUtil, $oModel;
    private $oDb;
    protected $osetting;
    protected $_iId;
    protected $_slug;

    public function __construct()
    {
        // Enable PHP Session
        if (empty($_SESSION))
            @session_start();

        $this->oUtil = new \Bidi\Engine\Util;
        $this->oDb = new \Bidi\Engine\Db;
        $this->oUtil->gSetting = (new Setting())->getById();
        $this->oUtil->gCategories = (new Category())->getAll();
        $this->oUtil->gCurrentPage = 'Acceuil';
        $this->oUtil->oSinglePub = (new Pub())->randomActivePub();
        /** Get the Model class in all the controller class **/
        //$this->oUtil->getModel('Blog');
        //$this->oModel = new \Bidi\Model\Blog;
        $this->_iId = (int) (!empty($_GET['id']) ? $_GET['id'] : 0);
        $this->_slug = (string) (!empty($_GET['slug']) ? $_GET['slug'] : '');
    }


    /***** Front end *****/
    // Homepage
    public function index()
    {
        $categories  = new Category();
        $advert  = new Advert();
        $signlePub  = new Pub();
        
        $this->oUtil->oCategories = $categories->getAll();
        $this->oUtil->oAdverts = $advert->loadActive();
        $this->oUtil->oSinglePub = $signlePub->randomActivePub();

        $this->oUtil->getView('public/index');
    }

    // Category Page
    public function categories()
    {

        $category  = new Category();
        $advert  = new Advert();
        $signlePub  = new Pub();

        if($this->_iId != 0){
            $this->oUtil->oAdverts = $advert->getAllByCategory($this->_iId );
        }
        
        $this->oUtil->oCategories = $category->getAll();
        $this->oUtil->gCurrentPage = 'Catégories';
        $this->oUtil->getView('public/category');
    }

    // Category Page
    public function avatarChange()
    {
        if (!$this->isLogged()){
            header('Location: ' . ROOT_URL.'login');
                    exit;
        }

        $userId = (int) $_GET['slug'];

        if($_SESSION['auth']->id == $userId){
            $user = new User();
            if(!empty($_FILES['avatar']["tmp_name"])){
               $image_id = $this->singleUpload($_FILES['avatar'], 'user_id');
               if(!empty($image_id)){
                    $user->setImage($userId, $image_id);
               }
                header('Location: ' . ROOT_URL.'profile');
                    exit;
            }
            header('Location: ' . ROOT_URL.'profile');
                    exit;
        }

        $this->dump($_SESSION['auth']->id, $userId, $_FILES['avatar']);
        header('Location: ' . ROOT_URL.'notAuthorized');
                    exit;
        
    }

    // Category Page
    public function category()
    {
        //$this->dump($this->_slug);
        $category  = new Category();
        $advert  = new Advert();
        $signlePub  = new Pub();

        if(null != $this->_slug){
            $this->oUtil->oAdvertsList = $advert->getAllByCategory($this->_slug);
        }

        $this->oUtil->oRecentAdverts = $advert->loadActive();
        $this->oUtil->oSinglePub = $signlePub->randomActivePub();
        $this->oUtil->oCategories = $category->getAll();
        $this->oUtil->gCurrentPage = 'Catégories';
        $this->oUtil->getView('public/category_adverts');
    }

    // Show an Advert
    public function advert()
    {
        $category  = new Category();
        $advert  = new Advert();
        $signlePub  = new Pub();

        if(null != $this->_slug){
            $this->oUtil->oSingleAdvert = $advert->getBySlug($this->_slug);
        }

        //$this->dump($this->oUtil->oSingleAdvert);

        $this->oUtil->oRelativeAdverts = $advert->getAllByCategory($this->oUtil->oSingleAdvert->category_name);
        
        $this->oUtil->oCategories = $category->getAll();
        $this->oUtil->gCurrentPage = 'Catégories';
        $this->oUtil->getView('public/single_advert');
    }

    public function register()
    {
        $category  = new Category();
        $signlePub  = new Pub();
        $user  = new User();

        if (!empty($_POST['sign_up']))
        {
            $safeData = [];
            foreach ($_POST as $name => $value) {
                $safeData[$name] = $this->test_input($value);
            }
            if( !$this->chckMailExist($user, $safeData['email'])){

                if (isset($safeData['name'], $safeData['email'], $safeData['password']) && $safeData['password'] == $safeData['password_confirm'])
                {
                    $safeData['enabled'] = isset($safeData['enabled']) ? $safeData['enabled'] : true;
                    $safeData['confirm_token'] = isset($safeData['confirm_token']) ? $safeData['confirm_token'] : '';
                    $safeData['country'] = isset($safeData['country']) ? $safeData['country'] : '';
                    $safeData['town'] = isset($safeData['town']) ? $safeData['town'] : '';
                    $safeData['phone'] = isset($safeData['phone']) ? $safeData['phone'] : '';

                    $safeData['created_at'] = date("Y-m-d H:i:s");
                    if($safeData['enabled'] == false )
                        $safeData['confirm_token'] = uniqid().date("Y-m-d h:i:sa");

                    if(empty($safeData['role']))
                        $safeData['role'] = 'ROLE_USER';

                    $safeData['password'] = password_hash($safeData['password'] , PASSWORD_BCRYPT, array('cost' => 14));

                    $aData = array('name' => $safeData['name'], 'username' => $safeData['username'], 'email' => $safeData['email'], 'password' => $safeData['password'], 'country' => $safeData['country'], 'town' => $safeData['town'], 'phone' => $safeData['phone'], 'enabled' => $safeData['enabled'], 'role' => $safeData['role'], 'confirm_token' => $safeData['confirm_token'], 'created_at' => $safeData['created_at']);
                    
                    if ($user->add($aData))
                        $this->oUtil->sSuccMsg = 'Félicitation! vous êtes maintenant inscris.';
                    else
                        $this->oUtil->sErrMsg = 'oops! Il y a une erreur! Veuillez reéssayer.';
                }
                else
                {
                    $this->oUtil->sErrMsg = 'Veuillez remplir tous les champs et verifier les mots de passe.';
                }
            } else {
                $this->oUtil->sErrMsg = 'Un compte exist déjà avec cet email';
            }
        }

        $this->oUtil->getView('public/register');
    }

    public function login()
    {
        if (!empty($_SESSION['is_logged'])) {

            header('Location: ' . ROOT_URL);
        }

        if (isset($_POST['email'], $_POST['password']))
        {
            $security = new \Bidi\Model\Security;
            $currentUser =  $security->login($_POST['email']);

            if (!empty($currentUser) && password_verify($_POST['password'], $currentUser->password))
            {
                $_SESSION['is_logged'] = 1;
                $_SESSION['auth'] = $currentUser;

                // Redirect to Dashboard if user has Admin role
                if($currentUser->role == 'ROLE_ADMIN')
                    header('Location: ' . ROOT_URL);exit;

                 // Admin is logged now
                header('Location: ' . ROOT_URL);
                exit;
            }
            else
            {
                $this->oUtil->sErrMsg = 'Email ou Mot de passe incorrect!';
            }
        }

        $this->oUtil->gCurrentPage = 'Connexion';
        $this->oUtil->getView('public/login');
    }

    public function logout()
    {
        if (!$this->isLogged())
            exit;

        // If there is a session, destroy it to disconnect the admin
        if (!empty($_SESSION))
        {
            $_SESSION = array();
            session_unset();
            session_destroy();
        }

        // Redirect to the homepage
        header('Location: ' . ROOT_URL);
        exit;
    }

    public function profile()
    {
        if (!$this->isLogged()){
            header('Location: ' . ROOT_URL.'login');
                    exit;
        }

        $this->oUtil->oUser = (new User())->getById($_SESSION['auth']->id);
        $this->oUtil->gCurrentPage = 'Profile';
        $this->oUtil->pToggle = 'show';
        $this->oUtil->getView('public/profile');
    }

    public function profileEdit()
    {
        if (!$this->isLogged()){
            header('Location: ' . ROOT_URL.'login');
                    exit;
        }

        $user = new User();
        
        $this->oUtil->oUser = (new User())->getById($_SESSION['auth']->id);

        if(isset($_POST['edit_profile'])){

            $safeData = [];
            foreach ($_POST as $name => $value) {
                $safeData[$name] = $this->test_input($value);
            }

            if(password_verify($_POST['password'], $this->oUtil->oUser->password)) {

                $safeData['name'] = isset($safeData['name']) ? $safeData['name'] : $this->oUtil->oUser->name ;
                $safeData['country'] = isset($safeData['country']) ? $safeData['country'] : $this->oUtil->oUser->country;
                $safeData['phone'] = isset($safeData['phone']) ? $safeData['phone'] : $this->oUtil->oUser->phone;
                $safeData['town'] = isset($safeData['town']) ? $safeData['town'] : $this->oUtil->oUser->town;

                $aData = array('user_id' => $this->oUtil->oUser->id, 'name' => $safeData['name'], 'phone' => $safeData['phone'], 'country' => $safeData['country'], 'town' => $safeData['town']);

                $this->oUtil->oUser = (new User())->getById($_SESSION['auth']->id);
                if ($user->profileUpdate($aData)){
                    $this->oUtil->sSuccMsg = 'Bravo, ton profile a bien été mise à jour.';
                    //header('Location: ' . ROOT_URL . '?p=front&a=profileEdit');exit;
                }
                else
                    $this->oUtil->sErrMsg = 'oops! il y a une erreur! Veuillez reéssayer';
            } else{
                $this->oUtil->sErrMsg = 'Mot de passe invalide.';
            }
        }

        $this->oUtil->gCurrentPage = 'Profile Edition';
        $this->oUtil->pToggle = 'edit';
        $this->oUtil->getView('public/profile');
    }

    public function profileEditPass()
    {
        if (!$this->isLogged()){
            header('Location: ' . ROOT_URL.'login');
                    exit;
        }

        $user = new User();
        
        $this->oUtil->oUser = (new User())->getById($_SESSION['auth']->id);
        
        if(isset($_POST['edit_pass_profile'])){

            if(password_verify($_POST['password_old'], $this->oUtil->oUser->password)) {
                if($_POST['password_new'] === $_POST['password_new_confirm']) {
                    if($_POST['password_new'] !== $_POST['password_old']) {
                        $newPassword = password_hash($_POST['password_new'] , PASSWORD_BCRYPT, array('cost' => 14));

                        $aData = array('user_id' => $this->oUtil->oUser->id, 'new_password' => $newPassword);

                        if ($user->profilePassUpdate($aData)){

                            $this->oUtil->oUser = (new User())->getById($_SESSION['auth']->id);

                            $this->oUtil->sSuccMsg = 'Bravo, ton mot de passe a bien été mise à jour.';

                            //header('Location: ' . ROOT_URL . '?p=front&a=profileEditPass');exit;
                        }
                        else
                            $this->oUtil->sErrMsg = 'oops! il y a une erreur! Veuillez reéssayer';
                    }else {
                        $this->oUtil->sErrMsg = 'Vous ne pouvez pas réutiliser l\'anciens mot de passe';
                    }
                } else {
                    $this->oUtil->sErrMsg = 'Les nouveaux mots de passes ne se correspondent pas';
                }

            } else{
                $this->oUtil->sErrMsg = 'Ancien mot de passe invalide.';
            }
        }

        $this->oUtil->gCurrentPage = 'Profile Changer mot de passe';
        $this->oUtil->pToggle = 'pass';
        $this->oUtil->getView('public/profile');
    }

    public function advertAdd()
    {
        if (!$this->isLogged()){
            header('Location: ' . ROOT_URL.'login');
                    exit;
        }

        if (!empty($_POST['add_submit']))
        {
            $safeData = [];
            foreach ($_POST as $name => $value) {
                $safeData[$name] = $this->test_input($value);
            }

            if (isset($safeData['title'], $safeData['content']) && mb_strlen($safeData['title']) <= 50)
            {
                $image_id = '';
                if(!empty($_FILES['image']["tmp_name"])){
                    $image_id = $this->singleUpload($_FILES['image'], 'advert_id', $this->_iId);
                }

                $safeData['slug'] = static::sluggify($safeData['title']);
                $safeData['created_at'] = date('Y-m-d H:i:s');
                $safeData['updated_at'] = date('Y-m-d H:i:s');
                $safeData['author_id'] = $_SESSION['auth']->id;
                $safeData['image_id'] = $image_id;
                $safeData['published_at'] = '';

                if($safeData['status'] == '1')
                    $safeData['published_at'] = date('d-m-Y H:i:s');
                
                $data = array('title' => $safeData['title'], 'slug' => $safeData['slug'], 'price' => $safeData['price'], 'currency' => $safeData['currency'], 'content' => $safeData['content'], 'location' => $safeData['location'], 'category_id' => $safeData['category_id'], 'type' => $safeData['type'], 'status' => $safeData['status'], 'author_id' => $safeData['author_id'], 'image_id' => $safeData['image_id'], 'created_at' => $safeData['created_at'], 'updated_at' => $safeData['updated_at'], 'published_at' => $safeData['published_at'],);

                $returnedValues = (new Advert())->add($data);

                if ($returnedValues['status']){
                    if(!empty($_FILES['images'])){
                        $image_id = $this->multiUpload($_FILES['images'], 'advert_id', $returnedValues['parent_id']);
                    }
                    $this->oUtil->sSuccMsg = 'L\'annonce a bien été ajoutée.';
                    /*header('Location: ' . ROOT_URL."?p=front&a=advertedit&id=".$returnedValues['parent_id']);
                    exit;*/
                }
                else{
                    $this->oUtil->sErrMsg = 'oops! Il y a une erreur! Veuillez reéssayer.';
                }
            }
            else
            {
                $this->oUtil->sErrMsg = 'Veuillez remplir tous les champs.';
            }
        }

        $this->oUtil->categoryList = (new Category())->getAll();

        $this->oUtil->gCurrentPage = 'Ajouter une annonce';
        $this->oUtil->pToggle = 'advert_add';
        $this->oUtil->getView('public/profile');
    }

    public function advertEdit()
    {
        if (!$this->isLogged()){
            header('Location: ' . ROOT_URL.'login');
                    exit;
        }

        if (!empty($_POST['edit_submit']))
        {   
            $advert = (new Advert())->getBySlug($this->_slug);
            $safeData = [];
            foreach ($_POST as $name => $value) {
                $safeData[$name] = $this->test_input($value);
            }

            if (isset($safeData['title'], $safeData['content']))
            {
                //$this->dump(empty($_FILES['image']["tmp_name"]));
                $image_id = isset($advert->image_id) ? $advert->image_id : '';

                if(!empty($_FILES['image']["tmp_name"])){
                    $image_id = $this->singleUpload($_FILES['image'], 'advert_id', $this->_iId, $image_id);
                }

                $safeData['slug'] = static::sluggify($safeData['title']);
                $safeData['updated_at'] = date("d-m-Y h:i:sa");
                $safeData['author_id'] = $_SESSION['auth']->id;
                $safeData['image_id'] = $image_id;

                $aData = array('advert_slug' => $this->_slug, 'title' => $safeData['title'], 'slug' => $safeData['slug'], 'price' => $safeData['price'], 'currency' => $safeData['currency'], 'content' => $safeData['content'], 'location' => $safeData['location'], 'category_id' => $safeData['category_id'], 'image_id' => $safeData['image_id'], 'type' => $safeData['type'], 'status' => $safeData['status'], 'author_id' => $safeData['author_id'], 'updated_at' => $safeData['updated_at']);


                if ((new Advert())->update($aData)){
                    if(!empty($_FILES['images'])){
                        $image_id = $this->multiUpload($_FILES['images'], 'advert_id', $advert->id);
                    }

                    $this->oUtil->sSuccMsg = 'Bravo, l\'annonce a bien été mise à jour.';
                }
                else
                    $this->oUtil->sErrMsg = 'oops! il y a une erreur! Veuillez reéssayer';
            }
            else
            {
                $this->oUtil->sErrMsg = 'Tous les champs sont requis.';
            }
        }

        $this->oUtil->categoryList = (new Category())->getAll();
        /* Get the data of the post */
        $iAdvert = new Advert();
        $this->oUtil->oAdvert = $iAdvert->getBySlug($this->_slug);
        $this->oUtil->oAdvertImages = $iAdvert->getAdvertImages($this->oUtil->oAdvert->id);
        $this->oUtil->gCurrentPage = 'Editer une annonce';
        $this->oUtil->pToggle = 'advert_edit';
        $this->oUtil->getView('public/profile');
    }

    public function userAdvertDelete()
    {
        if (!$this->isLogged()){
            header('Location: ' . ROOT_URL.'login');
                    exit;
        }

        $advert = (new Advert())->getOneById($_POST['delete']);

        if($_SESSION['auth']->id != $advert->author_id) {
            $this->oUtil->sErrMsg = 'Vous n\'êtes pas l\'auteur de cette annonce.';
        } 
        else {

            $status = $this->delete($advert->id);

            if(!$status){
                
                $this->oUtil->sErrMsg = 'Erreur au cours de la suppression, veuillez reéssayer.';
            }
            elseif($status){
                $this->oUtil->sSuccMsg = 'Annonce supprimée avec success.';
            }
        }

        $this->oUtil->oAdverts = (new Advert())->getUserAdverts($_SESSION['auth']->id, 0, self::MAX_ADVERTS);
        $this->oUtil->gCurrentPage = 'Mes annonces';
        $this->oUtil->pToggle = 'advert_list';
        $this->oUtil->getView('public/profile');
    }

    /*
     * Show an advert
     * 
     */
    public function userAdvertShow()
    {
        if (!$this->isLogged()){
            header('Location: ' . ROOT_URL.'login');
                    exit;
        }

        $this->oUtil->oAdvert = (new Advert())->getBySlug($this->_slug);
        //$this->dump($this->oUtil->oAdvert);
        $this->oUtil->gCurrentPage = 'Annonce';
        $this->oUtil->pToggle = 'advert_show';
        $this->oUtil->getView('public/profile');
    }

    /*
     * List All the user Adverts
     * 
     */
    public function userAdvertsIndex()
    {
        $this->oUtil->oAdverts = (new Advert())->getUserAdverts($_SESSION['auth']->id, 0, self::MAX_ADVERTS);

        $this->oUtil->gCurrentPage = 'Mes annonces';
        $this->oUtil->pToggle = 'advert_list';
        $this->oUtil->getView('public/profile');
    }

    /*
     * Contact page
     * 
     */
    public function contactUs()
    {
        if(!empty($_POST['contact_form'])) {

            if (!empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['message']))
            {
                $safeData = [];
                foreach ($_POST as $name => $value) {
                    $safeData[$name] = $this->test_input($value);
                }

                $aData = array('name' => $safeData['name'], 'email' => $safeData['email'], 'subject' => $safeData['subject'], 'message' => $safeData['message'], 'sent_at' => date('Y-m-d H:i:s'));

                if ((new Contact())->add($aData))
                    $this->oUtil->sSuccMsg = 'Votre message a bien été ennvoyé.';
                else
                    $this->oUtil->sErrMsg = 'Il y a erreur, veuillez reéssayer.';
            }
            else
            {
                $this->oUtil->sErrMsg = 'Les champs Nom, Email et Message sont requis.';
            }
        }

        
        $this->oUtil->gCurrentPage = 'Contactez nous';
        $this->oUtil->getView('public/contact');
    }

    public function faq()
    {
        
        $this->oUtil->gCurrentPage = 'Commment ça marche';
        $this->oUtil->getView('public/faq');
    }

    public function termes()
    {
        $this->oUtil->gCurrentPage = 'Mentions légales';
        $this->oUtil->getView('public/termes');
    }

    public function search()
    {
        $category  = new Category();
        $advert  = new Advert();
        $signlePub  = new Pub();

        if(!empty($_POST['home_search'])) {
           if(!empty(trim($_POST['advert'])) || !empty(trim($_POST['category']))) { 
                $this->oUtil->oAdvertsList = $advert->search($_POST['advert'], $_POST['category']);
           }
        }
        $this->dump($this->oUtil->oAdvertsList);
        $this->oUtil->oRecentAdverts = $advert->loadActive();
        
        $this->oUtil->oCategories = $category->getAll();
        $this->oUtil->gCurrentPage = 'Annonces';
        $this->oUtil->getView('public/category_adverts');
    }

    public function notFound()
    {
        $this->oUtil->gCurrentPage = 'Erreur';
        $this->oUtil->getView('not_found');
    }

    public function notAuthorized()
    {
        $this->oUtil->gCurrentPage = 'Erreur';
        $this->oUtil->getView('not_authorized');
    }

    /***** For Admin (Back end) *****/
    public function all()
    {
        if (!$this->isLogged()) exit;

        $this->oUtil->oPosts = $this->oModel->getAll();

        $this->oUtil->getView('index');
    }


    public function add()
    {
        if (!$this->isLogged()) exit;

        if (!empty($_POST['add_submit']))
        {
            if (isset($_POST['title'], $_POST['body']) && mb_strlen($_POST['title']) <= 50) // Allow a maximum of 50 characters
            {
                $aData = array('title' => $_POST['title'], 'body' => $_POST['body'], 'created_date' => date('Y-m-d H:i:s'));

                if ($this->oModel->add($aData))
                    $this->oUtil->sSuccMsg = 'Hurray!! The post has been added.';
                else
                    $this->oUtil->sErrMsg = 'Whoops! An error has occurred! Please try again later.';
            }
            else
            {
                $this->oUtil->sErrMsg = 'All fields are required and the title cannot exceed 50 characters.';
            }
        }

        $this->oUtil->getView('add_post');
    }

    public function edit()
    {
        if (!$this->isLogged()) exit;

        if (!empty($_POST['edit_submit']))
        {
            if (isset($_POST['title'], $_POST['body']))
            {
                $aData = array('post_id' => $this->_iId, 'title' => $_POST['title'], 'body' => $_POST['body']);

                if ($this->oModel->update($aData))
                    $this->oUtil->sSuccMsg = 'Hurray! The post has been updated.';
                else
                    $this->oUtil->sErrMsg = 'Whoops! An error has occurred! Please try again later';
            }
            else
            {
                $this->oUtil->sErrMsg = 'All fields are required.';
            }
        }

        /* Get the data of the post */
        $this->oUtil->oPost = $this->oModel->getById($this->_iId);

        $this->oUtil->getView('edit_post');
    }

    public function deleteAdvertImage()
    {
        $advert = (new Advert())->getBySlug($_GET['slug']);
        //var_dump($advert); die();
        if (!$this->isLogged()){
            header('Location: ' . ROOT_URL.'login');
                    
        }
        if (empty($advert->author_id) || ($advert->author_id != $_SESSION['auth']->id)){
            header('Location: ' . ROOT_URL.'notAuthorized');
                    exit;
        }

        if((new Image())->deleteAdvertImage((int) $_GET['id'])){
            $this->oUtil->sSuccMsg = 'L\'image a bien été supprimée';
            header('Location: ' . ROOT_URL.'advertedit/'.$advert->slug);
                    exit;
        }
        else
            $this->oUtil->sErrMsg = 'Erreur de suppression de l\'image.';

        $this->oUtil->categoryList = (new Category())->getAll();
        /* Get the data of the post */
        $iAdvert = new Advert();
        $this->oUtil->oAdvert = $advert;
        $this->oUtil->oAdvertImages = $iAdvert->getAdvertImages($advert->id);
        $this->oUtil->gCurrentPage = 'Editer une annonce';
        $this->oUtil->pToggle = 'advert_edit';
        $this->oUtil->getView('public/profile');
    }

    public function delete($advertId)
    {
        if (!$this->isLogged()){
            header('Location: ' . ROOT_URL.'login');
                    exit;
        }

        if (!empty($_POST['delete'])){
            if((new Advert())->delete($advertId))
                return true;
        }
        
        return false;
    }

    protected function isLogged()
    {
        return !empty($_SESSION['is_logged']);
    }

    protected function test_input($data) {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
    }

    public function chckMailExist ($model, $email) 
    {
        return $checker = $model->isMailExist($email);
    }

    public function dump($elm) 
    {
        echo "<pre>"; var_dump($elm); echo "</pre>"; die();
    }

    public static function sluggify($str, $delimiter = '-'){

        $unwanted_array = ['ś'=>'s', 'ą' => 'a', 'ć' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ź' => 'z', 'ż' => 'z',
            'Ś'=>'s', 'Ą' => 'a', 'Ć' => 'c', 'Ę' => 'e', 'Ł' => 'l', 'Ń' => 'n', 'Ó' => 'o', 'Ź' => 'z', 'Ż' => 'z']; // Polish letters for example
        $str = strtr( $str, $unwanted_array );

        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
        return $slug;
    }

    protected function singleUpload($uploadedFile,$relationName, $parent_id=null, $oldFileId=null)
    {
        //echo "<pre>"; var_dump($uploadedFile,$relationName, $parent_id); echo "</pre>"; die();
        //$subdir = (explode('_', strtolower($relationName)))[0];
        $parent_id = $this->oDb->quote($parent_id);
        $files = $uploadedFile;
        $images = array();
        $uploadDir = SITE_ROOT . '\static\images\\'. (explode('_', strtolower($relationName)))[0] .'\\';

        $image = array(
            'name' => $files['name'],
            'tmp_name' => $files['tmp_name']
        );
    
        $extensions = pathinfo($image['name'], PATHINFO_EXTENSION);
        if(in_array($extensions, array('jpg', 'jpeg', 'png'))){
            //$this->oDb->query("INSERT INTO image SET $relationName=$parent_id");
            //$image_id = $this->oDb->lastInsertId();
            $image_name = uniqid() . '.'. $extensions;

            if(move_uploaded_file($image['tmp_name'], $uploadDir . $image_name)){
                $this->resizedImage($uploadDir . $image_name, 150,150);
                $image_name = $this->oDb->quote($image_name);
                //$this->oDb->query("UPDATE image SET url=$image_name WHERE id=$image_id");
                $this->oDb->query("INSERT INTO image SET url=$image_name");
                $image_id = $this->oDb->lastInsertId();
            }

            if(null != $oldFileId)
                $this->deleteImage($oldFileId, $image_id);

            return $image_id;

        } else {
            $this->oUtil->sErrMsg = "Seul les extensions: [jpg, jpeg, png] sont supportées";
        }
    }

    protected function multiUpload($uploadedFiles,$relationName, $parent_id, $oldFileId=null)
    {
        $parent_id = $this->oDb->quote($parent_id);
        $files = $uploadedFiles;
        $images = array();
        $uploadDir = SITE_ROOT . '\static\images\\'. (explode('_', strtolower($relationName)))[0] .'\\';
        foreach ($files['tmp_name'] as $k => $v) {
            $image = array(
                'name' => $files['name'][$k],
                'tmp_name' => $files['tmp_name'][$k]
            );
        
            $extensions = pathinfo($image['name'], PATHINFO_EXTENSION);
            if(in_array($extensions, array('jpg', 'jpeg', 'png'))){
                $image_name = uniqid() . '.'. $extensions;
                if(move_uploaded_file($image['tmp_name'], $uploadDir . $image_name)){
                    $this->resizedImage($uploadDir . $image_name, 150,150);
                    $image_name = $this->oDb->quote($image_name);
                    $this->oDb->query("INSERT INTO images SET url=$image_name, advert_id=$parent_id");
                }
            }
        }
    }

    protected function deleteImage($parent_id, $image_id=null)
    {
        $oStmt = $this->oDb->prepare('DELETE FROM image WHERE id = :imageId LIMIT 1');
        $oStmt->bindParam(':imageId', $oldFileId, \PDO::PARAM_INT);
        return $oStmt->execute();
    }
    public function resizedImage($file, $width, $height){
    
        # We find the right file
        $pathinfo   = pathinfo(trim($file, '/'));
        $output     = $pathinfo['dirname'] . '/' . $pathinfo['filename'] . '_' . $width . 'x' . $height . '.' . $pathinfo['extension'];
    
        # Setting defaults and meta
        $info                         = getimagesize($file);
        list($width_old, $height_old) = $info;
        # Create image ressource
        switch ( $info[2] ) {
            case IMAGETYPE_GIF:   $image = imagecreatefromgif($file);   break;
            case IMAGETYPE_JPEG:  $image = imagecreatefromjpeg($file);  break;
            case IMAGETYPE_PNG:   $image = imagecreatefrompng($file);   break;
            default: return false;
        }
        # We find the right ratio to resize the image before cropping
        $heightRatio = $height_old / $height;
        $widthRatio  = $width_old /  $width;
        $optimalRatio = $widthRatio;
        if ($heightRatio < $widthRatio) {
            $optimalRatio = $heightRatio;
        }
        $height_crop = ($height_old / $optimalRatio);
        $width_crop  = ($width_old  / $optimalRatio);
        # The two image ressources needed (image resized with the good aspect ratio, and the one with the exact good dimensions)
        $image_crop = imagecreatetruecolor( $width_crop, $height_crop );
        $image_resized = imagecreatetruecolor($width, $height);
        # This is the resizing/resampling/transparency-preserving magic
        if ( ($info[2] == IMAGETYPE_GIF) || ($info[2] == IMAGETYPE_PNG) ) {
            $transparency = imagecolortransparent($image);
            if ($transparency >= 0) {
                $transparent_color  = imagecolorsforindex($image, $trnprt_indx);
                $transparency       = imagecolorallocate($image_crop, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue']);
                imagefill($image_crop, 0, 0, $transparency);
                imagecolortransparent($image_crop, $transparency);
                imagefill($image_resized, 0, 0, $transparency);
                imagecolortransparent($image_resized, $transparency);
            }elseif ($info[2] == IMAGETYPE_PNG) {
                imagealphablending($image_crop, false);
                imagealphablending($image_resized, false);
                $color = imagecolorallocatealpha($image_crop, 0, 0, 0, 127);
                imagefill($image_crop, 0, 0, $color);
                imagesavealpha($image_crop, true);
                imagefill($image_resized, 0, 0, $color);
                imagesavealpha($image_resized, true);
            }
        }
        imagecopyresampled($image_crop, $image, 0, 0, 0, 0, $width_crop, $height_crop, $width_old, $height_old);
        imagecopyresampled($image_resized, $image_crop, 0, 0, ($width_crop - $width) / 2, ($height_crop - $height) / 2, $width, $height, $width, $height);
        # Writing image according to type to the output destination and image quality
        switch ( $info[2] ) {
          case IMAGETYPE_GIF:   imagegif($image_resized, $output, 80);    break;
          case IMAGETYPE_JPEG:  imagejpeg($image_resized, $output, 80);   break;
          case IMAGETYPE_PNG:   imagepng($image_resized, $output, 9);    break;
          default: return false;
        }
        return true;
    }
}
