<?php

namespace Bidi\Controller;

class Setting extends Base
{
    const MAX_CATEGORY = 1;

    public function __construct()
    {
        parent::__construct(
            'Setting', 
            isset($_GET['id']) ? $_GET['id'] : null, 
            ROOT_URL."admin/setting/edit/".$this->_iId);
    }

    /***** Front end *****/
    // Homepage
    public function index()
    {
        if(null != $this->_iId){
            header('Location: ' . ROOT_URL . 'admin/setting/edit/'.$this->_iId);
            exit;
        }
        
        header('Location: ' . ROOT_URL . 'admin/setting/add');
    }

    public function add()
    {
        $this->auth->hasRoleAdmin();
        if(count($this->oModel->getAll()) > 0)
            header('Location: ' . ROOT_URL."admin/setting/edit");
                    exit;

        if (!empty($_POST['setting_submit']))
        {
            $safeData = [];
            foreach ($_POST as $name => $value) {
                $safeData[$name] = $this->test_input($value);
            }

            if (isset($safeData['name']))
            {

                $aData = array('name' => $safeData['name'], 'slogan' => $safeData['slogan'], 'description' => $safeData['description'], 'address' => $safeData['address'], 'phone' => $safeData['phone'], 'email' => $safeData['email'], 'facebook' => $safeData['facebook'], 'twitter' => $safeData['twitter'], 'instagram' => $safeData['instagram'], 'linkedin' => $safeData['linkedin'], 'google_plus' => $safeData['google_plus']);
                
                $returnedValues = $this->oModel->add($data);

                if ($returnedValues['status']){
                    $this->oUtil->sSuccMsg = 'Les paramètres ont bien été enregistrés.';
                    header('Location: ' . ROOT_URL."admin/setting/edit/".$returnedValues['parent_id']);
                    exit;
                }

                else
                    $this->oUtil->sErrMsg = 'oops! Il y a une erreur! Veuillez reéssayer.';
            }
            else
            {
                $this->oUtil->sErrMsg = 'Veuillez remplir tous les champs.';
            }
        }
        
        $this->oUtil->oHead = 'Parametres';
        $this->oUtil->oLinkText = 'setting';
        $this->oUtil->oSetting = '';
        $this->oUtil->getView('setting/edit_setting');
    }

    public function edit()
    {
        $this->auth->hasRoleAdmin();

        if (!empty($_POST['setting_submit']))
        {
            $safeData = [];
            foreach ($_POST as $name => $value) {
                $safeData[$name] = $this->test_input($value);
            }

            if (isset($safeData['name']))
            {
                $aData = array('setting_id' => $safeData['setting_id'], 'name' => $safeData['name'], 'slogan' => $safeData['slogan'], 'description' => $safeData['description'], 'address' => $safeData['address'], 'phone' => $safeData['phone'], 'email' => $safeData['email'], 'facebook' => $safeData['facebook'], 'twitter' => $safeData['twitter'], 'instagram' => $safeData['instagram'], 'linkedin' => $safeData['linkedin'], 'google_plus' => $safeData['google_plus']);

                if ($this->oModel->update($aData))
                    $this->oUtil->sSuccMsg = 'Bravo, les paramètres ont bien été mise à jour.';
                else
                    $this->oUtil->sErrMsg = 'oops! il y a une erreur! Veuillez reéssayer';
            }
            else
            {
                $this->oUtil->sErrMsg = 'Tous les champs sont requis.';
            }
        }

        $this->oUtil->oHead = 'Paramètres';
        $this->oUtil->oLinkText = 'setting';
        /* Get the data of Setting */
        $this->oUtil->oSetting = $this->oModel->getById();

        $this->oUtil->getView('setting/edit_setting');
    }

}
