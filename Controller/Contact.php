<?php

namespace Bidi\Controller;

class Contact extends Base
{
    const MAX_PUB = 10;

    public function __construct()
    {
        parent::__construct(
            'Contact', 
            isset($_GET['id']) ? $_GET['id'] : null, 
            ROOT_URL."admin/contact/all");
    }

    /***** Back end *****/
    // Homepage
    public function index()
    {
        $this->auth->hasRoleAdmin();

        $this->oUtil->oMessages = $this->oModel->get(0, self::MAX_PUB); // Get only the latest X posts

        $this->oUtil->getView('contact/index');
    }

    /***** For Admin (Back end) *****/
    public function all()
    {
        $this->auth->hasRoleAdmin();

        $this->oUtil->oMessages = $this->oModel->getAll();

        $this->oUtil->getView('contact/index');
    }

    public function add()
    {

        $this->auth->hasRoleAdmin();
        
        if (!empty($_POST['add_submit']))
        {
            $safeData = [];
            foreach ($_POST as $name => $value) {
                $safeData[$name] = $this->test_input($value);
            }

            if (isset($safeData['title']))
            {
                if(!empty($_FILES['image'])){
                    $image_id = $this->singleUpload($_FILES['image'], 'pub_id', $this->_iId);
                }

                $safeData['slug'] = static::sluggify($safeData['title']);
                $safeData['created_at'] = date("Y-m-d H:i:s");
                $safeData['updated_at'] = date("Y-m-d H:i:s");
                $safeData['published_at'] = null;
                $safeData['image_id'] = $image_id;
                $safeData['created_by_id'] = $_SESSION['auth']->id;

                if($safeData['status'] == 1)
                    $safeData['published_at'] = date("Y-m-d H:i:s");

                $aData = array('title' => $safeData['title'], 'slug' => $safeData['slug'], 'content' => $safeData['content'], 'url' => $safeData['url'], 'pub_category_id' => $safeData['pub_category_id'], 'image_id' => $safeData['image_id'], 'created_at' => $safeData['created_at'], 'created_by_id' => $safeData['created_by_id'], 'updated_at' => $safeData['updated_at'], 'published_at' => $safeData['published_at'], 'end_date' => $safeData['end_date'], 'status' => $safeData['status']);
                
               $returnedValues = $this->oModel->add($aData);

                if ($returnedValues['status']) {
                    $this->oUtil->sSuccMsg = 'La publicité a bien été ajoutée.';
                    header('Location: ' . ROOT_URL."admin/pub/edit/".$returnedValues['parent_id']);
                    exit;
                }

                else
                    $this->oUtil->sErrMsg = 'oops! Il y a une erreur! Veuillez reéssayer.';
            }
            else
            {
                $this->oUtil->sErrMsg = 'Veuillez remplir tous les champs.';
            }
        }

        $this->oUtil->categoryList = (new PubCategory())->getAll();

        $this->oUtil->getView('pub/new_pub');
    }

    public function edit()
    {
    
        $this->auth->hasRoleAdmin();

        if (!empty($_POST['edit_submit']))
        {
            $pub = $this->oModel->getById($this->_iId);
            
            $safeData = [];
            foreach ($_POST as $name => $value) {
                $safeData[$name] = $this->test_input($value);
            }

            if (isset($safeData['title'], $safeData['content'], $safeData['end_date']))
            {
                $oldFileId = isset($pub->image_id) ? $pub->image_id : null;

                if(!empty($_FILES['image'])){
                    $image_id = $this->singleUpload($_FILES['image'], 'pub_id', $this->_iId, $oldFileId);
                }

                $safeData['slug'] = static::sluggify($safeData['title']);
                $safeData['updated_at'] = date("Y-m-d H:i:s");
                $safeData['published_at'] = null;
                $safeData['image_id'] = $image_id;
                if ($safeData['status'] == 1 && $pub->published_at != null)
                    $safeData['published_at'] = date("Y-m-d H:i:s");

                $aData = array('pub_id' => $this->_iId, 'title' => $safeData['title'], 'slug' => $safeData['slug'], 'content' => $safeData['content'], 'url' => $safeData['url'], 'pub_category_id' => $safeData['pub_category_id'], 'image_id' => $safeData['image_id'], 'end_date' => $safeData['end_date'], 'updated_at' => $safeData['updated_at'], 'published_at' => $safeData['published_at'], 'status' => $safeData['status']);

                if ($this->oModel->update($aData))
                    $this->oUtil->sSuccMsg = 'Bravo, la publicité a bien été mise à jour.';
                else
                    $this->oUtil->sErrMsg = 'oops! il y a une erreur! Veuillez reéssayer';
            }
            else
            {
                $this->oUtil->sErrMsg = 'Tous les champs sont requis.';
            }
        }

        $this->oUtil->categoryList = (new PubCategory())->getAll();
        /* Get the data of the post */
        $this->oUtil->oPub = $this->oModel->getById($this->_iId);

        $this->oUtil->getView('Pub/edit_pub');
    }

}
