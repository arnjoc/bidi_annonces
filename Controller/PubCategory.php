<?php

namespace Bidi\Controller;

class PubCategory extends Base
{
    const MAX_CATEGORY = 5;

    public function __construct()
    {
        parent::__construct(
            'PubCategory', 
            isset($_GET['id']) ? $_GET['id'] : null, 
            ROOT_URL."admin/pubcategory/all");
    }

    /***** Front end *****/
    // Homepage
    public function index()
    {
        $this->oUtil->oCategory = $this->oModel->get(0, self::MAX_CATEGORY); // Get only the latest X posts

        $this->oUtil->getView('category/index');
    }

    /***** For Admin (Back end) *****/
    public function all()
    {
        $this->auth->hasRoleAdmin();

        $this->oUtil->oHead = 'Publicités';
        $this->oUtil->oLinkText = 'pubcategory';
        $this->oUtil->oCategories = $this->oModel->getAll();

        $this->oUtil->getView('category/index');
    }

    public function add()
    {
        $this->auth->hasRoleAdmin();
        
        if (!empty($_POST['add_submit']))
        {
            $safeData = [];
            foreach ($_POST as $name => $value) {
                $safeData[$name] = $this->test_input($value);
            }

            if (isset($safeData['name']))
            {

                $safeData['slug'] = static::sluggify($safeData['name']);
                $safeData['created_at'] = date("Y-m-d H:i:s");

                $aData = array('name' => $safeData['name'], 'slug' => $safeData['slug'], 'description' => $safeData['description'], 'created_at' => $safeData['created_at']);
                
                if ($this->oModel->add($aData))
                    $this->oUtil->sSuccMsg = 'La categorie a bien été ajoutée.';
                else
                    $this->oUtil->sErrMsg = 'oops! Il y a une erreur! Veuillez reéssayer.';
            }
            else
            {
                $this->oUtil->sErrMsg = 'Veuillez remplir tous les champs.';
            }
        }
        
        $this->oUtil->oHead = 'Publicités';
        $this->oUtil->oLinkText = 'pubcategory';
        $this->oUtil->getView('category/new_Category');
    }

    public function edit()
    {
        $this->auth->hasRoleAdmin();

        if (!empty($_POST['edit_submit']))
        {
            $safeData = [];
            foreach ($_POST as $name => $value) {
                $safeData[$name] = $this->test_input($value);
            }

            if (isset($safeData['name']))
            {
                $safeData['slug'] = static::sluggify($safeData['name']);

                $aData = array('category_id' => $this->_iId, 'name' => $safeData['name'], 'slug' => $safeData['slug'], 'description' => $safeData['description']);


                if ($this->oModel->update($aData))
                    $this->oUtil->sSuccMsg = 'Bravo, la categorie a bien mise à jour.';
                else
                    $this->oUtil->sErrMsg = 'oops! il y a une erreur! Veuillez reéssayer';
            }
            else
            {
                $this->oUtil->sErrMsg = 'Tous les champs sont requis.';
            }
        }

        $this->oUtil->oHead = 'Publicités';
        $this->oUtil->oLinkText = 'pubcategory';
        /* Get the data of the post */
        $this->oUtil->oCategory = $this->oModel->getById($this->_iId);

        $this->oUtil->getView('category/edit_Category');
    }

}
