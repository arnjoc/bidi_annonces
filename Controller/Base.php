<?php

namespace Bidi\Controller;

use Bidi\Controller\Security;

class Base
{

    protected $oUtil, $oModel;
    protected $_iId;
    protected $indexRoot;
    protected $auth;
    protected $oDb;

    public function __construct($model, $_id, $indexRoot)
    {
        // Enable PHP Session
        if (empty($_SESSION))
            @session_start();

        $this->oUtil = new \Bidi\Engine\Util;

        /** Get the Model class in all the controller class **/
        $this->oUtil->getModel($model);
        $modelPath = '\Bidi\Model\\'.$model;
        $this->oModel = new $modelPath;
        $this->indexRoot = $indexRoot;
        $this->auth = new \Bidi\Controller\Security;
        $this->oDb = new \Bidi\Engine\Db;

        /** Get the Category ID in the constructor in order to avoid the duplication of the same code **/
        $this->_iId = (int) (!empty($_id) ? $_id : 0);
    }

    public function notFound()
    {
        $this->oUtil->getView('not_found');
    }

    public function isUniq($value)
    {
        return !($this->oModel->isUniq($value));
    }

    public function delete()
    {
        $this->auth->hasRoleAdmin();

        if (!empty($_POST['delete']) && $this->oModel->delete($this->_iId))
            header('Location: ' . $this->indexRoot);
        else
            exit('oops! Erreur au cours de la suppression.');
    }

    protected function isLogged()
    {
        return !empty($_SESSION['is_logged']);
    }

    protected function singleUpload($uploadedFile,$relationName, $parent_id, $oldFileId=null)
    {
        //echo "<pre>"; var_dump($uploadedFile,$relationName, $parent_id); echo "</pre>"; die();
        //$subdir = (explode('_', strtolower($relationName)))[0];
        $parent_id = $this->oDb->quote($parent_id);
        $files = $uploadedFile;
        $images = array();
        $uploadDir = SITE_ROOT . '\static\images\\'. (explode('_', strtolower($relationName)))[0] .'\\';

        $image = array(
            'name' => $files['name'],
            'tmp_name' => $files['tmp_name']
        );
    
        $extensions = pathinfo($image['name'], PATHINFO_EXTENSION);
        if(in_array($extensions, array('jpg', 'jpeg', 'png'))){
            //$this->oDb->query("INSERT INTO image SET $relationName=$parent_id");
            //$image_id = $this->oDb->lastInsertId();
            $image_name = uniqid() . '.'. $extensions;

            if(move_uploaded_file($image['tmp_name'], $uploadDir . $image_name)){
                $this->resizedImage($uploadDir . $image_name, 150,150);
                $image_name = $this->oDb->quote($image_name);
                //$this->oDb->query("UPDATE image SET url=$image_name WHERE id=$image_id");
                $this->oDb->query("INSERT INTO image SET url=$image_name");
                $image_id = $this->oDb->lastInsertId();
            }

            if(null != $oldFileId)
                $this->deleteImage($oldFileId, $image_id);

            return $image_id;

        } else {
            $this->oUtil->sErrMsg = "Seul les extensions: [jpg, jpeg, png] sont supportées";
        }
    }

    protected function multiUpload($uploadedFiles,$relationName, $parent_id, $oldFileId=null)
    {
        $parent_id = $this->oDb->quote($parent_id);
        $files = $uploadedFiles;
        $images = array();
        $uploadDir = SITE_ROOT . '\static\images\\'. (explode('_', strtolower($relationName)))[0] .'\\';
        foreach ($files['tmp_name'] as $k => $v) {
            $image = array(
                'name' => $files['name'][$k],
                'tmp_name' => $files['tmp_name'][$k]
            );
        
            $extensions = pathinfo($image['name'], PATHINFO_EXTENSION);
            if(in_array($extensions, array('jpg', 'jpeg', 'png'))){
                $image_name = uniqid() . '.'. $extensions;
                if(move_uploaded_file($image['tmp_name'], $uploadDir . $image_name)){
                    $this->resizedImage($uploadDir . $image_name, 150,150);
                    $image_name = $this->oDb->quote($image_name);
                    $this->oDb->query("INSERT INTO images SET url=$image_name, advert_id=$parent_id");
                }
            }
        }
    }

    protected function deleteImage($parent_id, $image_id=null)
    {
        $oStmt = $this->oDb->prepare('DELETE FROM image WHERE id = :imageId LIMIT 1');
        $oStmt->bindParam(':imageId', $oldFileId, \PDO::PARAM_INT);
        return $oStmt->execute();
    }

    protected function test_input($data) {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
    }

    public static function sluggify($str, $delimiter = '-'){

        $unwanted_array = ['ś'=>'s', 'ą' => 'a', 'ć' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ź' => 'z', 'ż' => 'z',
            'Ś'=>'s', 'Ą' => 'a', 'Ć' => 'c', 'Ę' => 'e', 'Ł' => 'l', '&amp;' => 'et', 'Ń' => 'n', 'Ó' => 'o', 'Ź' => 'z', 'Ż' => 'z']; // Polish letters for example
        $str = strtr( $str, $unwanted_array );

        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'et', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
        return $slug;
    }


    public function resizedImage($file, $width, $height){
    
        # We find the right file
        $pathinfo   = pathinfo(trim($file, '/'));
        $output     = $pathinfo['dirname'] . '/' . $pathinfo['filename'] . '_' . $width . 'x' . $height . '.' . $pathinfo['extension'];
    
        # Setting defaults and meta
        $info                         = getimagesize($file);
        list($width_old, $height_old) = $info;
        # Create image ressource
        switch ( $info[2] ) {
            case IMAGETYPE_GIF:   $image = imagecreatefromgif($file);   break;
            case IMAGETYPE_JPEG:  $image = imagecreatefromjpeg($file);  break;
            case IMAGETYPE_PNG:   $image = imagecreatefrompng($file);   break;
            default: return false;
        }
        # We find the right ratio to resize the image before cropping
        $heightRatio = $height_old / $height;
        $widthRatio  = $width_old /  $width;
        $optimalRatio = $widthRatio;
        if ($heightRatio < $widthRatio) {
            $optimalRatio = $heightRatio;
        }
        $height_crop = ($height_old / $optimalRatio);
        $width_crop  = ($width_old  / $optimalRatio);
        # The two image ressources needed (image resized with the good aspect ratio, and the one with the exact good dimensions)
        $image_crop = imagecreatetruecolor( $width_crop, $height_crop );
        $image_resized = imagecreatetruecolor($width, $height);
        # This is the resizing/resampling/transparency-preserving magic
        if ( ($info[2] == IMAGETYPE_GIF) || ($info[2] == IMAGETYPE_PNG) ) {
            $transparency = imagecolortransparent($image);
            if ($transparency >= 0) {
                $transparent_color  = imagecolorsforindex($image, $trnprt_indx);
                $transparency       = imagecolorallocate($image_crop, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue']);
                imagefill($image_crop, 0, 0, $transparency);
                imagecolortransparent($image_crop, $transparency);
                imagefill($image_resized, 0, 0, $transparency);
                imagecolortransparent($image_resized, $transparency);
            }elseif ($info[2] == IMAGETYPE_PNG) {
                imagealphablending($image_crop, false);
                imagealphablending($image_resized, false);
                $color = imagecolorallocatealpha($image_crop, 0, 0, 0, 127);
                imagefill($image_crop, 0, 0, $color);
                imagesavealpha($image_crop, true);
                imagefill($image_resized, 0, 0, $color);
                imagesavealpha($image_resized, true);
            }
        }
        imagecopyresampled($image_crop, $image, 0, 0, 0, 0, $width_crop, $height_crop, $width_old, $height_old);
        imagecopyresampled($image_resized, $image_crop, 0, 0, ($width_crop - $width) / 2, ($height_crop - $height) / 2, $width, $height, $width, $height);
        # Writing image according to type to the output destination and image quality
        switch ( $info[2] ) {
          case IMAGETYPE_GIF:   imagegif($image_resized, $output, 80);    break;
          case IMAGETYPE_JPEG:  imagejpeg($image_resized, $output, 80);   break;
          case IMAGETYPE_PNG:   imagepng($image_resized, $output, 9);    break;
          default: return false;
        }
        return true;
    }

    public function dump($elm) 
    {
        echo "<pre>"; var_dump($elm); echo "</pre>"; die();
    }
}
