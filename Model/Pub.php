<?php

namespace Bidi\Model;

class Pub
{
    protected $oDb;

    public function __construct()
    {
        $this->oDb = new \Bidi\Engine\Db;
    }

    public function get($iOffset, $iLimit)
    {
        $oStmt = $this->oDb->prepare('SELECT * FROM pubs ORDER BY created_at DESC LIMIT :offset, :limit');
        $oStmt->bindParam(':offset', $iOffset, \PDO::PARAM_INT);
        $oStmt->bindParam(':limit', $iLimit, \PDO::PARAM_INT);
        $oStmt->execute();
        return $oStmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function getAll()
    {
        $oStmt = $this->oDb->query('SELECT * FROM pubs ORDER BY created_at DESC');
        return $oStmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function randomActivePub()
    {
        $oStmt = $this->oDb->query('SELECT p.title, p.content, p.url, c.name category_name, i.url image_url FROM pubs p LEFT JOIN pub_category c ON p.pub_category_id=c.id LEFT JOIN image i ON p.image_id=i.id ORDER BY RAND() LIMIT 1');
        $oStmt->execute();
        return $oStmt->fetch(\PDO::FETCH_OBJ);
    }

    public function getWithCat()
    {
        $oStmt = $this->oDb->query("SELECT p.id, p.title, p.created_at, p.url, p.end_date, c.name category_name FROM pubs p LEFT JOIN pub_category c ON p.pub_category_id=c.id ORDER BY created_at DESC");
        //echo'<pre>';print_r($oStmt->fetchAll(\PDO::FETCH_OBJ)); echo'</pre>';die();
        return $oStmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function add(array $aData)
    {
        $oStmt = $this->oDb->prepare('INSERT INTO pubs (title, slug, content, url, end_date, pub_category_id, image_id, created_at, created_by_id, updated_at, published_at, status) VALUES(:title, :slug, :content, :url, :end_date, :pub_category_id, :image_id, :created_at, :created_by_id, :updated_at, :published_at, :status)');
        $status = $oStmt->execute($aData);
        $parent_id = $this->oDb->lastInsertId();
        
        return compact('status', 'parent_id');
    }
    
    public function getById($iId)
    {
        $oStmt = $this->oDb->prepare('SELECT p.id, p.title, p.content, p.pub_category_id, p.url, p.end_date, p.status, i.url image_url FROM pubs p LEFT JOIN image i ON p.image_id=i.id WHERE p.id = :pubId LIMIT 1');
        $oStmt->bindParam(':pubId', $iId, \PDO::PARAM_INT);
        $oStmt->execute();
        return $oStmt->fetch(\PDO::FETCH_OBJ);
    }

    public function update(array $aData)
    {
        $oStmt = $this->oDb->prepare('UPDATE pubs SET title = :title, slug = :slug, content = :content, url = :url, end_date = :end_date,  pub_category_id = :pub_category_id, image_id = :image_id, updated_at = :updated_at, published_at = :published_at, status = :status WHERE id = :pubId LIMIT 1');
        $oStmt->bindValue(':pubId', $aData['pub_id'], \PDO::PARAM_INT);
        $oStmt->bindValue(':title', $aData['title']);
        $oStmt->bindValue(':slug', $aData['slug']);
        $oStmt->bindValue(':content', $aData['content']);
        $oStmt->bindValue(':url', $aData['url']);
        $oStmt->bindValue(':pub_category_id', $aData['pub_category_id']);
        $oStmt->bindValue(':image_id', $aData['image_id']);
        $oStmt->bindValue(':end_date', $aData['end_date']);
        $oStmt->bindValue(':updated_at', $aData['updated_at']);
        $oStmt->bindValue(':published_at', $aData['published_at']);
        $oStmt->bindValue(':status', $aData['status']);
        return $oStmt->execute();
    }

    public function delete($iId)
    {
        $oStmt = $this->oDb->prepare('DELETE FROM pubs WHERE id = :pubsId LIMIT 1');
        $oStmt->bindParam(':pubsId', $iId, \PDO::PARAM_INT);
        return $oStmt->execute();
    }
}
