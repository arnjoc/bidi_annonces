<?php

namespace Bidi\Model;

class Setting
{
    protected $oDb;
    protected $setting_id;

    public function __construct()
    {
        $this->oDb = new \Bidi\Engine\Db;
    }

    public function getAll()
    {
        $oStmt = $this->oDb->query('SELECT * FROM settings ORDER BY id DESC');
        return $oStmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function add(array $aData)
    {
        //echo'<pre>';print_r($aData); echo'</pre>';die();
        $oStmt = $this->oDb->prepare('INSERT INTO settings (name, slogan, description, address, phone, email, facebook, twitter, instagram, linkedin, google_plus) VALUES(:name, :slogan, :description, :address, :phone, :email, :facebook, :twitter, :instagram, :linkedin, :google_plus)');
        $status = $oStmt->execute($aData);
        $parent_id = $this->oDb->lastInsertId();
        
        return compact('status', 'parent_id');
    }
    
    public function getById()
    {
        $oStmt = $this->oDb->prepare('SELECT * FROM settings LIMIT 1');
        $oStmt->execute();
        return $oStmt->fetch(\PDO::FETCH_OBJ);
    }

    public function update(array $aData)
    {
        $oStmt = $this->oDb->prepare('UPDATE settings SET name = :name,slogan = :slogan, description = :description, address = :address, phone = :phone, email = :email, facebook = :facebook, twitter = :twitter, instagram = :instagram, linkedin = :linkedin, google_plus = :google_plus WHERE id = :settingId LIMIT 1');
        $oStmt->bindValue(':settingId', $aData['setting_id'], \PDO::PARAM_INT);
        $oStmt->bindValue(':name', $aData['name']);
        $oStmt->bindValue(':slogan', $aData['slogan']);
        $oStmt->bindValue(':description', $aData['description']);
        $oStmt->bindValue(':address', $aData['address']);
        $oStmt->bindValue(':phone', $aData['phone']);
        $oStmt->bindValue(':email', $aData['email']);
        $oStmt->bindValue(':facebook', $aData['facebook']);
        $oStmt->bindValue(':twitter', $aData['twitter']);
        $oStmt->bindValue(':instagram', $aData['instagram']);
        $oStmt->bindValue(':linkedin', $aData['linkedin']);
        $oStmt->bindValue(':google_plus', $aData['google_plus']);
        return $oStmt->execute();
    }
}
