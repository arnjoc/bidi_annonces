<?php

namespace Bidi\Model;

class PubCategory
{
    protected $oDb;

    public function __construct()
    {
        $this->oDb = new \Bidi\Engine\Db;
    }

    public function get($iOffset, $iLimit)
    {
        $oStmt = $this->oDb->prepare('SELECT * FROM pub_category ORDER BY created_at DESC LIMIT :offset, :limit');
        $oStmt->bindParam(':offset', $iOffset, \PDO::PARAM_INT);
        $oStmt->bindParam(':limit', $iLimit, \PDO::PARAM_INT);
        $oStmt->execute();
        return $oStmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function getAll()
    {
        $oStmt = $this->oDb->query('SELECT * FROM pub_category ORDER BY created_at DESC');
        return $oStmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function add(array $aData)
    {
        $oStmt = $this->oDb->prepare('INSERT INTO pub_category (name, slug, description, created_at) VALUES(:name, :slug, :description, :created_at)');
        return $oStmt->execute($aData);
    }
    
    public function getById($iId)
    {
        $oStmt = $this->oDb->prepare('SELECT * FROM pub_category WHERE id = :categoryId LIMIT 1');
        $oStmt->bindParam(':categoryId', $iId, \PDO::PARAM_INT);
        $oStmt->execute();
        return $oStmt->fetch(\PDO::FETCH_OBJ);
    }

    public function update(array $aData)
    {
        $oStmt = $this->oDb->prepare('UPDATE pub_category SET name = :name, slug = :slug, description = :description WHERE id = :categoryId LIMIT 1');
        $oStmt->bindValue(':categoryId', $aData['category_id'], \PDO::PARAM_INT);
        $oStmt->bindValue(':name', $aData['name']);
        $oStmt->bindValue(':slug', $aData['slug']);
        $oStmt->bindValue(':description', $aData['description']);
        return $oStmt->execute();
    }

    public function delete($iId)
    {
        $oStmt = $this->oDb->prepare('DELETE FROM pub_category WHERE id = :categoryId LIMIT 1');
        $oStmt->bindParam(':categoryId', $iId, \PDO::PARAM_INT);
        return $oStmt->execute();
    }
}
