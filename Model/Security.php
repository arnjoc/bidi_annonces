<?php

namespace Bidi\Model;

class Security extends User
{
    public function login($sEmail)
    {
        $oStmt = $this->oDb->prepare('SELECT u.id, u.username, u.role, u.password, u.email, u.name, u.phone, u.country, u.town, i.url image_url FROM users u LEFT JOIN image i ON u.image_id=i.id WHERE u.email = :email LIMIT 1');
        $oStmt->bindValue(':email', $sEmail, \PDO::PARAM_STR);
        $oStmt->execute();
        $oRow = $oStmt->fetch(\PDO::FETCH_OBJ);
 		
        return @$oRow; // Use the PHP 5.5 password function
    }
}
