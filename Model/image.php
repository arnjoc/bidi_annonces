<?php
 
namespace Bidi\Model;
 
class Image
{

    protected $oDb;

    private $extension;

    private $alt;
     
    private $file;
     
    private $tempFilename;


    public function __construct()
    {
        $this->oDb = new \Bidi\Engine\Db;
    }

    public function add(array $aData)
    {
        $oStmt = $this->oDb->prepare('INSERT INTO advert (title, slug, summary, content, category_id, type, status, author_id, created_at, updated_at, published_at) VALUES(:title, :slug, :summary, :content, :category_id, :type, :status, :author_id, :created_at, :updated_at, :published_at)');
        return $oStmt->execute($aData);
    }

    public function deleteAdvertImage($advertId)
    {
        $oStmt = $this->oDb->prepare('DELETE FROM Images WHERE id = :imageId LIMIT 1');
        $oStmt->bindParam(':imageId', $advertId, \PDO::PARAM_INT);
        return $oStmt->execute();
    }

    /**
     * Set extension
     *
     * @param string $extension
     *
     * @return Image
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;

        return $this;
    }

    /**
     * Get extension
     *
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * Set alt
     *
     * @param string $alt
     *
     * @return Image
     */
    public function setAlt($alt)
    {
        $this->alt = $alt;

        return $this;
    }

    /**
     * Get alt
     *
     * @return string
     */
    public function getAlt()
    {
        return $this->alt;
    }


    public function setFile(File $file)
    {
        $this->file = $file;
        if (null !== $this->extension) {
            $this->tempFilename = $this->extension;
 
            $this->extension = null;
            $this->alt = null;
        }
    }
 
    public function getFile()
    {
        return $this->file;
    }
     
     
    /**
    * @ORM\PrePersist()
    * @ORM\PreUpdate()
    */
    public function preUpload()
    {
        //dump($this->portfolio->getId()); die('prepersist image');
        if (null === $this->file) {
            return;
        }
        $this->extension = uniqid().'.'.$this->file->guessExtension();
 
        $this->alt = $this->file->getClientOriginalName();
     
    }
 
    /**
    * @ORM\PostPersist()
    * @ORM\PostUpdate()
    */
    public function upload()
    { 
        if (null === $this->file) {
            return;
        }

        if (null !== $this->tempFilename) {
            $oldFile = $this->getUploadRootDir().'/'.$this->id.'.'.$this->tempFilename;
            if (file_exists($oldFile)) {
                unlink($oldFile);
            }
        }
        //dump($this->getUploadRootDir().'.'.$this->id.'.'.$this->extension); die();
        $this->file->move($this->getUploadRootDir(),$this->getExtension());
        $this->file = null;
    }
 
    /**
     * @ORM\PreRemove()
     */
    public function preRemoveUpload()
    {
        $this->tempFilename = $this->getUploadRootDir().'/'.$this->id.'.'.$this->extension;
    }
 
    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if (file_exists($this->tempFilename)) {
            unlink($this->tempFilename);
        }
    }
 
    public function getUploadDir()
    {
        return 'upload/Photo';
    }
 
    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../web/'.$this->getUploadDir();
    }
     
    public function getWebPath()
    {
        return $this->getUploadDir().'/'.$this->getExtension();
    }

    public function resizeName($file, $width, $height){
        $info = pathinfo($file);
        $return = '';
        if($info['dirname'] != '.'){
            $return .= $info['dirname'] . '/';
         }
            $return .= $info['filename'] . "_$width" . "x$height." . $info['extension'];
            return $return;

    }
}
