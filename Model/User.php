<?php

namespace Bidi\Model;

class User
{
    protected $oDb;

    public function __construct()
    {
        $this->oDb = new \Bidi\Engine\Db;
    }

    public function get($iOffset, $iLimit)
    {
        $oStmt = $this->oDb->prepare('SELECT * FROM users ORDER BY created_at DESC LIMIT :offset, :limit');
        $oStmt->bindParam(':offset', $iOffset, \PDO::PARAM_INT);
        $oStmt->bindParam(':limit', $iLimit, \PDO::PARAM_INT);
        $oStmt->execute();
        return $oStmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function getAll()
    {
        $oStmt = $this->oDb->query('SELECT * FROM users ORDER BY created_at DESC');
        return $oStmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function isMailExist($email)
    {
        $oStmt = $this->oDb->prepare('SELECT email FROM users WHERE email = :email LIMIT 1');
        $oStmt->bindValue(':email', $email, \PDO::PARAM_STR);
        $oStmt->execute();
        $oRow = $oStmt->fetch(\PDO::FETCH_OBJ);
        
        return @$oRow;
    }

    public function add(array $aData)
    {
        $oStmt = $this->oDb->prepare('INSERT INTO users (name, username, email, phone, password, country, town, enabled, role, confirm_token, created_at) VALUES(:name, :username, :email, :phone, :password, :country, :town, :enabled, :role, :confirm_token, :created_at)');
        return $oStmt->execute($aData);
    }
    
    public function getById($iId)
    {
        $oStmt = $this->oDb->prepare('SELECT u.id, u.username, u.email, u.name, u.phone, u.country, u.town, i.url image_url FROM users u LEFT JOIN image i ON u.image_id=i.id WHERE u.id = :userId LIMIT 1');
        $oStmt->bindParam(':userId', $iId, \PDO::PARAM_INT);
        $oStmt->execute();
        return $oStmt->fetch(\PDO::FETCH_OBJ);
    }

    public function update(array $aData)
    {
        $oStmt = $this->oDb->prepare('UPDATE users SET name = :name, username = :username, email  = :email, phone = :phone, password = :password, country = :country, town = :town, enabled = :enabled, role = :role, confirm_token = :confirm_token WHERE id = :userId LIMIT 1');
        $oStmt->bindValue(':userId', $aData['user_id'], \PDO::PARAM_INT);
        $oStmt->bindValue(':name', $aData['name']);
        $oStmt->bindValue(':username', $aData['username']);
        $oStmt->bindValue(':email', $aData['email']);
        $oStmt->bindValue(':phone', $aData['phone']);
        $oStmt->bindValue(':password', $aData['password']);
        $oStmt->bindValue(':country', $aData['country']);
        $oStmt->bindValue(':town', $aData['town']);
        $oStmt->bindValue(':enabled', $aData['enabled']);
        $oStmt->bindValue(':role', $aData['role']);
        $oStmt->bindValue(':confirm_token', $aData['confirm_token']);
        return $oStmt->execute();
    }

    public function profileUpdate(array $aData)
    {
        $oStmt = $this->oDb->prepare('UPDATE users SET name = :name, phone = :phone, country = :country, town = :town WHERE id = :userId LIMIT 1');
        $oStmt->bindValue(':userId', $aData['user_id'], \PDO::PARAM_INT);
        $oStmt->bindValue(':name', $aData['name']);
        $oStmt->bindValue(':phone', $aData['phone']);
        $oStmt->bindValue(':country', $aData['country']);
        $oStmt->bindValue(':town', $aData['town']);
        return $oStmt->execute();
    }

    public function profilePassUpdate(array $aData)
    {
        $oStmt = $this->oDb->prepare('UPDATE users SET password = :password WHERE id = :userId LIMIT 1');
        $oStmt->bindValue(':userId', $aData['user_id'], \PDO::PARAM_INT);
        $oStmt->bindValue(':password', $aData['new_password']);
        return $oStmt->execute();
    }

    public function setImage($userId, $imageId)
    {
        $oStmt = $this->oDb->prepare('UPDATE users SET image_id = :image_id WHERE id = :userId LIMIT 1');
        $oStmt->bindValue(':userId', $userId, \PDO::PARAM_INT);
        $oStmt->bindValue(':image_id', $imageId, \PDO::PARAM_INT);
        return $oStmt->execute();
    }

    public function delete($iId)
    {
        $oStmt = $this->oDb->prepare('DELETE FROM users WHERE id = :userId LIMIT 1');
        $oStmt->bindParam(':userId', $iId, \PDO::PARAM_INT);
        return $oStmt->execute();
    }
}
