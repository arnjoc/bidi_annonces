<?php

namespace Bidi\Model;

class Advert
{
    protected $oDb;

    public function __construct()
    {
        $this->oDb = new \Bidi\Engine\Db;
    }

    public function get($iOffset, $iLimit)
    {
        $oStmt = $this->oDb->prepare('SELECT * FROM advert ORDER BY created_at DESC LIMIT :offset, :limit');
        $oStmt->bindParam(':offset', $iOffset, \PDO::PARAM_INT);
        $oStmt->bindParam(':limit', $iLimit, \PDO::PARAM_INT);
        $oStmt->execute();
        return $oStmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function getOneById($iId)
    {
        $oStmt = $this->oDb->prepare('SELECT * FROM advert WHERE id = :advertId LIMIT 1');
        $oStmt->bindParam(':advertId', $iId, \PDO::PARAM_INT);
        $oStmt->execute();
        return $oStmt->fetch(\PDO::FETCH_OBJ);
    }

    public function getAdvertImages($iId)
    {
        //var_dump($iId); die();
        $oStmt = $this->oDb->prepare('SELECT * FROM images WHERE advert_id = :advertId');
        $oStmt->bindParam(':advertId', $iId, \PDO::PARAM_INT);
        $oStmt->execute();
        return $oStmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function getUserAdverts($userId, $iOffset, $iLimit)
    {
        $oStmt = $this->oDb->prepare("SELECT a.id, a.title,a.slug, a.status, a.created_at, u.username author, c.name category_name FROM advert a LEFT JOIN category c ON a.category_id=c.id LEFT JOIN users u ON a.author_id=u.id WHERE author_id = :userId ORDER BY created_at DESC LIMIT :offset, :limit");

        $oStmt->bindParam(':userId', $userId);
        $oStmt->bindParam(':offset', $iOffset, \PDO::PARAM_INT);
        $oStmt->bindParam(':limit', $iLimit, \PDO::PARAM_INT);
        $oStmt->execute();
        return $oStmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function search($a=null, $b=null)
    {
        $b = (int) $b;

        if(null != $a) { 
            $oStmt = $this->oDb->query("SELECT a.id, a.title, a.slug, a.created_at, a.price, a.location, a.currency, c.name category_name, i.url image_url FROM advert a LEFT JOIN category c ON a.category_id=c.id LEFT JOIN image i ON a.image_id=i.id WHERE a.status = 1 AND a.title LIKE '%$a%' ORDER BY a.created_at");
            return $oStmt->fetchAll(\PDO::FETCH_OBJ);
        }

        $oStmt = $this->oDb->query("SELECT a.id, a.title, a.slug, a.created_at, a.price, a.location, a.currency, c.name category_name, i.url image_url FROM advert a LEFT JOIN category c ON a.category_id=c.id LEFT JOIN image i ON a.image_id=i.id WHERE a.status = 1 AND a.category_id LIKE '%$b%' ORDER BY a.created_at");
        return $oStmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function getAll()
    {
        $oStmt = $this->oDb->query('SELECT * FROM advert ORDER BY created_at DESC');
        return $oStmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function loadActive()
    {
        $oStmt = $this->oDb->query('SELECT a.id, a.title, a.slug, a.price, a.currency, a.location, c.name category_name, i.url image_url FROM advert a LEFT JOIN category c ON a.category_id=c.id LEFT JOIN image i ON a.image_id=i.id WHERE a.status = 1 ORDER BY a.created_at DESC LIMIT 8');
        return $oStmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function getAllByCategory($slug)
    {
        $oStmt = $this->oDb->query("SELECT a.id, a.title, a.slug, a.content, a.created_at, a.price, a.location, a.currency, c.name category_name, i.url image_url FROM advert a LEFT JOIN category c ON a.category_id=c.id LEFT JOIN image i ON a.image_id=i.id WHERE a.status = 1 AND c.slug = '$slug' ORDER BY a.created_at DESC LIMIT 4");
        //$oStmt->bindParam(':categoryId', $iId, \PDO::PARAM_INT);

        return $oStmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function getWithCat()
    {
        $oStmt = $this->oDb->query("SELECT a.id, a.title, a.currency, a.price, a.created_at, u.username author, c.name category_name FROM advert a LEFT JOIN category c ON a.category_id=c.id LEFT JOIN users u ON a.author_id=u.id ORDER BY created_at DESC");
        //echo'<pre>';print_r($oStmt->fetchAll(\PDO::FETCH_OBJ)); echo'</pre>';die();
        return $oStmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function add(array $aData)
    {
        //echo'<pre>';print_r($aData); echo'</pre>';die();
        $oStmt = $this->oDb->prepare('INSERT INTO advert (title, slug, price, currency, content, location, category_id, image_id, type, status, author_id, created_at, updated_at, published_at) VALUES(:title, :slug, :price, :currency, :content, :location, :category_id, :image_id, :type, :status, :author_id, :created_at, :updated_at, :published_at)');
        $status = $oStmt->execute($aData);
        $parent_id = $this->oDb->lastInsertId();
        
        return compact('status', 'parent_id');
    }
    
    public function getBySlug($slug)
    {
        $oStmt = $this->oDb->prepare('SELECT a.id, a.title, a.slug, a.price, a.currency, a.content, a.location, a.category_id, a.created_at, a.type, a.status, a.image_id, a.author_id, c.name category_name, c.slug category_slug, i.url image_url, u.username author_name, u.phone author_phone, u.email author_email FROM advert a LEFT JOIN image i ON a.image_id=i.id LEFT JOIN category c ON a.category_id=c.id LEFT JOIN users u ON a.author_id=u.id WHERE a.slug = :advertSlug LIMIT 1');
        $oStmt->bindParam(':advertSlug', $slug);
        $oStmt->execute();
        return $oStmt->fetch(\PDO::FETCH_OBJ);
    }

    public function getBy($iId)
    {
        $oStmt = $this->oDb->prepare('SELECT a.id, a.title, a.price, a.currency, a.content, a.location, a.category_id, a.created_at, a.type, a.status, a.image_id, c.name category_name, i.url image_url, u.username author_name FROM advert a LEFT JOIN image i ON a.image_id=i.id LEFT JOIN category c ON a.category_id=c.id LEFT JOIN users u ON a.author_id=u.id WHERE a.id = :advertId LIMIT 1');
        $oStmt->bindParam(':advertId', $iId, \PDO::PARAM_INT);
        $oStmt->execute();
        return $oStmt->fetch(\PDO::FETCH_OBJ);
    }

    public function update(array $aData)
    {
        $oStmt = $this->oDb->prepare('UPDATE advert SET title = :title, slug = :slug, price = :price, currency = :currency, content = :content, location = :location, category_id = :category_id, image_id = :image_id, type = :type, status = :status, author_id = :author_id, updated_at = :updated_at WHERE slug = :advertSlug LIMIT 1');
        $oStmt->bindValue(':advertSlug', $aData['advert_slug']);
        $oStmt->bindValue(':title', $aData['title']);
        $oStmt->bindValue(':slug', $aData['slug']);
        $oStmt->bindValue(':price', $aData['price']);
        $oStmt->bindValue(':currency', $aData['currency']);
        $oStmt->bindValue(':content', $aData['content']);
        $oStmt->bindValue(':location', $aData['location']);
        $oStmt->bindValue(':category_id', $aData['category_id']);
        $oStmt->bindValue(':image_id', $aData['image_id']);
        $oStmt->bindValue(':type', $aData['type']);
        $oStmt->bindValue(':status', $aData['status']);
        $oStmt->bindValue(':author_id', $aData['author_id']);
        $oStmt->bindValue(':updated_at', $aData['updated_at']);
        return $oStmt->execute();
    }

    public function updateById(array $aData)
    {
        $oStmt = $this->oDb->prepare('UPDATE advert SET title = :title, slug = :slug, price = :price, currency = :currency, content = :content, location = :location, category_id = :category_id, image_id = :image_id, type = :type, status = :status, author_id = :author_id, updated_at = :updated_at WHERE id = :advertId LIMIT 1');
        $oStmt->bindValue(':advertId', $aData['advert_id'], \PDO::PARAM_INT);
        $oStmt->bindValue(':title', $aData['title']);
        $oStmt->bindValue(':slug', $aData['slug']);
        $oStmt->bindValue(':price', $aData['price']);
        $oStmt->bindValue(':currency', $aData['currency']);
        $oStmt->bindValue(':content', $aData['content']);
        $oStmt->bindValue(':location', $aData['location']);
        $oStmt->bindValue(':category_id', $aData['category_id']);
        $oStmt->bindValue(':image_id', $aData['image_id']);
        $oStmt->bindValue(':type', $aData['type']);
        $oStmt->bindValue(':status', $aData['status']);
        $oStmt->bindValue(':author_id', $aData['author_id']);
        $oStmt->bindValue(':updated_at', $aData['updated_at']);
        return $oStmt->execute(); die();
    }

    public function delete($iId)
    {
        $oStmt = $this->oDb->prepare('DELETE FROM advert WHERE id = :advertId LIMIT 1');
        $oStmt->bindParam(':advertId', $iId, \PDO::PARAM_INT);
        return $oStmt->execute();
    }
}
