<?php

namespace Bidi\Model;

class Category
{
    protected $oDb;

    public function __construct()
    {
        $this->oDb = new \Bidi\Engine\Db;
    }

    public function get($iOffset, $iLimit)
    {
        $oStmt = $this->oDb->prepare('SELECT * FROM category ORDER BY created_at DESC LIMIT :offset, :limit');
        $oStmt->bindParam(':offset', $iOffset, \PDO::PARAM_INT);
        $oStmt->bindParam(':limit', $iLimit, \PDO::PARAM_INT);
        $oStmt->execute();
        return $oStmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function getAll()
    {
        $oStmt = $this->oDb->query('SELECT * FROM category ORDER BY created_at DESC');
        return $oStmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function add(array $aData)
    {
        $oStmt = $this->oDb->prepare('INSERT INTO category (name, slug, description, icon, image_id, created_at) VALUES(:name, :slug, :description, :icon, :image_id, :created_at)');
        return $oStmt->execute($aData);
    }
    
    public function getById($iId)
    {
        $oStmt = $this->oDb->prepare('SELECT c.id, c.name, c.description, c.icon, c.created_at, i.url image_url FROM category c LEFT JOIN image i ON c.image_id=i.id WHERE c.id = :categoryId LIMIT 1');
        $oStmt->bindParam(':categoryId', $iId, \PDO::PARAM_INT);
        $oStmt->execute();
        return $oStmt->fetch(\PDO::FETCH_OBJ);
    }

    public function update(array $aData)
    {
        $oStmt = $this->oDb->prepare('UPDATE category SET name = :name, slug = :slug, description = :description, icon = :icon, image_id = :image_id WHERE id = :categoryId LIMIT 1');
        $oStmt->bindValue(':categoryId', $aData['category_id'], \PDO::PARAM_INT);
        $oStmt->bindValue(':name', $aData['name']);
        $oStmt->bindValue(':slug', $aData['slug']);
        $oStmt->bindValue(':description', $aData['description']);
        $oStmt->bindValue(':icon', $aData['icon']);
        $oStmt->bindValue(':image_id', $aData['image_id']);
        return $oStmt->execute();
    }
    
    public function isUniq($value)
    {
        $oStmt = $this->oDb->prepare("SELECT * FROM category WHERE name = :category_name LIMIT 1");
        $oStmt->bindParam(':category_name', $value);
        $oStmt->execute();
        return (bool) $oStmt->rowCount();
    }

    public function delete($iId)
    {
        $oStmt = $this->oDb->prepare('DELETE FROM category WHERE id = :categoryId LIMIT 1');
        $oStmt->bindParam(':categoryId', $iId, \PDO::PARAM_INT);
        return $oStmt->execute();
    }
}
