-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  sam. 17 fév. 2018 à 00:04
-- Version du serveur :  10.1.29-MariaDB
-- Version de PHP :  7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bidi`
--

-- --------------------------------------------------------

--
-- Structure de la table `advert`
--

CREATE TABLE `advert` (
  `id` int(11) NOT NULL,
  `author_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `summary` longtext COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `published_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `type` enum('standard','sponsored') COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `image_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `advert`
--

INSERT INTO `advert` (`id`, `author_id`, `category_id`, `title`, `slug`, `summary`, `content`, `location`, `price`, `currency`, `created_at`, `published_at`, `updated_at`, `type`, `status`, `image_id`) VALUES
(37, 3, 6, 'Gestionnaire de contenu', 'gestionnaire-de-contenu', 'kerjmk:erf', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Canada', '12', '€', '2018-02-02 11:07:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'standard', 1, 75),
(38, 3, 2, 'Téléphone android', 'telephone-android', 'Un phone solide', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip', 'Libreville', '146', '€', '2018-02-03 13:19:05', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'standard', 1, 91),
(39, 3, 2, 'Ordinateur complet', 'ordinateur-complet', 'Un tres bon ordinateur de marque', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Chicago', '230', '$', '2018-02-03 13:31:01', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'standard', 1, 83),
(40, 3, 7, 'Offre immobilière', 'offre-immobili-ere', 'Agent immobilier', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Belgique', '752538', '€', '2018-02-03 13:33:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'standard', 1, 82),
(41, 3, 8, 'Jeux de Foot', 'jeux-de-foot', 'hhldchzdhc', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Bénin', '15', 'cfa', '2018-02-08 23:31:56', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'standard', 1, 80),
(42, 3, 1, 'Produits de beauté', 'produits-de-beaute', 'De super produits pour votre soin corporel', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum ggd.', 'Suisse', '18', '€', '2018-02-09 07:51:54', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'standard', 1, 79),
(43, 3, 1, 'Chaussure Boss', 'chaussure-boss', 'une très bonne chaussure', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Bénin', '38', '€', '2018-02-09 10:27:45', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'standard', 1, 84),
(48, 3, 74, 'Volkwgen C4', 'volkwgen-c4', 'skelzez', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Allemagne', '45248', '€', '2018-02-09 12:25:07', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'standard', 1, 73),
(49, 3, 74, 'Nissan C5', 'nissan-c5', '', 'C\'est une voiture à conduire  absolument. consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore db magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit essen\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'France', '21538', '€', '2018-02-10 19:11:38', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'standard', 1, 92);

-- --------------------------------------------------------

--
-- Structure de la table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `icon` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `category`
--

INSERT INTO `category` (`id`, `name`, `slug`, `description`, `icon`, `image_id`, `created_at`) VALUES
(1, 'Mode &amp; beauté', 'mode-et-beaute', 'pour la beauté et la mode', 'umbrella', 0, '2018-01-31 12:32:27'),
(2, 'High tech &amp; informatique', 'high-tech-et-informatique', 'informatique', 'laptop', 0, '2018-01-31 12:33:06'),
(3, 'Loisirs &amp; Cultures', 'loisirs-et-cultures', '', 'puzzle-piece', 0, '2018-01-31 12:33:48'),
(4, 'Emploi &amp; Formation', 'emploi-et-formation', 'bk', 'graduation-cap', 0, '2018-01-31 12:34:13'),
(5, 'Electroménager', 'electromenager', '', 'ship', 0, '2018-01-31 12:34:53'),
(6, 'Services', 'services', '', 'edit', 0, '2018-01-31 12:35:59'),
(7, 'Immobilier', 'immobilier', 'zeh', 'building', 0, '2018-01-31 12:36:13'),
(8, 'Jeux &amp; jouets', 'jeux-et-jouets', 'qhbdlq', 'gamepad', 72, '2018-02-10 13:34:40'),
(71, 'Meubles &amp; décoration', 'meubles-et-decoration', '', 'paint-brush', 0, '2018-02-10 21:34:34'),
(72, 'Bricolage &amp; Jardinage', 'bricolage-et-jardinage', '', 'cog', 0, '2018-02-11 18:18:29'),
(74, 'Auto &amp; Moto', 'auto-et-moto', '', 'automobile', 0, '2018-02-11 19:34:46');

-- --------------------------------------------------------

--
-- Structure de la table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  `subject` varchar(45) DEFAULT NULL,
  `message` text NOT NULL,
  `sent_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `contact`
--

INSERT INTO `contact` (`id`, `name`, `email`, `subject`, `message`, `sent_at`) VALUES
(4, 'introductions', 'arnjoc@gmail.com', 'Problème d\'ajout  d\'annonces', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit', '2018-02-10 01:20:14');

-- --------------------------------------------------------

--
-- Structure de la table `image`
--

CREATE TABLE `image` (
  `id` int(11) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `alt` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `image`
--

INSERT INTO `image` (`id`, `url`, `alt`) VALUES
(1, 'fjfdj', NULL),
(2, '5a742c23e4b54.jpg', NULL),
(3, '5a742c5588622.jpg', NULL),
(4, '5a742c55cb86a.jpg', NULL),
(5, '5a742e53e478a.jpg', NULL),
(6, '5a742e5434732.jpg', NULL),
(7, '5a742f926fd34.jpg', NULL),
(8, '5a742f92a933a.jpg', NULL),
(9, '5a7431a2ae9f3.jpg', NULL),
(10, '5a74344f0f3b9.png', NULL),
(11, '5a7436027830a.png', NULL),
(12, '5a7436b55e9cb.png', NULL),
(13, '5a7436efe4f97.jpg', NULL),
(14, '5a7437ba0635e.jpg', NULL),
(15, '5a743856ec6df.jpg', NULL),
(16, '5a75a8b8ecb9d.jpg', NULL),
(17, '5a75ab85aeb69.jpg', NULL),
(18, '5a75ac0f44b95.jpg', NULL),
(19, '5a75ac82e313c.jpg', NULL),
(20, '5a75acc2ca173.png', NULL),
(21, '5a75ad4a8473d.png', NULL),
(22, '5a75af749a599.jpg', NULL),
(23, '5a75af8d430c3.jpg', NULL),
(24, '5a75aff3341d1.jpg', NULL),
(25, '5a7d4509c0a24.jpg', NULL),
(26, '5a7d699129b91.jpg', NULL),
(27, '5a7d9a792615f.jpg', NULL),
(28, '5a7d9c8462dbf.jpg', NULL),
(29, '5a7d9d7c0e6d4.jpg', NULL),
(30, '5a7eefcde54fd.jpg', NULL),
(31, '5a7eefdf2d788.jpg', NULL),
(32, '5a7eefdf6bf96.jpg', NULL),
(33, '5a7eefdfb288f.jpg', NULL),
(34, '5a7eefe004b5f.jpg', NULL),
(35, '5a7eefe05ecdd.jpg', NULL),
(36, '5a7ef09ae926c.jpg', NULL),
(37, '5a7ef09b2cad9.jpg', NULL),
(38, '5a7ef09b5e7c4.jpg', NULL),
(39, '5a7ef09b94719.jpg', NULL),
(40, '5a7ef0af90158.jpg', NULL),
(41, '5a7ef0b004af8.jpg', NULL),
(42, '5a7ef0b02bfea.jpg', NULL),
(43, '5a7ef0b2bada5.jpg', NULL),
(44, '5a7ef0b3139ef.jpg', NULL),
(45, '5a7ef0c87bae1.jpg', NULL),
(46, '5a7ef0c917973.jpg', NULL),
(47, '5a7ef0c93dec4.jpg', NULL),
(48, '5a7ef0d5e0f10.jpg', NULL),
(49, '5a7ef0d621c85.jpg', NULL),
(50, '5a7ef0d652200.jpg', NULL),
(51, '5a7ef0fa3b6ee.jpg', NULL),
(52, '5a7ef2c5eb0ac.png', NULL),
(53, '5a7ef2d0112a6.png', NULL),
(54, '5a7ef2d0f2c3b.png', NULL),
(55, '5a7ef2d18d35d.png', NULL),
(56, '5a7ef2d21b72b.png', NULL),
(57, '5a7ef331ac94f.png', NULL),
(58, '5a7ef33281616.png', NULL),
(59, '5a7ef33313095.png', NULL),
(60, '5a7ef333aae69.png', NULL),
(61, '5a7ef334347fe.png', NULL),
(62, '5a7ef334a4911.png', NULL),
(63, '5a7ef335140a8.png', NULL),
(64, '5a7ef340bc2c4.png', NULL),
(65, '5a7ef34187731.png', NULL),
(66, '5a7ef34215718.png', NULL),
(67, '5a7ef34372f7f.png', NULL),
(68, '5a7ef3444320d.png', NULL),
(69, '5a7ef34498569.png', NULL),
(70, '5a7ef348aedb3.png', NULL),
(71, '5a7f35d9ec103.jpg', NULL),
(72, '5a7f594ac5794.jpg', NULL),
(73, '5a809958af8c4.jpg', NULL),
(74, '5a809a900843a.jpg', NULL),
(75, '5a809b808301e.jpg', NULL),
(76, '5a809cdf1df22.jpg', NULL),
(77, '5a809dcf9600d.jpg', NULL),
(78, '5a809e4a3c684.jpg', NULL),
(79, '5a809e866299c.jpg', NULL),
(80, '5a809eb51cf0c.jpg', NULL),
(81, '5a809f072b06a.jpg', NULL),
(82, '5a809f303ceef.jpg', NULL),
(83, '5a809f4e22bd5.jpg', NULL),
(84, '5a80aa9340878.jpg', NULL),
(85, '5a80aba02c1d5.jpg', NULL),
(86, '5a80abcac77ec.png', NULL),
(87, '5a81533983243.jpg', NULL),
(88, '5a815559b826a.png', NULL),
(89, '5a8155b263f09.png', NULL),
(90, '5a8155fc72285.jpg', NULL),
(91, '5a8157e136853.jpg', NULL),
(92, '5a81583a6f6e3.jpg', NULL),
(93, '5a87416baf16b.png', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `advert_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `pubs`
--

CREATE TABLE `pubs` (
  `id` int(11) NOT NULL,
  `created_by_id` int(11) DEFAULT NULL,
  `pub_category_id` int(11) DEFAULT NULL,
  `image_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `published_at` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `pubs`
--

INSERT INTO `pubs` (`id`, `created_by_id`, `pub_category_id`, `image_id`, `title`, `slug`, `content`, `url`, `created_at`, `published_at`, `updated_at`, `end_date`, `status`) VALUES
(1, NULL, 1, 24, 'Services de devflam', 'services-de-devflam', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus illo vel dolorum soluta consectetur doloribus sit. Delectus non tenetur odit dicta vitae debitis suscipit doloribus. Ipsa, aut voluptas quaerat', 'http://asso.phpnet.us/ftpbucket/deploy.php', '2018-01-31 17:56:20', NULL, '2018-02-03 13:49:55', '2018-01-20 00:00:00', 1),
(2, 3, 2, 23, 'BD a lire absolument', 'bd-a-lire-absolument', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud', 'http://asso.phpnet.us/ftpbucket/deploy.php', '2018-02-02 11:00:22', NULL, '2018-02-03 13:48:13', '2018-02-02 00:00:00', 1);

-- --------------------------------------------------------

--
-- Structure de la table `pub_category`
--

CREATE TABLE `pub_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `pub_category`
--

INSERT INTO `pub_category` (`id`, `name`, `slug`, `description`, `created_at`) VALUES
(1, 'Music', 'music', '', '2018-01-31 16:15:19'),
(2, 'Santé', 'sante', '', '2018-01-31 16:15:51');

-- --------------------------------------------------------

--
-- Structure de la table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slogan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `linkedin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_plus` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `settings`
--

INSERT INTO `settings` (`id`, `name`, `slogan`, `description`, `address`, `phone`, `email`, `facebook`, `twitter`, `instagram`, `linkedin`, `google_plus`) VALUES
(1, 'Bidi Annonces', 'Votre site de petites annonces', 'Un super site de petites annonces.Lorem ipsum dolor sit amet, \r\nconsectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim \r\nad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip\r\n ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit', 'Aibatin , Bénin', '(+229) 66 45 89 94', 'Test@test.com', 'Facebook.com/bidi-annoces', 'Twitter.com/bidi-annoces', 'Instagram.com/bidi-annoces', 'Linkedin.com/bidi-annoces', 'Google.com/bidi-annoces');

-- --------------------------------------------------------

--
-- Structure de la table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `image_id` int(11) DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `town` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `confirm_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `image_id`, `name`, `username`, `email`, `phone`, `password`, `country`, `town`, `role`, `enabled`, `confirm_token`, `created_at`) VALUES
(3, 93, 'admin', 'admin', 'test@test.com', NULL, '$2y$14$oeyWXhljsDsuzzAUvoQhM.vBw4KLHjRskvROFHTSLNyOhJPmRNMrK', NULL, NULL, 'ROLE_ADMIN', 1, NULL, '2018-02-01 11:38:18'),
(4, NULL, 'joki cojel', 'arnjoc', 'joc@gmail.com', NULL, '$2y$14$EcmxZTTGzQElsbwv9PQlk.h2gPszCHzEYr1oNmWvpQYHV/qyLtwya', '', '', 'ROLE_USER', 1, '', '2018-02-04 14:58:20');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `advert`
--
ALTER TABLE `advert`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_77E0ED582B36786B` (`title`),
  ADD KEY `IDX_77E0ED58F675F31B` (`author_id`),
  ADD KEY `IDX_77E0ED5812469DE2` (`category_id`),
  ADD KEY `image_id` (`image_id`) USING BTREE;

--
-- Index pour la table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_64C19C15E237E06` (`name`);

--
-- Index pour la table `contact`
--
ALTER TABLE `contact`
  ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `pubs`
--
ALTER TABLE `pubs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_5A443C85989D9B62` (`slug`),
  ADD KEY `IDX_5A443C85B03A8386` (`created_by_id`),
  ADD KEY `IDX_5A443C8512469DE2` (`pub_category_id`),
  ADD KEY `image_id` (`image_id`) USING BTREE;

--
-- Index pour la table `pub_category`
--
ALTER TABLE `pub_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_64C19C15E237E06` (`name`);

--
-- Index pour la table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_CFC710072B36786B` (`title`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`),
  ADD UNIQUE KEY `UNIQ_8D93D6493DA5256D` (`image_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `advert`
--
ALTER TABLE `advert`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT pour la table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT pour la table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `image`
--
ALTER TABLE `image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT pour la table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `pubs`
--
ALTER TABLE `pubs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `pub_category`
--
ALTER TABLE `pub_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `advert`
--
ALTER TABLE `advert`
  ADD CONSTRAINT `FK_77E0ED5812469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `FK_77E0ED58F675F31B` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `pubs`
--
ALTER TABLE `pubs`
  ADD CONSTRAINT `FK_5A443C8512469DE2` FOREIGN KEY (`pub_category_id`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `FK_5A443C85B03A8386` FOREIGN KEY (`created_by_id`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `FK_8D93D6493DA5256D` FOREIGN KEY (`image_id`) REFERENCES `image` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
